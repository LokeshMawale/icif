import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
// import { MainComponent } from './main/main.component';
// import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserService } from './services/user.service';
import { AppCustomPreloader } from './services/AppCustomPreloader';
import { SelectivePreloadingStrategyService } from './services/selective-preloading-strategy.service';

// import { UsersComponent } from './AdminPanel/users/users.component';
// import { RolesComponent } from './AdminPanel/roles/roles.component';
// import { PlantComponent } from './AdminPanel/plant/plant.component';
// import { WorkflowComponent } from './AdminPanel/workflow/workflow.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent, data: { title: 'Login Page', animation: 'main'}},
  // { path: 'main', component: MainComponent}, //, canActivate: [AuthGuard]},
  { path: 'main',
    loadChildren: () => import('./main/main.module').then(m => m.MainModule),
    data: {animation: 'Home'},
    // canLoad: [AuthGuard]
  },
  { path: 'user/:userid', component: UserService , data: {animation: 'About'}},
  // { path: '**', component: PageNotFoundComponent  }

  /*
  { path: 'AdminPanel/users', component: UsersComponent},//, canActivate: [AuthGuard]},
  { path: 'AdminPanel/roles', component: RolesComponent},//, canActivate: [AuthGuard]},
  { path: 'AdminPanel/plant', component: PlantComponent},//, canActivate: [AuthGuard]},
  { path: 'AdminPanel/workflow', component: WorkflowComponent},//, canActivate: [AuthGuard]},
  */

//  {    path: 'myApp', canActivate: [IsAuthorized], loadChildren: './layout/layout.module#LayoutModule'//<- Other routes here  },
//  {    path: 'login', loadChildren: './login/login.module#LoginModule'  },
//  {    path: 'error', component: ErrorComponent  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,  { enableTracing: false, preloadingStrategy: SelectivePreloadingStrategyService })],
  providers: [AppCustomPreloader],
  exports: [RouterModule]
})
export class AppRoutingModule { }
