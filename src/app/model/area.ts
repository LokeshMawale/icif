export class Area{
    areaId?: number;
    name: string;
    code: string;
    regionId: number;
    clusters?: [];
}