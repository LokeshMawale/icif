export class Opco{
    opcoId?: number;
    name: string;
    code: string;
    clusterId: number;
    plants?: [];
}