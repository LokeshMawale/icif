export interface typetask{
    id: number;
    name: string;
    description: string;
}