export interface Processes {
    processId?: number;
    name: string;
    type: string;
    description: string;
    durationInDays: number;
    sequenceNumber: number;
    mandatory?: boolean;
    //linksToDoc: [];
    //linksToFolderName: string;
    //linksToFolder: [];
    //linksToSiteName: string;
    //linksToSite: [];
    //subTasks: [];
    //procId: number;
    //roleIds: number;
    processTasks?: Workflow[];
    active?: boolean;
};

export interface Workflow {
taskId?: number;
name: string;
description: string;
durationInDays: number;
sequenceNumber?: number;
mandatory?: boolean;
docName?: string[];
folderName?: string[];
videoName?: string[];
siteName?: string[];
type: typetask;
linksToDoc?: string[];
linksToVideo?: string[];
linksToFolder?: string[];
linksToSite?: string[];
sprint:string;
subTasks?: [];
roles?: Roles[];
procId?: number;
active?: boolean;
version : String;
docLink?:string[];
folderLink?:string[];
videoLink?:string[];
siteLink?:string[];
}

export interface typetask{
id: number;
name: string
}

export interface Roles {
id?: number;
name?: string;
type?: string;
description?: string
};
