
export interface User {
    id?: number;
    name: string;
    email: string;
    imageUrl?: string;
    emailVerified?: boolean;
    sapUserId?: string;
    provider?: string;
    providerId?: string;
    createdBy?: string;
    createDate?: Date;
    updatedBy?: string;
    updateDate?: string;
    lastLoginDate?: string;
    roles?: Roles[];
    plants?: Plants[];
    //totalPages?: number;
    //totalElements?: number
};

export interface Roles {
    id?: number;
    name?: string;
    type?: string;
    description?: string
};

export interface Plants {
    plantId?: number;
    name: string;
    code: string;
    smartsheetLink?: string;
    madiLink?: string;
    type: [{id: number, name: string}];
    opcoId: number;
    opcoName: string;
}