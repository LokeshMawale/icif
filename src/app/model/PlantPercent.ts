export interface PlantPercent {
        taskPercentages: [
                {
                plantId: number;
                taskPercentage?: number;
                plant_Name: string;
                porcessName?: string;
                process_Id?: number
                }
        ];
}