export class Task {
    taskId: number;
    plantId: number;
    processId: number;
    taskType: String;
    comment: String;
    sequenceNo: number;
    totalNoOfTask: number;
    input: String;
    dependency: number;
    taskTransId: number;
    userId: number;
    processTrnsID : number;
    isLastTask : boolean;
    completedBy : String;
    sprint: String;
    completedTasks:number;
    version:String;
    floating:boolean;
    reopenedBy:String
    postponedBy : String;
    completeProcess:boolean;
    taskFlag:String;
    wave:String;
}
