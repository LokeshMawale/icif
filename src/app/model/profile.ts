export interface Profile{
    id: number;
    name: string;
    email: string;
    imageUrl?: string;
    emailVerified?: boolean;
    sapUserId?: string;
    provider?: string;
    providerId?: string;
    lastLoginDate?: Date;
    roles?:[{
        id?: number;
        name?: string;
        type?: string;
        description?: string
        }];
    plants?:[{
        plantId: number;
        name?: string;
        code?: string;
        smartsheetLink?: string;
        madiLink?: string;
        gapAnalyzerLink?: string
    }]
 }