export interface Comment {
    taskTransId: Number;
    comment: String;
    user: String;
    completedBy: String;
    commentDate: String;
    taskFlag : String
  }