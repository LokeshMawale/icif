export class Plant{
    plantId?: number;
    name: string;
    code: string;
    smartsheetLink?: string;
    madiLink?: string;
    gapAnalyzerLink?: string;
    type: [{id: number, name: string}];
    opcoId: number;
    opcoName: string;
}