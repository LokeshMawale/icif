export class Region{
    regionId?: number;
    name: string;
    code: string;
}