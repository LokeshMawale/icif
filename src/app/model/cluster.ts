export class Cluster{
    clusterId?: number;
    name: string;
    code: string;
    areaId: number;
    opcos?: [];
}