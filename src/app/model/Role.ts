export interface Role{
    id?: number;
    name: string;
    type?: string;
    description?: string
}