
export interface SubTasks {
  name: string;
  description: string;
  daysToDelay: number;
  docName?: string[];
  folderName?: string[];
  videoName?: string[];
  siteName?: string[];
  sprint:string;
  processTransactionId?: number;
  docLink?:string[];
  folderLink?:string[];
  videoLink?:string[];
  siteLink?:string[];
}

