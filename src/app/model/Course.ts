
export class Course {
    courseName: string;
    enrolled: number;
    inProgress: number;
    completed: number;
    impactVote: number;
    averageRating: number;
}
