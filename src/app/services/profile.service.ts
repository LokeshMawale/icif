import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Profile } from '../model/profile';
import { environment } from '../../environments/environment.prod';
import { ToasterService } from 'angular2-toaster';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
    providedIn: 'root'
})

export class ProfileService {

    api = environment.apiUrl;
    profile: Profile={id: 0, name: "", email: "", imageUrl: "", plants:[{ plantId: 0, name: "", code: "", smartsheetLink: ""}]};

    constructor(
        private http: HttpClient) { }

    GetProfile(): Observable<Profile> {
        let path = `${this.api}/user/me`;
        if (window.location.hostname == 'localhost') {
            path = `http://localhost:8080/user/me`;
        }
        return this.http.get<Profile>(path) ;
    }
}


