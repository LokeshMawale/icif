import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

//const AUTH_API = 'http://localhost:8080/api/auth/';
const AUTH_API ='http://localhost:8080/oauth2/authorize/google?redirect_uri=http://localhost:4200/main';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class AuthService2 {

  //uri = 'http://localhost:5000/api';
  uri = 'http://localhost:8080/oauth2/authorize/google?redirect_uri=http://localhost:4200/main';

  constructor(private http: HttpClient, private router: Router) { }
  login() {
    this.http.post(this.uri, {})
    .subscribe((resp: any) => {
      localStorage.setItem('auth_token', resp.token);
      console.log(resp);
      });
      this.router.navigate(['main']);
    }
  
  register(user): Observable<any> {
    return this.http.post(AUTH_API + 'signup', {
      username: user.username,
      email: user.email,
      password: user.password
    }, httpOptions);
  }
}

