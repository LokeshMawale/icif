import { Cluster } from '../model/cluster';
import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { environment } from './../../environments/environment.prod';
import { ToastrService } from 'ngx-toastr';
  

@Injectable({
  providedIn: 'root'
})
export class ClusterService {

  private api = environment.apiUrl;
    
  constructor(
    private http: HttpClient,
    private toastr: ToastrService) { }
      
  getAllCluster() {
    const path = `${this.api}/clusters/`;
    //const path = environment.apiUrl + '/regions';
    return this.http.get<Cluster[]>(path);
  }

  getCluster(id: string) {
    const path = `${this.api}/clusters?clusterId=${id}`;
    return this.http.get<Cluster>(path);
  }

  createCluster(cluster: Cluster) {
    const path = `${this.api}/createcluster`;
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    this.showSuccess();
    return this.http.post(path,  JSON.stringify(cluster), httpOptions);
  }

  updateCluster(cluster: Cluster) {
    const path = `${this.api}/updatecluster/${cluster.clusterId}`;
    this.showSuccess();
    return this.http.put<Cluster>(path, cluster);
  }

  deleteCluster(clusterId: string) {
    const path = `${this.api}/cluster/${clusterId}`;
    this.showSuccess();
    return this.http.delete(path);
  }
  
  showSuccess() {
    this.toastr.success('Successfully', 'Cluster Configured!',{
      timeOut:1500, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
  
  showError() {
    this.toastr.error('Error', 'Cluster Change Failure!',{
      timeOut:2000, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
}

