import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { Region } from "../model/region";
import { environment } from './../../environments/environment.prod';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  private api = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private toastr: ToastrService) { }

  getAllRegion() {
    const path = `${this.api}/regions/`;
    //const path = environment.apiUrl + '/regions';
    return this.http.get<Region[]>(path);
  }

  getRegion(id: string) {
    const path = `${this.api}/regions?regionId=${id}`;
    return this.http.get<Region>(path);
  }

  createRegion(region: Region) {
    const path = `${this.api}/createregion`;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }
    this.showSuccess();
    return this.http.post(path, JSON.stringify(region), httpOptions);
  }

  updateRegion(region: Region) {
    const path = `${this.api}/updateregion/${region.regionId}`;
    this.showSuccess();
    return this.http.put<Region>(path, region);
  }

  deleteRegion(regionId: string) {
    const path = `${this.api}/regions/${regionId}`;
    this.showSuccess();
    return this.http.delete(path);
  }

  showSuccess() {
    this.toastr.success('Successfully', 'Region Configured!',{
      timeOut:1500, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
  
  showError() {
    this.toastr.error('Error', 'Region Change Failure!',{
      timeOut:2000, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }

}

