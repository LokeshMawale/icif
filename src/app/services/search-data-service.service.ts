import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchDataService {

  public searchDataDetails: any = [];

  private messageSource = new BehaviorSubject(this.searchDataDetails);
  constructor() { }

  currentMessage = this.messageSource.asObservable();

  changeMessage(searchResult: any) {
    this.messageSource.next(searchResult)
  }

}
