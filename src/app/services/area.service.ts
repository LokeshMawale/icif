import { Area } from '../model/area';
import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import { environment } from './../../environments/environment.prod';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  private api = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private toastr: ToastrService) { }

  getAllArea() {
    const path = `${this.api}/areas/`;
    // const path = environment.apiUrl + '/regions';
    return this.http.get<Area[]>(path);
  }

  getArea(id: string) {
    const path = `${this.api}/areas?areaId=${id}`;
    return this.http.get<Area>(path);
  }

  createArea(area: Area) {
    const path = `${this.api}/createarea`;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }
    this.showSuccess();
    return this.http.post(path, JSON.stringify(area), httpOptions);
  }

  updateArea(area: Area) {
    const path = `${this.api}/updatearea/${area.areaId}`;
    this.showSuccess();
    return this.http.put<Area>(path, area);
  }

  deleteArea(areaId: string) {
    const path = `${this.api}/areas/${areaId}`;
    this.showSuccess();
    return this.http.delete(path);
  }
  
  showSuccess() {
    this.toastr.success('Successfully', 'Area Configured!',{
      timeOut:1500, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
  
  showError() {
    this.toastr.error('Error', 'Area Change Failure!',{
      timeOut:2000, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
}

