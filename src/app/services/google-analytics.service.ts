import { Injectable } from '@angular/core';

declare let gtag:Function;
@Injectable({
  providedIn: 'root'
})
export class GoogleAnalyticsService {

  emailsExclude = ["lokesh.mawale.","frederic.gimbert","kati.tarpila","johan.vanzanten","david.fauvet"," jelena.stamenkovski","anastasia.vorobieva","thierry.davila","d.candea","rene.burkhalter","andre.neves"];

  constructor() { }

  public eventEmitter( 
    eventName: string, 
    eventCategory: string, 
    eventAction: string, 
    eventLabel: string = null,  
    eventValue:string = null,
    plantName:string=null,
    plantCode :string=null,
    userEmail:string=null ){ 
      let mail = localStorage.getItem("email");
      var isCorporate = false;
      if(localStorage.getItem('isCorporate')){
        isCorporate = true;
      }
      if(!this.emailsExclude.includes(mail.split('@')[0]) && !isCorporate){
         gtag('event', eventAction, { 
                 eventCategory: eventCategory, 
                 eventLabel: eventLabel, 
                 eventName: eventName, 
                 eventValue: eventValue,
                 plantName:plantName,
                 plantCode:plantCode,
                 userEmail:userEmail
               })
      }
    }
    
}
