import { MainDetailComponent } from './../main/main-detail/main-detail.component';
import { PlantId } from './../model/PlantId';
import { ProfileService } from './profile.service';
import { PlantComponent } from './../main/plant/plant.component';
import { Injectable } from '@angular/core';
import { BehaviorSubject, forkJoin, Observable, throwError } from 'rxjs';
import { Profile } from '../model/profile';
import { PlantPercent } from './../model/PlantPercent';
import { environment } from '../../environments/environment.prod';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError, tap, retry, map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class PlantIdservice {
  log: any;
  tasks: any[];

  setPlantPercentages() {
    let plantString = '';
    let plants : any;
    plants = JSON.parse(localStorage.getItem("plants"));
      let totalPlants = plants.length;
      let i = 0;
      for (var plant of plants) {
        if (i < totalPlants) {
          if(plant.plantId && plant.name){
            plantString = plantString + plant.plantId.toString().trim()+"="+plant.name.trim()+","
            plantString = plantString.trim();
          }
        }
        i++;
      }
  
    let path = `${this.api}/transactions?plants=` + plantString;
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/transactions?plants=` + plantString;
    }
    return this.http.get<any>(path)
  }

  selectedPlantPercentages() {
    this.changeLoadingValue(true);
    let plantString = localStorage.getItem("plantID")+"="+localStorage.getItem("plantName")+"&wave="+localStorage.getItem("nwaveSelected");
    //let plantString = localStorage.getItem("plantID")+"="+localStorage.getItem("plantName")+"&wave="+Number(nwaveSelected);
    let path = `${this.api}/transactions?plants=` + plantString;
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/transactions?plants=` + plantString;
    }
    return this.http.get<any>(path)
  }

  profile: Profile = { id: 0, name: "", email: "", imageUrl: "", plants: [{ plantId: 0, name: "", code: "", smartsheetLink: "" }] };
  api = environment.apiUrl;
  Plantid: string;
  PlantId: string = ""; 
  //PlantId= {PlantId: "PlantId"};
  waves: number[] =[];
  WaveSelected: number = 0;
  content: any = "";
  loading: boolean = false;
  value: number;

  // private plantidSource = new BehaviorSubject<string>("carboneras");
  // private plantidSource = new BehaviorSubject<string>(null); //(this.profile.plants[0].name); //("Carboneras");

  private plantidSource = new BehaviorSubject<Object>(this.PlantId); //(this.profile.plants[0].name); //("Carboneras");
  currentPlantid = this.plantidSource.asObservable();

  private taskContentSource = new BehaviorSubject<Object>(this.content);
  newcontent = this.taskContentSource.asObservable();

  private isLoadingSource = new BehaviorSubject<Object>(this.loading);
  isLoading = this.isLoadingSource.asObservable();

  private currentCodeSource = new BehaviorSubject<string>("CS");
  currentPlantCode = this.currentCodeSource.asObservable();

  private preparationContentSource = new BehaviorSubject<Object>(this.content);
  preparationcontent = this.preparationContentSource.asObservable();

  private assessmentContentSource = new BehaviorSubject<Object>(this.content);
  assessmentcontent = this.assessmentContentSource.asObservable();

  private implementationContentSource = new BehaviorSubject<Object>(this.content);
  implementationcontent = this.implementationContentSource.asObservable();

  private folllowupContentSource = new BehaviorSubject<Object>(this.content);
  followupcontent = this.folllowupContentSource.asObservable();

  private sustainContentSource = new BehaviorSubject<Object>(this.content);
  sustaincontent = this.sustainContentSource.asObservable();

  private totalPercentSource = new BehaviorSubject<Object>(this.content);
  totalPercentContent = this.totalPercentSource.asObservable();

  private startButtonContentSource = new BehaviorSubject<Object>(this.content);
  startButtonContent = this.startButtonContentSource.asObservable();

  private NArrayWaves = new BehaviorSubject<number[]>(this.waves);
  ArrayWaves = this.NArrayWaves.asObservable();
  
  private NWaveSelected = new BehaviorSubject<number>(this.WaveSelected);
  waveselected = this.NWaveSelected.asObservable();


  constructor(private ProfileService: ProfileService,
    private http: HttpClient,
    private toastr: ToastrService)  { }

  changeTotalPercentContent(percent: any) {
    this.totalPercentSource.next(percent);
  }

  changePreparationContent(percent: any) {
    this.preparationContentSource.next(percent);
  }

  changeAssesmentContent(percent: any) {
    this.assessmentContentSource.next(percent);
  }

  changeImplementationContent(percent: any) {
    this.implementationContentSource.next(percent);
  }

  changeFollowupContent(percent: any) {
    this.folllowupContentSource.next(percent);
  }

  changeSustainpContent(percent: any) {
    var circleOption: any;
    this.sustainContentSource.next(circleOption);
  }

  changeTaskContent(content: any) {
    this.taskContentSource.next(content);
  }

  changeStartButtonEnableConte(content: any) {
    this.startButtonContentSource.next(content);
  }

  getidplant(PlantId: PlantId) {
    this.ProfileService.GetProfile().subscribe(profile => {
      this.profile = profile;
      this.profile.plants.sort((a, b) => a.name.localeCompare(b.name));
      if (!localStorage.getItem('plantName')) {
        let plants = JSON.parse(localStorage.getItem("plants")); localStorage.setItem("plantID", plants[0].plantId);
        localStorage.setItem("plantName", plants[0].name);
        this.plantidSource.next(this.PlantId);
        console.log("antes: " + localStorage.getItem('plantName'));
      }
      else {

        this.PlantId = this.profile.plants[0].name;
        this.plantidSource.next(this.PlantId);
        //console.log("despues " + localStorage.getItem('plantName'));

      }
    });
  }

  GetPlantPercent() {
    this.profile.plants;
    const path = `${this.api}/transactions`;
    return this.http.get(path,
      {
        params: new HttpParams(
          { fromString: "plant=" + this.profile.plants[0].name }), observe: "response"
      }).pipe(retry(3),
        catchError(this.handleError),
        tap(res => {
          console.log(res.headers.get('Link'));
          this.parseLinkHeader(res.headers.get('Link'));
        }));
  }

  parseLinkHeader(header) {
    if (header.length == 0) {
      return;
    }

    let parts = header.split(',');
    var links = {};
    parts.forEach(p => {
      let section = p.split(';');
      var url = section[0].replace(/<(.*)>/, '$1').trim();
      var name = section[1].replace(/rel="(.*)"/, '$1').trim();
      links[name] = url;
    });
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  closeWave(plantId: number): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let path = `${this.api}/closeWave`;
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/closeWave`
    };
    let options = { headers: headers, responseType: 'text' as 'json' };    
   return this.http.post<any>(path, plantId, options).pipe(tap(text => this.showSuccessWithoutTimeOut(text + " Please Remember to start the Follow up & Sustainability processess in the next 3 weeks")));
   //return this.http.post<any>(path, plantId, options);
  }
  
getAllClosedWave(): Observable<any> {
  let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  const plantID = localStorage.getItem("plantID");
  const nwaveSelected = localStorage.getItem("nwaveSelected");
  //const plantID = this.plantId;
  this.NArrayWaves.next(this.waves);
  this.NWaveSelected.next(this.WaveSelected);
  let path = `${this.api}/getAllClosedWave/`;
  if (window.location.hostname == 'localhost') {
    path = `http://localhost:8080/getAllClosedWave/`
  };
  return this.http.get(path+ plantID, {'headers':headers});
}


viewClosedWave(nwaveSelectedHistory: number): Observable<any[]>  {
  let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  const plantID = localStorage.getItem("plantID");
  const nwaveSelectedHistory2 = localStorage.getItem("nwaveSelectedHistory");
  let preparation =  this.http.get(`http://localhost:8080/viewClosedWave/`+ plantID +"/"+ nwaveSelectedHistory +"/"+ "1", {'headers':headers});
  let assessment = this.http.get(`http://localhost:8080/viewClosedWave/`+ plantID +"/"+ nwaveSelectedHistory +"/"+ "2", {'headers':headers});
  let implementarionS1 = this.http.get(`http://localhost:8080/viewClosedWave/`+ plantID +"/"+ nwaveSelectedHistory +"/"+ "3=S1", {'headers':headers});
  let implementarionS2 = this.http.get(`http://localhost:8080/viewClosedWave/`+ plantID +"/"+ nwaveSelectedHistory +"/"+ "3=S2", {'headers':headers});
  let implementarionS3 = this.http.get(`http://localhost:8080/viewClosedWave/`+ plantID +"/"+ nwaveSelectedHistory +"/"+ "3=S3", {'headers':headers});
  let implementarionS4 = this.http.get(`http://localhost:8080/viewClosedWave/`+ plantID +"/"+ nwaveSelectedHistory +"/"+ "3=S4", {'headers':headers});
  let implementarionS5 = this.http.get(`http://localhost:8080/viewClosedWave/`+ plantID +"/"+ nwaveSelectedHistory +"/"+ "3=S5", {'headers':headers});
  let followup = this.http.get(`http://localhost:8080/viewClosedWave/`+ plantID +"/"+ nwaveSelectedHistory +"/"+ "4", {'headers':headers});
  let sustainability = this.http.get(`http://localhost:8080/viewClosedWave/`+ plantID +"/"+ nwaveSelectedHistory +"/"+ "5", {'headers':headers});
  
  return forkJoin([preparation, assessment,implementarionS1,implementarionS2,implementarionS3,implementarionS4,implementarionS5,followup,sustainability])
  /* .subscribe(results => {
    // results[0] is our character
    // results[1] is our character homeworld
    //results[0].homeworld = results[1];
    this.tasks = results;
    console.log(this.tasks);
    localStorage.setItem('viewClosedWaveContent',  JSON.stringify(this.tasks));
    //return this.tasks;
    return results; }); }*/


/*const plantID = localStorage.getItem("plantID");
  const nwaveSelected = localStorage.getItem("nwaveSelected");
  const processID = localStorage.getItem("processID");
  return this.http.get(`http://localhost:8080/viewClosedWave/`+ plantID +"/"+ nwaveSelected +"/"+ processID, {'headers':headers});
*/
}

  /*
  closeWave(plantId: number) {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    let bodyData = {body: JSON.stringify(plantId)};

    this.http.post(this.api + "/closeWave", bodyData, options).subscribe(data => {
      //this.dialogData = this.PlantId;
      this.showSuccess();
      console.log(data);
      console.log(this.PlantId);
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message);
        this.showError();
        this.toastr.show(err.name + ' ' + err.message);
      });
  }

*/
  changeplantid(Plantid: string) {
    this.plantidSource.next(Plantid);
  }

  changeplantcode(PlantCode: string) {
    this.currentCodeSource.next(PlantCode)
  }

  changeLoadingValue(isLoading: Boolean) {
    this.isLoadingSource.next(isLoading);
  }
  
  changeNArrayWaves(waves: number[]) {
    this.NArrayWaves.next(waves);
  }

  changeWaveSelected(waveselected: number) {
    this.NWaveSelected.next(waveselected);
  }

  showSuccess(text) {
    this.toastr.success('Successfully', text, {
      timeOut:5000, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }

  showSuccessWithoutTimeOut(text) {
    this.toastr.success('Successfully', text, {
      closeButton:true, disableTimeOut:true, progressBar:true, positionClass: 'toast-center-center'});
  }


  showError() {
    this.toastr.error('Error', 'Wave not closed! Change Failure!',{
      timeOut:5000, closeButton:true, tapToDismiss:true, progressBar:true, positionClass: 'toast-center-center'});
  }

}


