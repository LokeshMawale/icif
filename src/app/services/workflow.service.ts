import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError, pipe, from } from 'rxjs';
import { Processes, Workflow } from '../model/Workflow';
import { HttpClient, HttpErrorResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment.prod';
import { retry, catchError, tap, map, publish } from 'rxjs/operators';
import { AddWorkflowDialogComponent } from '../main/workflow/dialogs/add/addWorkflow.dialog.component';
import { ToastrService } from 'ngx-toastr';
import { SubTasks } from '../model/subTask';

@Injectable({
  providedIn: 'root'
})
export class WorkflowService {
  addNewTask(data: SubTasks) {
    let path = `${this.apiurl}/addNewSubTask`;
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/addNewSubTask`
    }
    return this.httpClient.post<any>(path, data);  
  }

  dataChange: BehaviorSubject<Workflow[]> = new BehaviorSubject<Workflow[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  apiurl = environment.apiUrl;
  items: any;
  AddWorkflowDialogComponent: AddWorkflowDialogComponent;
  AllDataObj: any;
  AllData_Arr: string;
  processes: Processes;
  processes2: string;
  workflow: Workflow[]=[];
  processTasks: any[];
  MatSelectChange: any;
  processid2: number = 1; //last added jmpinerop
  taskId: any;

  constructor(private httpClient: HttpClient,
    private toastr: ToastrService) { }

  get data(): Workflow[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  private ProcessIdSelected = new BehaviorSubject<number>(this.processid2);
  currentProcessId = this.ProcessIdSelected.asObservable();

  changeProcessIdSelected(processid2: number) {
    this.ProcessIdSelected.next(processid2);
  }

  //: Observable<Processes> 
  /** CRUD METHODS */

  getThisProcessesConfig() {
    // now returns an Observable of Config
    return this.httpClient.get<Processes>(this.apiurl + `/processes`)
  }


  getAllProcesses() {
    return this.httpClient.get<Processes>(this.apiurl + `/processes`)
      .pipe(
        map(x => x),
        //map(response => response),
        //map((Processes: Array<Processes>) => Processes.filter(Process => Process.data.processTasks),
        tap(Processes => console.log("Processes array: ", Processes))
      ).subscribe(response => {
        this.processes = response;
      },
      (error: HttpErrorResponse) => {
          console.log(error.name + ' ' + error.message);
        });
  }

  getAllWorkflow(processId: number) {
    let version = localStorage.getItem('PROCESSVERSION');
    return this.httpClient.get<Processes>(this.apiurl + `/processes/` + processId+`/`+version)
      .pipe(
        map(x => x.processTasks),
        //map(response => response),
        //map((Processes: Array<Processes>) => Processes.filter(Process => Process.data.processTasks),
        tap(workflow => console.log("workflow array", workflow))
      ).subscribe(response => {
        this.workflow = response;
        this.dataChange.next(this.workflow);
      },
        (error: HttpErrorResponse) => {
          console.log(error.name + ' ' + error.message);
        });
  }

  /*
  getAllWorkflow(processId: number) {
    this.httpClient.get<Workflow[]>(this.apiurl + `/processes/` + processId)
    .pipe(retry(3), catchError(this.handleError))
    .subscribe((data: any) => {
      //}).subscribe(data => {
      this.dataChange.next(data.content);
      console.log(this.data);
    },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }

  getById(processId: number): Observable<Workflow[]> {
    return this.httpClient.get<Workflow[]>(this.apiurl + `/processes/` + processId);
  }

  getAllNrs(processId: number) {
    return this.httpClient.get(this.apiurl + `/processes?` + '/:' + processId).pipe(map((response: Response) => response.json()));
  }

  public sendGetRequest(processId) {
    return this.httpClient.get(this.apiurl + `/processes?`,
      {
        params: new HttpParams(
          { fromString: processId }), observe: "response"
      }).pipe(retry(3),
        catchError(this.handleError),
        tap(res => {
          console.log(res.headers.get('Link'));
          this.parseLinkHeader(res.headers.get('Link'));
        }));
  }

  parseLinkHeader(header) {
    if (header.length == 0) {
      return;
    }

    let parts = header.split(',');
    var links = {};
    parts.forEach(p => {
      let section = p.split(';');
      var url = section[0].replace(/<(.*)>/, '$1').trim();
      var name = section[1].replace(/rel="(.*)"/, '$1').trim();
      links[name] = url;

    });

  }

  //let params = new HttpParams();
  //params = params.append('size=','200');
  //params = params.append('page=','200');
  //this.httpClient.get<User[]>(this.apiurl + `/users?`,{params})
  /*
      this.httpClient.get<User[]>(this.apiurl + `/users?`,{
          params: new HttpParams()
            .set('size', "2000")
            .set('page', "1")
            .set('limit', "10")
          }).pipe(retry(3),catchError(this.handleError))
          .subscribe((data: any) => {
          //}).subscribe(data => {
          this.dataChange.next(data.content);
          console.log(this.data);
        },
          (error: HttpErrorResponse) => {
            console.log(error.name + ' ' + error.message);
          });
          
          }
          
          */
  // getAllUsers(): void{
  //   this.httpClient.get<User[]>(this.apiurl+"/users").subscribe((data: User[])=> {
  //     console.log(data);
  //     this.dataChange.next(data);
  //     //this.AllDataObj = data;
  //     //this.AllData_Arr= JSON.stringify(this.AllDataObj);
  //     //console.log(this.AllData_Arr);
  //     },
  //     (error: HttpErrorResponse) => {
  //     console.log (error.name + ' ' + error.message);
  //     });
  // }

  // DEMO ONLY, you can find working methods below
  addWorkflow(workflow: Workflow): void {
    let version = localStorage.getItem('PROCESSVERSION');
    workflow.version = version;
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    let options = { headers: headers };

    this.httpClient.post(this.apiurl + "/createtask", workflow, options).subscribe(data => {
      this.dialogData = workflow;
      console.log(JSON.stringify(workflow));
      this.showSuccess();
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message)
        this.showError();
        this.toastr.show (err.name + ' ' +  err.message);     
      });
  }

  updateWorkflow(workflow: Workflow): void {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    this.httpClient.put(this.apiurl + "/updatetask/" + workflow.taskId, workflow, options).subscribe(data => {
      this.dialogData = workflow;
      console.log(JSON.stringify(workflow));
      this.showSuccess();
      //console.log(JSON.stringify(workflow));
      //this.toasterService.showToaster('Successfully edited', 3000);
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message);
        this.showError();
        this.toastr.show (err.name + ' ' +  err.message);
      }
    );
  }

  deleteWorkflow(taskId: number): void {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    this.httpClient.delete(this.apiurl + "/deletetask/" + taskId, options).subscribe(data => {
      console.log("this user id is: " + JSON.stringify(taskId));
      console.log(data['']);
      console.log("delete id: " + taskId);
      this.showSuccess();
      //this.toasterService.showToaster('Successfully deleted', 3000);
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message);
        this.showError();
        this.toastr.show (err.name + ' ' +  err.message);
        // this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
      }
    );
  }

  getConfig(): Observable<Workflow[]> {
    return this.httpClient.get<Workflow[]>(this.apiurl + "/pocesses");
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      this.showError();
      this.toastr.show (errorMessage);
    }
    window.alert(errorMessage);
    return throwError(errorMessage);    
  }
  
  showSuccess() {
    this.toastr.success('Successfully', 'Task Configured!',{
      timeOut:1500, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
  
  showError() {
    this.toastr.error('Error', 'Task Change Failure!',{
      timeOut:1500, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
}