import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment.prod';
import { HttpClient, HttpParams } from "@angular/common/http";
import { BehaviorSubject, Observable } from 'rxjs';
import { Task } from '../model/Task';
import { Comment } from '../model/Comment';


@Injectable({
  providedIn: 'root'
})
export class TasksService {
  postpone(task: Task) {
    let path = `${this.api}/postponeTask`;
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/postponeTask`
    }
    return this.http.post<any>(path, task);  
  }
  reopen(task: Task) {
    let path = `${this.api}/reopenTask`;
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/reopenTask`
    }
    return this.http.post<any>(path, task);  
  }
  
  assignTask(taskDetails: any, name: any, email: any) {
    let path = `${this.api}/assignTask`;
    let processID = localStorage.getItem("processID");
    let processName = "";
    if(processID == "1"){
      processName = "PREPARATION";
    }
    if(processID == "2"){
      processName = "ASSESSMENT";
    }
    if(processID == "3"){
      processName = "IMPLEMENTATION";
    }
    if(processID == "4"){
      processName = "FOLLOW UP";
    }
    if(processID == "5"){
      processName = "SUSTAINABILITY";
    }
    let taskName = taskDetails.name;
    let plantName = localStorage.getItem("plantName");
    let data= {
      "assignTo": name,
      "email": email,
      "taskTransId": taskDetails.transactionId,
      "processName":processName,
      "taskName" : taskName,
      "plantName" :plantName,
      "taskFlag":taskDetails.taskFlag,

    }
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/assignTask`
    }
    return this.http.post<any>(path, data);  
  }

  getUsersInPlant(plantID: number, userID : String) {
    
    let path = `${this.api}/getUsersInPlants/plant/`+plantID+`/userID/`+userID;
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/getUsersInPlants/plant/`+plantID+`/userID/`+userID;
    }
    return this.http.get<any>(path);
  }

  getTaskData(taskDetail: any) {
    let path = `${this.api}/openTask/plant/` + taskDetail.plantId + `/process/` + taskDetail.processId + `/task/` + taskDetail.taskId;
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/openTask/plant/` + taskDetail.plantId + `/process/` + taskDetail.processId + `/task/` + taskDetail.taskId;
    }
    return this.http.get<any>(path);
  }


  modifyTaskJSON(data: any, id: Number) {

    let path = `${this.api}/modifyTaskJson`;
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/modifyTaskJson/` + id
    }
    return this.http.post<Task>(path, data);
  }


  processTask(data: any): Observable<Task> {
    let path = `${this.api}/completeTask`;
    if (window.location.hostname == 'localhost') {
      path = 'http://localhost:8080/completeTask'
    }
    return this.http.post<Task>(path, data);
  }

  private api = environment.apiUrl;

  constructor(private http: HttpClient) {

  }

  retrieveuserTaskData(processId: any) {

    // let path = `${this.api}/users/2300/transactions?plants=PlantA`;
    // if(window.location.hostname == 'localhost'){
    //   path = `http://localhost:8080/users/2300/transactions?plants=PlantA`
    // }

    let plantID = parseInt(localStorage.getItem("plantID"));
    let userID = parseInt(localStorage.getItem("userID"));
    let nwaveSelected = parseInt(localStorage.getItem("nwaveSelected"));
    let path = `${this.api}/user/` + userID + `/plant/` + plantID + `/process/` + processId + "?wave=" + nwaveSelected;

    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/user/` + userID + `/plant/` + plantID + `/process/` + processId + "?wave=" + nwaveSelected;
    }
     // console.log(path);
    return this.http.get<any>(path);
  }

  completeTask() {

  }

  startProcess(processID: any , startDate : any) {
    let plantID = parseInt(localStorage.getItem("plantID"));
    let wave = localStorage.getItem('nwaveSelected');
    var obj = {
      "statusId": 1,
      "processId": processID,
      "plantId": plantID,
      "startDate" : startDate,
      "wave":wave
    }

    let path = `${this.api}/createprocesstransaction`;
    if (window.location.hostname == 'localhost') {
      path = 'http://localhost:8080/createprocesstransaction'
    }
    return this.http.post<any>(path, obj);
  }

      
  public writeComment(comment: Comment) {
    let plantID = parseInt(localStorage.getItem("plantID"));
    let userID = parseInt(localStorage.getItem("userID"));
    let path = `${this.api}/addNewComment`;

    let commentData= {
      "comment":comment.comment,
      "completedBy":comment.completedBy,
      "taskTransId":comment.taskTransId,
      "taskFlag":comment.taskFlag
    };
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/addNewComment`
    }
    console.log(comment);
    return this.http.post<Task>(path, commentData);  
  }


}