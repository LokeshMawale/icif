import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Course } from '../model/Course';
import { environment } from '../../environments/environment.prod';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TaskElements2 } from '../main/main-detail/main-detail.component';

@Injectable({
  providedIn: 'root'
})


export class CompetencyService {

  private CoursesUrl = 'api/heroes';  // URL to web api
  private api = environment.apiUrl;
  Course: any;

  constructor(
    private http: HttpClient,
    private toastr: ToastrService) {}

  getCompetency(){
    const plantCode = localStorage.getItem('code');
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    let path = `${this.api}/competency/getUserData/${plantCode}`;
    if (window.location.hostname === 'localhost') {
      path = `http://localhost:8080/competency/getUserData/${plantCode}`;
    }
    // console.log(path);
    return this.http.get<any>(path, httpOptions);
  }

  getCourses(): Observable<Course[]> {
    const plantCode = localStorage.getItem('code');
    let path = `${this.api}/competency/getUserData/${plantCode}`;
    if (window.location.hostname === 'localhost') {
      path = `http://localhost:8080/competency/getUserData/${plantCode}`;
    }
    return this.http.get<Course[]>(path).pipe(
      catchError(this.handleError<Course[]>('getCourses', []))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  log(arg0: string) {
    throw new Error('Method not implemented.');
  }

  updateCompetency(courseName: string, rating: number ){
    const plantCode = localStorage.getItem('code');let path = `${this.api}/competency/saveCourseData`;
    if (window.location.hostname === 'localhost') {
      path = `http://localhost:8080/competency/saveCourseData`;
    }
    const body = ({courseName, plantCode, rating});
    console.log('stringify: ' + JSON.stringify(body));
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
    this.showSuccess();
    return this.http.post<any>(path, body, httpOptions);
  }

  showSuccess() {
    this.toastr.success('Successfully', 'Saved Vote!', {
      timeOut: 1500, closeButton: true, progressBar: true, positionClass: 'toast-center-center'});
  }

  showError() {
    this.toastr.error('Error', 'Saving vote Failure!', {
      timeOut: 2000, closeButton: true, progressBar: true, positionClass: 'toast-center-center'});
  }

}


