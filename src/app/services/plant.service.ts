import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, pipe, throwError } from 'rxjs';
import { Plant } from '../model/Plant';
import { HttpClient, HttpErrorResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { ToastrService } from 'ngx-toastr';


@Injectable()
export class PlantService {
  

  private api = environment.apiUrl;

  dataChange: BehaviorSubject<Plant[]> = new BehaviorSubject<Plant[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private httpClient: HttpClient,
    private toastr: ToastrService) { }

  get data(): Plant[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  /** CRUD METHODS */
  getAllPlants(): void {
    const path = `${this.api}/plants/`;
    this.httpClient.get<Plant[]>(path).subscribe(data => {
      this.dataChange.next(data);
      //console.log(data);
    },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
        this.showError();
        this.toastr.show(error.name + ' ' + error.message);
      });
  }

  getAll() {
    const path = `${this.api}/plants/`;
    //const path = environment.apiUrl + '/regions';
    return this.httpClient.get<Plant[]>(path);

  }

  addPlant(plant: Plant): void {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    this.httpClient.post(this.api + "/createplant", plant, options).subscribe(data => {
      this.dialogData = plant;
      this.showSuccess();
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message);
        this.showError();
        this.toastr.show(err.name + ' ' + err.message);
      });
  }

  updatePlant(plant: Plant): void {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    this.httpClient.put(this.api + "/updateplant/" + plant.plantId, plant, options).subscribe(data => {
      this.dialogData = plant;
      this.showSuccess();
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message);
        this.showError();
        this.toastr.show(err.name + ' ' + err.message);
      }
    );
  }

  deletePlant(plantId: number): void {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    this.httpClient.delete(this.api + "/deleteplant/" + plantId, options).subscribe(data => {
      console.log("this user id is: " + JSON.stringify(plantId));
      console.log(data['']);
      console.log("delete id: " + plantId);
      this.showSuccess();
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message);
        this.showError();
        this.toastr.show(err.name + ' ' + err.message);
      }
    );
  }

  showSuccess() {
    this.toastr.success('Successfully', 'Plant Configured!', {
      timeOut: 1500, closeButton: true, progressBar: true, positionClass: 'toast-center-center'
    });
  }

  showError() {
    this.toastr.error('Error', 'Plant Change Failure!', {
      timeOut: 2000, closeButton: true, progressBar: true, positionClass: 'toast-center-center'
    });
  }

  updatePlantLinks() {
    let path = `${this.api}/updatePlantLinks/MADILinks`;
    if (window.location.hostname == 'localhost') {
      path = 'http://localhost:8080/updatePlantLinks/MADILinks'
    }
     this.httpClient.get(path,{responseType: 'text'}).subscribe(data => {
      if(data && data == 'Process Already Running'){
        this.toastr.error('Please try after sometime', 'Synchronization Alredy Running', {
          timeOut: 3500, closeButton: true, progressBar: true, positionClass: 'toast-center-center'
        });
      }
      console.log("success");
    },
      (err: HttpErrorResponse) => {
        console.log("error");
      }
    );;
  }

  showMadiUpdate() {
    this.toastr.success('Dear User, You will recieve MADI Links status update email shortly', 'MADI Link Configuration!', {
      timeOut: 1500, closeButton: true, progressBar: true, positionClass: 'toast-center-center'
    });
  }

}
