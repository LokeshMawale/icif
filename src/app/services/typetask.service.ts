import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { typetask } from "../model/typetask";
import { environment } from './../../environments/environment.prod';
import { ToasterService } from 'angular2-toaster';
import { throwError, Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TypeTaskService {

  private api = environment.apiUrl;

  constructor(
    private http: HttpClient) { }

  getAlltypetask() {
    const path = `${this.api}/typetask/`;
    //const path = environment.apiUrl + '/regions';
    return this.http.get<typetask[]>(path);
  }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  // HttpClient API get() method => Fetch employees list
  getTypetasks(): Observable<typetask[]> {
    //return this.http.get<typetask[]>(this.api + '/typetask')
    return this.http.get<typetask[]>("../../assets/typetask.json")
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method => Fetch employee
  getTypetask(id): Observable<typetask> {
    return this.http.get<typetask>(this.api + '/typetask/' + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API post() method => Create employee
  createTypetask(typetask): Observable<typetask> {
    return this.http.post<typetask>(this.api + '/typetask', JSON.stringify(typetask), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API put() method => Update employee
  updateTypetask(id, typetask): Observable<typetask> {
    return this.http.put<typetask>(this.api + '/typetask/' + id, JSON.stringify(typetask), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method => Delete employee
  deleteTypetask(id){
    return this.http.delete<typetask>(this.api + '/typetask/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Error handling 
  handleError(error) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     window.alert(errorMessage);
     return throwError(errorMessage);
  }

}


