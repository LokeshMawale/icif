import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor(private httpClient: HttpClient) { }

  login() {
    return this.httpClient.post<{token}>('http://localhost:8080/oauth2/authorize/google?redirect_uri=http://localhost:4200/main',{}).pipe(tap(res => {
    localStorage.setItem('token', res.token);
}))
}

/*register(token:string) {
  return this.httpClient.post<{token: string}>('http://localhost:8080/oauth2/authorize/google?redirect_uri=http://localhost:4200/main', {token}).pipe(tap(res => {
  this.login(token);
}))
}
*/
logout() {
  localStorage.removeItem('tokencif');
}

public get loggedIn(): boolean{
  return localStorage.getItem('tokencif') !==  null;
}


}
