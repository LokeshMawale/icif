import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { SubTasks } from './../model/subTask';
import { environment } from '../../environments/environment.prod';
import { throwError, Observable } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SubtaskService {

  private api = environment.apiUrl;

  constructor(
    private http: HttpClient) { }
  /*
  getAllsubTasks() {
    const path = `${this.api}/subTasks/`;
    //const path = environment.apiUrl + '/regions';
    return this.http.get<SubTasks[]>(path);
  }
*/
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // HttpClient API get() method => Fetch employees list
  getsubTasks(): Observable<SubTasks[]> {
    //return this.http.get<subTasks[]>(this.api + '/subTasks')
    return this.http.get<SubTasks[]>("../../assets/SubTask.json")
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
    
  }

  // HttpClient API post() method => Create employee
  createsubTasks(subTasks): Observable<SubTasks> {
    return this.http.post<SubTasks>(this.api + '/subTasks', JSON.stringify(subTasks), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API put() method => Update employee
  updatesubTasks(id, subTasks): Observable<SubTasks> {
    return this.http.put<SubTasks>(this.api + '/subTasks/' + id, JSON.stringify(subTasks), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method => Delete employee
  deletesubTasks(id){
    return this.http.delete<SubTasks>(this.api + '/subTasks/' + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Error handling 
  handleError(error) {
     let errorMessage = '';
     if(error.error instanceof ErrorEvent) {
       // Get client-side error
       errorMessage = error.error.message;
     } else {
       // Get server-side error
       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
     }
     window.alert(errorMessage);
     return throwError(errorMessage);
  }

}


