
  import { Injectable } from "@angular/core";
  import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
  import { Opco } from "../model/opco";
  import { environment } from './../../environments/environment.prod';
  import { ToastrService } from 'ngx-toastr';
  
@Injectable({
  providedIn: 'root'
})
export class OpcoService {

    private api = environment.apiUrl;
    
  constructor(
    private http: HttpClient,
    private toastr: ToastrService) { }
      
  getAllOpco() {
    const path = `${this.api}/opcos/`;
    //const path = environment.apiUrl + '/regions';
    return this.http.get<Opco[]>(path);
  }

  getOpco(id: string) {
    const path = `${this.api}/opcos?opcoId=${id}`;
    return this.http.get<Opco>(path);
  }

  createOpco(opco: Opco) {
    const path = `${this.api}/createopco`; 
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    this.showSuccess();
    return this.http.post(path, JSON.stringify(opco), httpOptions);
  }

  updateOpco(opco: Opco) {
    const path = `${this.api}/updateregion/${opco.opcoId}`;
    this.showSuccess();
    return this.http.put<Opco>(path, opco);
  }

  deleteOpco(opcoId: string) {
    const path = `${this.api}/regions/${opcoId}`;
    this.showSuccess();
    return this.http.delete(path);
  }
  
  showSuccess() {
    this.toastr.success('Successfully', 'Opco Configured!',{
      timeOut:1500, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
  
  showError() {
    this.toastr.error('Error', 'Opco Change Failure!',{
      timeOut:2000, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
}
