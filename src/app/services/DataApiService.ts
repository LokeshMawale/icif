import { UsersComponent } from './../Main/users/users.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from "rxjs/internal/observable";
import { User } from "../model/user";
import { IdToken } from "../model/IdToken";

@Injectable({
  providedIn: 'root'
})
export class DataApiService {
    
  baseURL: string = "http://localhost:8080/";
  IdToken: IdToken;
  users: Observable<any>;
  user: Observable<any>;

  constructor(private http: HttpClient) {}
  getAllUsers(){
      const url_api = this.baseURL + 'users'; 
//      var reqHeader = new HttpHeaders({ 
  //      'Content-Type': 'application/json',
    //    'Authorization': 'Bearer  '+ JSON.parse(localStorage.getItem('access_token')),
    // });
    console.log(url_api);
    return this.http.get(url_api); //,{ headers: reqHeader});
  }

  getUserbyid(id: string){
    const url_api = this.baseURL +'{id}';
    console.log(url_api);
    return (this.user = this.http.get(url_api));
  }

/*getUsersList(){
  var reqHeader = new HttpHeaders({ 
      'Content-Type': 'application/json',
      'Authorization': 'Bearer  '+ JSON.parse(localStorage.getItem('access_token')),
   });
   return this.http.get(this.baseURL + 'users', { Headers: this.headers });
  }
}
*/
}