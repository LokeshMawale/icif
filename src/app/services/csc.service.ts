import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CscService {

  apiBaseUrl = 'http://localhost:8080';
  
  constructor(private http: HttpClient) { }

  getRegions() {
    return this.http.get(`${this.apiBaseUrl}/regions`).pipe(
      catchError(this.handleError)
    );
  }

  getAreas(areaId: number) {
    return this.http.get(`${this.apiBaseUrl}/areas/${areaId}`).pipe(
      catchError(this.handleError)
    );
  }

  getClusters(clusterId: number) {
    return this.http.get(`${this.apiBaseUrl}/clusters/${clusterId}`).pipe(
      catchError(this.handleError)
    );
  }

  getOpcos(opcoId: number) {
    return this.http.get(`${this.apiBaseUrl}/opcos/${opcoId}`).pipe(
      catchError(this.handleError)
    );
  }

  getPlants(plantId: number) {
    return this.http.get(`${this.apiBaseUrl}/plants/${plantId}`).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened. Please try again later.');
  }
}