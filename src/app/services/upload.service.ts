import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from  '@angular/common/http';  
import { map } from  'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class UploadService {
  // SERVER_URL: string = "https://file.io/";
  // SERVER_URL: string = "https://drive.google.com/drive/u/0/folders/1plqcQHkjAC0Yo_wP6e57Bu4qRo7I6Ajl";
  //SERVER_URL: string = "https://drive.google.com/drive/folders/1plqcQHkjAC0Yo_wP6e57Bu4qRo7I6Ajl?usp=sharing";
  SERVER_URL: string = "/folders/1plqcQHkjAC0Yo_wP6e57Bu4qRo7I6Ajl?usp=sharing";
    
    constructor(private httpClient: HttpClient) { }

    public upload(formData) {
      
      return this.httpClient.post<any>(this.SERVER_URL, formData, {  
          reportProgress: true,  
          observe: 'events'  
        });  
    }

  }
