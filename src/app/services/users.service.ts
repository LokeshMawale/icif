import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './../model/user';
import { environment } from 'src/environments/environment.prod';


@Injectable({
  providedIn: 'root'
})
export class UsersService {

  apiurl = environment.apiUrl;
  // for call to users pagination http://localhost:8080/users?size=20&page=1

  constructor(private http: HttpClient) { }

  findUserById(id: number): Observable<User> {
    return this.http.get<User>(this.apiurl + `/users/${id}`);
  }

  findAllRoles(): Observable<User[]> {
    return this.http.get(this.apiurl + '/roles')
      .pipe(
        map(res => res['payload'])
      );
  }

  findAllUsersRoles(id: number): Observable<User[]> {
    return this.http.get(this.apiurl + '/roles', {
      params: new HttpParams()
        .set('id', id.toString())
        .set('pageNumber', "0")
        .set('pageSize', "1000")
    }).pipe(
      map(res => res["payload"])
    );
  }

  findUsers(
    id: number, filter = '', sortOrder = 'asc',
    pageNumber = 0, pageSize = 10): Observable<User[]> {

    return this.http.get(this.apiurl+"/users", {
      params: new HttpParams()
        .set('id', id.toString())
        .set('filter', filter)
        .set('sortOrder', sortOrder)
        .set('pageNumber', pageNumber.toString())
        .set('pageSize', pageSize.toString())
    }).pipe(
      map(res => res["payload"])
    );
  }
}
