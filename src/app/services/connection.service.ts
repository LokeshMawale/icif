import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';
import { Contact } from '../model/contact';

@Injectable({
providedIn: 'root'
})
export class ConnectionService {
    apiurl = environment.apiUrl;
    //url: string = 'https://emea-lh-cif.appspot.com/contact';

constructor(private http: HttpClient,
    private toastr: ToastrService) { }

sendMessage(Contact: Contact) {
  this.showSuccess();
  return this.http.post(this.apiurl + "/contact",
  JSON.stringify(Contact),
  { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text' }).pipe(
      map(
    (response)=>{
        if (response){ 
        console.log(response);
        return response;
        }
    },
    (error: any) => {
        console.log(error);
        this.showError();
        this.toastr.show (error.name + ' ' +  error.message);   
        return error;
        })
  );
}

//popup messages toast on screen:

showSuccess() {
    this.toastr.success('Successfully', 'Email sent!',{
      timeOut:1500, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
  
  showError() {
    this.toastr.error('Error', 'Email sent Failure!',{
      timeOut:2000, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
}