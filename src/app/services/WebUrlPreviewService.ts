import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
  })
  export class WebUrlPreviewService {
    private requestHeaders: HttpHeaders;
  
    constructor(private http: HttpClient) {}
  
     headRequest(url: string):Observable<any> {
      return this.http.head(url, {observe: 'response'}); //is observe property necessary to make this http call? If not you can remove it.
    }
  }