import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { User } from '../model/user';
import { HttpClient, HttpErrorResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from './../../environments/environment.prod';
import { ToastrService } from 'ngx-toastr';
import { AddDialogComponent } from '../main/users/dialogs/add/add.dialog.component';
import { retry, catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  

  //private searchURL : String  = 'https://test-search-dot-emea-lh-cif.appspot.com';

   private searchURLTest : String  = 'https://test-search-dot-emea-lh-cif.appspot.com';

   private searchURLProd : String = 'https://corp-search-doc-library.appspot.com'

   private searchURL : String = 'https://corp-search-doc-library.appspot.com';
  

   increaseDocumentView(id: any,viewCount : any) {
    let path = `${this.apiurl}/updateMapWithView/`+id+`/`+viewCount;
    if (window.location.hostname == 'localhost') {
       path = `http://localhost:8080/updateMapWithView/`+id+`/`+viewCount;
     
    }
    
    return this.httpClient.get<any>(path);
  }

  getMapStatus() {
    let path = `${this.apiurl}/getMapStatus`;
     if (window.location.hostname == 'localhost') {
        path = `http://localhost:8080/getMapStatus`;
      }
      return this.httpClient.get<any>(path);

  }
 
  
  increaseDocumentLike(documentId: any,likeCount : any) {
    let path = `${this.apiurl}/updateMapWithLike/`+documentId+`/`+likeCount;
    if (window.location.hostname == 'localhost') {
       path = `http://localhost:8080/updateMapWithLike/`+documentId+`/`+likeCount;
     
    }
    return this.httpClient.get<any>(path);
  }
  
  post_comment(data) {
    let email = localStorage.getItem('email');
    let userName = localStorage.getItem('userName');
    
    data.userId = email;
    data.userName = userName;
    let params = new HttpParams().set('email', email).set('name',userName);
    this.getSearchURL();
    return this.httpClient.post(this.searchURL+'/emea/saveComment',data);
  }

  retrieveDocumentLikedBy(id: any) {
    let email = localStorage.getItem('email');
    let userName = localStorage.getItem('userName');
    let params = new HttpParams().set('email', email).set('name',userName);
    this.getSearchURL();
    return this.httpClient.get<any>(this.searchURL+'/emea/listLikedBy?docId='+id,{ params: params});
  }
  
  
  document_likes(like_data) {
    let email = localStorage.getItem('email');
    let userName = localStorage.getItem('userName');
    

    let params = new HttpParams().set('email', email).set('name',userName);
    this.getSearchURL();
    return this.httpClient.post(this.searchURL+'/emea/likeDocument',like_data,{ params: params});
  }
  retrieve_Documente_Data(id: any) {
    let email = localStorage.getItem('email');
    let userName = localStorage.getItem('userName');
    

    let params = new HttpParams().set('email', email).set('name',userName);
    this.getSearchURL();
    return this.httpClient.get<any>(this.searchURL+'/emea/viewDocument/' + id,{ params: params});
  }


  public csdlSearchString:string = "";
  public csdlDSrcFilter:string = "";
  public csdlDTypeFilter:string = "";


  searchCIFDocuments(searchTermValue : any) {
    this.getSearchURL();
    
    var dataSource = '';
    var docType = '';
   /* if(this.csdlDSrcFilter){
      dataSource =  this.csdlDSrcFilter;
    }
    if(this.csdlDTypeFilter){
      docType = this.csdlDTypeFilter
    }*/
    if(searchTermValue!=''){
      var url = this.searchURL+`/emea/search?searchTerm=${searchTermValue}&dataSource=`+dataSource+`&docType=`+docType;
    }
    let headers = new HttpHeaders();
    
    let email = localStorage.getItem('email');
    let userName = localStorage.getItem('userName');
    localStorage.setItem("searchTerm",searchTermValue);

    let params = new HttpParams().set('email', email).set('name',userName);
    return this.httpClient.get<any>(url,{ params: params});
  }
  

  searchCIFDocumentsFilter(name:string, dataSrc:string, docType:string) {
    this.csdlSearchString = name;
    this.csdlDSrcFilter = dataSrc;
    this.csdlDTypeFilter = docType;
    let email = localStorage.getItem('email');
    let userName = localStorage.getItem('userName');

    let params = new HttpParams().set('email', email).set('name',userName);
    this.getSearchURL();
    var url = this.searchURL+`/emea/search?searchTerm=${name}&dataSource=${dataSrc}&docType=${docType}`;
    return this.httpClient.get<any>(url,{ params: params});
  }

  getAllSearchResults(){
    let path = `${this.apiurl}/getAllSearchResults`;
    if (window.location.hostname == 'localhost') {
      if (window.location.hostname == 'localhost') {
        path = `http://localhost:8080/getAllSearchResults`;
      }
    }
    return this.httpClient.get<any>(path);

  }

  dataChange: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);

  // Temporarily stores data from dialogs
  dialogData: any;
  apiurl = environment.apiUrl;
  items: any;
  AddDialogComponent: AddDialogComponent;
  AllDataObj: any;
  AllData_Arr: string;

  constructor(private httpClient: HttpClient,
    private toastr: ToastrService) {}

  get data(): User[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  /** CRUD METHODS http://localhost:8080/users?size=1&page=1  */

  getAllUsers(size: number, page: number) {
    this.httpClient.get<User[]>(this.apiurl + `/users?`, {
      params: new HttpParams()
        //.set('size', <any>size)
        //.set('page', <any>page)
        .set('size', size.toString())
        .set('page', page.toString())

    }).pipe(retry(3), catchError(this.handleError))
      .subscribe((data: any) => {
        //}).subscribe(data => {
        this.dataChange.next(data);

        console.log(this.data);
      },
        (error: HttpErrorResponse) => {
          console.log(error.name + ' ' + error.message);
        });
  }

  getUsers(size: number, page: number): Observable<User> {
    // return undefined;
    const href = 'http://localhost:8080/users';
    const requestUrl = `${href}/users?size=${size}&page=${page + 1}`;
    return this.httpClient.get<User>(requestUrl);
  }

  public sendGetRequest() {
    return this.httpClient.get(this.apiurl + `/users?`,
      {
        params: new HttpParams(
          { fromString: "_page=1&_limit=20" }), observe: "response"
      }).pipe(retry(3),
        catchError(this.handleError),
        tap(res => {
          //console.log(res.headers.get('Link'));
          //this.parseLinkHeader(res.headers.get('Link'));
        }));
  }
  /*
    parseLinkHeader(header) {
      if (header.length == 0) {
        return;
      }
  
      let parts = header.split(',');
      var links = {};
      parts.forEach(p => {
        let section = p.split(';');
        var url = section[0].replace(/<(.*)>/, '$1').trim();
        var name = section[1].replace(/rel="(.*)"/, '$1').trim();
        links[name] = url;
  
      });
  
    }
  */
  //let params = new HttpParams();
  //params = params.append('size=','200');
  //params = params.append('page=','200');
  //this.httpClient.get<User[]>(this.apiurl + `/users?`,{params})
  /*
      this.httpClient.get<User[]>(this.apiurl + `/users?`,{
          params: new HttpParams()
            .set('size', "2000")
            .set('page', "1")
            .set('limit', "10")
          }).pipe(retry(3),catchError(this.handleError))
          .subscribe((data: any) => {
          //}).subscribe(data => {
          this.dataChange.next(data.content);
          console.log(this.data);
        },
          (error: HttpErrorResponse) => {
            console.log(error.name + ' ' + error.message);
          });
          
          }
          
          */
  // getAllUsers(): void{
  //   this.httpClient.get<User[]>(this.apiurl+"/users").subscribe((data: User[])=> {
  //     console.log(data);
  //     this.dataChange.next(data);
  //     //this.AllDataObj = data;
  //     //this.AllData_Arr= JSON.stringify(this.AllDataObj);
  //     //console.log(this.AllData_Arr);
  //     },
  //     (error: HttpErrorResponse) => {
  //     console.log (error.name + ' ' + error.message);
  //     });
  // }

  // DEMO ONLY, you can find working methods below
  addUser(user: User): void {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    this.httpClient.post(this.apiurl + "/createuser", user, options).subscribe(user => {
      this.dialogData = user;
      this.showSuccess();
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message)
        this.showError();
        this.toastr.show (err.name + ' ' +  err.message);  
      });
  }

  updateUser(user: User): void {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    this.httpClient.put(this.apiurl + "/updateuser/" + user.id, user, options).subscribe(data => {
      this.dialogData = user;
      this.showSuccess();
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message);
        this.showError();
        this.toastr.show (err.name + ' ' +  err.message);
      }
    );
  }

  deleteUser(id: number): void {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    this.httpClient.delete(this.apiurl + "/deleteuser/" + id, options).subscribe(data => {
      console.log("this user id is: " + JSON.stringify(id));
      console.log("delete id: " + id);
      this.showSuccess();
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message);
        this.showError();
        this.toastr.show (err.name + ' ' +  err.message);
      }
    );
  }


  //updateUser(user: User): void {    this.dialogData = user;  }
  //deleteUser(id: number): void {    console.log(id);  }

  getConfig(): Observable<User[]> {
    return this.httpClient.get<User[]>(this.apiurl + "/users");
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  pauseProcess(processTransactID: any) {
    let path = `${this.apiurl}/pauseProcessTransaction`;
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/pauseProcessTransaction`
    }
    let userName = localStorage.getItem("userName");
    var obj = {
      "transactionId": processTransactID,
      "PausedBy": userName,
    }
    return this.httpClient.post<any>(path, obj);
  }

  resumeProcess(processTransactID: any) {
    let path = `${this.apiurl}/resumeProcessTransaction`;
    if (window.location.hostname == 'localhost') {
      path = `http://localhost:8080/resumeProcessTransaction`
    }
    let userName = localStorage.getItem("userName");
    var obj = {
      "transactionId": processTransactID,
      "ResumeBy": userName,
    }
    return this.httpClient.post<any>(path, obj);
  }

  getTasksForOverView(processID) {
    let plantData: any = JSON.parse(localStorage.getItem(localStorage.getItem('plantName')));
    let userID = parseInt(localStorage.getItem("userID"));
    let plantID = parseInt(localStorage.getItem("plantID"));
    if (processID) {
      let path = `${this.apiurl}/getTasksForOverView/user/` + userID + `/plant/` + plantID + `/process/` + processID;
      if (window.location.hostname == 'localhost') {
        path = `http://localhost:8080/getTasksForOverView/user/` + userID + `/plant/` + plantID + `/process/` + processID;
      }
      let userName = localStorage.getItem("userName");
      return this.httpClient.get<any>(path);
    }

  }
  
  showSuccess() {
    this.toastr.success('Successfully', 'User Configured!',{
      timeOut:1500, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
  
  showError() {
    this.toastr.error('Error', 'User Change Failure!',{
      timeOut:1500, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }

  getSearchURL(){
    this.searchURL = this.searchURLProd;
    if (window.location.hostname == 'localhost' || window.location.hostname == 'emea-lh-cif.appspot.com' || window.location.hostname == 'dev-dot-emea-lh-cif.appspot.com') {
      this.searchURL = this.searchURLTest;
    }
  }

  bulkUpdateUserData() {
    let path = `${this.apiurl}/bulkUpdateUserData`;
    if (window.location.hostname == 'localhost') {
      path = 'http://localhost:8080/bulkUpdateUserData'
    }
     this.httpClient.get(path,{responseType: 'text'}).subscribe(data => {
      if(data && data == 'Process Already Running'){
        this.toastr.error('Please try after sometime', 'Synchronization Alredy Running', {
          timeOut: 3500, closeButton: true, progressBar: true, positionClass: 'toast-center-center'
        });
      }
      console.log("success");
    },
      (err: HttpErrorResponse) => {
        if(err.error && err.error.text && err.error.text == 'Process Already Running'){
          this.toastr.error('Please try after sometime', 'Synchronization Alredy Running', {
            timeOut: 3500, closeButton: true, progressBar: true, positionClass: 'toast-center-center'
          });
        }
        console.log("error");
      }
    );;
  }

}