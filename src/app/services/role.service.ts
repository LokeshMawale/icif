import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { BehaviorSubject, throwError } from 'rxjs';
import { Role } from '../model/role';
import { HttpClient, HttpErrorResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { retry, catchError, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class RoleService {
  //private readonly API_URL = 'https://api.github.com/repos/angular/angular/issues';

  private api = environment.apiUrl;
  private apiurl = environment.apiUrl;
  private readonly API_URL = 'http://localhost:8080/users';

  dataChange: BehaviorSubject<Role[]> = new BehaviorSubject<Role[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor(private httpClient: HttpClient,
    private toastr: ToastrService) { }

  get data(): Role[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  /** CRUD METHODS */
  getAllRoles(): void {
    const path = `${this.api}/roles/`;
    this.httpClient.get<Role[]>(path).subscribe(data => {
      this.dataChange.next(data);
    },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });
  }


  public sendGetRequest() {
    return this.httpClient.get(this.apiurl + `/roles?`,
      {
        params: new HttpParams(
          { fromString: "_page=1&_limit=20" }), observe: "response"
      }).pipe(retry(3),
        catchError(this.handleError),
        tap(res => {
          //console.log(res.headers.get('Link'));
          //this.parseLinkHeader(res.headers.get('Link'));
        }));
  }

  /*
  parseLinkHeader(header) {
    if (header.length == 0) {
      return;
    }

    let parts = header.split(',');
    var links = {};
    parts.forEach(p => {
      let section = p.split(';');
      var url = section[0].replace(/<(.*)>/, '$1').trim();
      var name = section[1].replace(/rel="(.*)"/, '$1').trim();
      links[name] = url;

    });
  }
*/
  getAllRole() {
    const path = `${this.apiurl}/roles/`;
    //const path = environment.apiUrl + '/regions';
    return this.httpClient.get<Role[]>(path);
  }

  addRole(role: Role): void {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    this.httpClient.post(this.apiurl + "/createrole", role, options).subscribe(data => {
      this.dialogData = role;
      this.showSuccess();
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message);
        this.showError();
        this.toastr.show (err.name + ' ' +  err.message);   
      });
  }

  updateRole(role: Role): void {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    this.httpClient.put(this.apiurl + "/updaterole/" + role.id, role, options).subscribe(data => {
      this.dialogData = role;
      this.showSuccess();
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message);
        this.showError();
        this.toastr.show (err.name + ' ' +  err.message);
      }
    );
  }

  deleteRole(id: number): void {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };

    this.httpClient.delete(this.apiurl + "/deleterole/" + id, options).subscribe(data => {
      console.log("this id is: " + JSON.stringify(id));
      console.log("delete id: " + id);
      this.showSuccess();
    },
      (err: HttpErrorResponse) => {
        console.log(err.name + ' ' + err.message);this.showError();
        this.toastr.show (err.name + ' ' +  err.message);
        // this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
      }
    );
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      this.showError();
      this.toastr.show (errorMessage);
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  showSuccess() {
    this.toastr.success('Successfully', 'Role Configured!',{
      timeOut:1500, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }
  
  showError() {
    this.toastr.error('Error', 'Role Change Failure!',{
      timeOut:2000, closeButton:true, progressBar:true, positionClass: 'toast-center-center'});
  }

}



