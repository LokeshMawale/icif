import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
// import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider } from "angularx-social-login";
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {  GoogleLoginProvider,  FacebookLoginProvider,  AmazonLoginProvider} from 'angularx-social-login';
import { LoginComponent } from './login/login.component';
import { MainComponent } from './main/main.component';
import { PlantIdservice } from './services/PlantIdservice';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { NgwWowModule } from 'ngx-wow';
import { MatDividerModule } from '@angular/material/divider';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatTreeModule } from '@angular/material/tree';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatRippleModule, MatNativeDateModule } from '@angular/material/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatStepperModule } from '@angular/material/stepper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatDialogModule } from '@angular/material/dialog';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatTableModule } from '@angular/material/table';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './services/authInterceptor';
import { AuthService2 } from './services/auth.service';
import { JwtModule } from '@auth0/angular-jwt';
import { UsersService } from './services/users.service';
import { TaskDialogComponent } from './main/task/task-dialog/task-dialog.component';
import { CookieService } from 'ngx-cookie-service';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { JwPaginationModule } from 'jw-angular-pagination';
import { ChartsModule } from 'ng2-charts';
import { environment } from 'src/environments/environment.prod';
import { StarRatingModule } from 'angular-star-rating';
import { MatSortModule } from '@angular/material/sort';
// import { API_KEY, GoogleSheetsDbService } from 'ng-google-sheets-db';

export function getToken() {
  return localStorage.getItem('token');
 }
/*let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("155477220882-kt5lb9r32dc05gs02663fjas1blbfnbf.apps.googleusercontent.com")
  }
]);

export function socialConfigs() {
  return config;
}
*/
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-center-center',
      preventDuplicates: true
    }),
    MDBBootstrapModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AppRoutingModule,
    SocialLoginModule,
    HttpClientModule,
    NgwWowModule,
    MatSidenavModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatTreeModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatToolbarModule,
    MatRippleModule,
    MatGridListModule,
    MatTreeModule,
    MatStepperModule,
    MatSortModule,
    MatInputModule,
    MatBadgeModule,
    MatDialogModule,
    DragDropModule,
    MatTableModule,
    MatSnackBarModule,
    HttpClientModule,
    MatProgressBarModule,
    MatCardModule,
    MatChipsModule,
    MatCheckboxModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    FlexLayoutModule,
    MatNativeDateModule,
    MatDatepickerModule,
    JwPaginationModule,
    ChartsModule,
    StarRatingModule.forRoot(),
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 20,
      innerStrokeWidth: 20,
      outerStrokeColor: '#29c17b',
      innerStrokeColor: '#C7E596',
      animationDuration: 300,
    }),
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('token');
        },
        allowedDomains: environment.whitelist,
        // disallowedRoutes: ['localhost:5000/api/auth']
        // whitelistedDomains: environment.whitelist,
        /* tokenGetter: function tokenGetter() {
          return localStorage.getItem('token');
        },
        whitelistedDomains: environment.whitelist,*/
        // whitelistedDomains: ['localhost:8080', 'emea-lh-cif.appspot.com', 'corp-search-doc-library.appspot.com', 'cem-icif-prod.ew.r.appspot.com']
        // ,blacklistedRoutes: ['http://localhost:3000/auth/login']
      }
    })
  ],

  exports: [ChartsModule],
  providers: [PlantIdservice, AuthService2, AuthInterceptor, UsersService,
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              'clientId'
            ),
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('clientId'),
          },
          {
            id: AmazonLoginProvider.PROVIDER_ID,
            provider: new AmazonLoginProvider(
              'clientId'
            ),
          },
        ],
      } as SocialAuthServiceConfig,
    }
    // ,
    // CookieService,
    // {
    //   provide: API_KEY,
    //   useValue: environment.googleSheetsApiKey,
    // },
    // GoogleSheetsDbService,
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: AuthInterceptor,
    //   multi: true
    // }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [TaskDialogComponent]
})
export class AppModule { }
