import { Component, OnInit } from '@angular/core';
//import { AuthService } from "angularx-social-login";
//import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider } from "angularx-social-login";

import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";


import { SocialUser } from "angularx-social-login";
import { Router, ActivatedRoute } from '@angular/router';
import { SocialloginService } from '../Services/sociallogin.service';  
import { Socialusers } from '../model/socialusers';
import { IdToken} from '../model/IdToken';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthService2 } from '../services/auth.service';
import { environment } from './../../environments/environment.prod';
import {CookieService } from 'ngx-cookie-service'
import { trigger, transition, style, animate, state } from '@angular/animations';
const googleLogoURL = 
"https://raw.githubusercontent.com/fireflysemantics/logo/master/Google.svg";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [
    trigger('divState',[
      state('greybox',style({
        //backgroundColor:'grey',
        opacity:"1",
        transform:'translateX(0) scale(1)'
      })),
      state('whitebox',style({
        //backgroundColor:'white',
        opacity:"0.1",
        transform:'translateX(0) scale(3)'
      })),
      transition('greybox => whitebox',animate(500)),
      transition('whitebox => greybox',animate(500)),
      //transition('redbox <=> yellowbox',animate(500)),
    ])
  ]
})

export class LoginComponent implements OnInit {
  private api = environment.apiUrl;
  stateNames ='greybox';
  response: any;
  socialusers=new Socialusers();  
  IdToken=new IdToken();
  //tokenInterceptor = new AuthInterceptor;
  
  public user: SocialUser;
  public loggedIn: boolean;
  public errorMessage: string = '';
  loginRedirection : Boolean = false;
  
  constructor(
    //public OAuth: AuthService,  
    private OAuth: SocialAuthService,
    private SocialloginService: SocialloginService,
    private matIconRegistry: MatIconRegistry,
    private router: Router,
    //public jwtHelper: JwtHelperService,
    private domSanitizer: DomSanitizer,
    private AuthService2: AuthService2,
    private cookieService :CookieService ,
    private route: ActivatedRoute) {
                this.matIconRegistry.addSvgIcon(
                        "logo",      
                this.domSanitizer.bypassSecurityTrustResourceUrl(googleLogoURL));
                if(window.location.search && window.location.search.split("=") && window.location.search.split('=')[1]){
                  this.loginRedirection = true;
                  let queryParamas = window.location.search;
                  if(queryParamas.indexOf("container") >= 0){
                    let decodedURL = decodeURIComponent(window.location.search.split('=')[1]);
                    this.router.navigate([decodedURL]);
                  }else{
                    localStorage.clear();
                    this.cookieService.deleteAll();
                    let token = window.location.search.split('=')[1];
                    localStorage.setItem('token', token);
                    cookieService.set('token', token);
                    this.router.navigate(['/main/main-detail']);   
                  }
                }
             
  }
   
  ngOnInit() {  
    //if(window.location.search && window.location.search.split("=") && window.location.search.split('=')[1]){
      //let token = window.location.search.split('=')[1];
      //localStorage.setItem('token', token);
      //this.router.navigate(['/main']);
    //}
    console.log();
  }  

  //for google validation directly without backend:
  /*signInWithGoogle(): void {
    this.OAuth.signIn(GoogleLoginProvider.PROVIDER_ID);
  }  
  signOut(): void {
    this.authService.signOut();
  }
  */

  public socialSignIn(socialProvider: string) {  
    let socialPlatformProvider;
    if (socialProvider === 'google') {  
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }  
    this.OAuth.signIn(socialPlatformProvider).then(socialusers => {  
      console.log(socialProvider, socialusers);  
      console.log("User Info"+socialusers); 
     // localStorage.setItem('currentUser', JSON.stringify({ token: IdToken, name: name }));
      localStorage.setItem('currentUser', JSON.stringify({ token:socialusers.idToken,name:socialusers.name}));
      this.router.navigate(['/main']);
      this.Savesresponse(socialusers);
      //   quitar para salvar la informacion de usuario en la api     this.Savesresponse(socialusers);  
    });  
  }  
 
  backendlogin(){
    let uri = `${this.api}/oauth2/authorize/google`;
    if(window.location.hostname == 'localhost'){
      uri = 'http://localhost:8080/oauth2/authorize/google?env=dev';
    }
    window.location.href = uri;
    this.switchState();
  }
  
  // quitar para salvar la informacion de usuario en la api     
  Savesresponse(socialusers: SocialUser) {
    this.SocialloginService.Savesresponse(socialusers).subscribe((res: any) => {  
      debugger;
      console.log(res);
      this.socialusers=res;
      this.response = res.userDetail;  
      localStorage.setItem('socialusers', JSON.stringify( this.socialusers));
      console.log(localStorage.setItem('socialusers', JSON.stringify(this.socialusers)));  
      this.router.navigate(['/main']);
      })  
  }

/*  logAnimation($event) {
    console.log(`${this.position} animation ${$event.phaseName}`)
  }*/

  switchState(){
    this.stateNames = this.stateNames === 'greybox' ? 'whitebox' : 'greybox'
   }  

}
