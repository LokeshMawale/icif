import { animate, state, style, transition, trigger, AnimationBuilder, AnimationPlayer } from '@angular/animations';
import { ElementRef, HostListener } from '@angular/core';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-processflow',
  templateUrl: './processflow.component.html',
  styleUrls: ['./processflow.component.scss'],
  animations: [
    trigger("openClose", [
      state(
        "open",
        style({
          height: "800px",
          width: '*',
//          opacity: 1,
          //backgroundColor: "grey",
          //transform: 'scale(4.9) translateY(55vh) translateX(38.7vw)',
          transform: '*',
          offset: 1.0
        })
      ),
      state(
        "closed",
        style({
          //opacity: 0.5,
          //backgroundColor: "grey",
          width: '*',
          transform: 'scale(1) translateY(0) translateX(0)',
          offset: 1.0
        })
      ),
      transition("* => closed", [animate("1s")]),
      transition("* => open", [animate("0.5s")])
    ]),
  ],
})

export class ProcessflowComponent implements OnInit {
  
  backgroundColor: string = "rgba(0,0,0,0)";
  overflow: string = "hidden";
  limitzoom: number = 10;
  autozoomout= false;
  isOpen = false;
  isAssessment = false;
  state: string = 'open';
  public innerWidth: any;  
  
  @ViewChild('myPinch', { static: false }) myPinch;
  
  constructor() { }

  ngOnInit(): void {
    this.innerWidth = window.innerWidth;
    //console.log((this.innerWidth-200)/6500*100);
    console.log(this.innerWidth);
  }
  
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }
  
  togglePreparation() {
    this.isOpen = !this.isOpen;
  }

  toggleAssessment() {
    this.isAssessment = !this.isAssessment;
    //this.state = (this.state === 'open' ? 'closed' : 'open');
  }

//  toggleAssessment() {    this.isAssessment = !this.isOpen;  }

  setZoom(value: number) {
    this.myPinch.setZoom({ scale: value });
  }

  zoomIn() {
    this.myPinch.zoomIn();  
  }
  
  zoomOut() {
    this.myPinch.zoomOut();
  }

  setTransform(properties: { x?: number, y?: number, scale?: number }) {
    this.myPinch.setTransform(properties);
  }

}  
