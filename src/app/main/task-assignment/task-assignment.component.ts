import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { Router, ActivatedRoute, Params } from '@angular/router';

export interface PeriodicElement {
  name: string;
  position: number;

}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Engagement with Industrial Director and Plant Manager'},
  {position: 2, name: 'Conduct Change Readiness survey'},
  {position: 3, name: 'Send Request to Service Provider'},
  {position: 4, name: 'Conduct Gap Analyzer assessment'},
  {position: 5, name: 'CIF video Intro'},
  
];


@Component({
  selector: 'app-task-assignment',
  templateUrl: './task-assignment.component.html',
  styleUrls: ['./task-assignment.component.scss']
})
export class TaskAssignmentComponent implements OnInit {

  displayedColumns: string[] = ['position', 'name','actions'];

  searchTerm :any;

  public selectprocesses = [
    { "id": 1, "name": "Preparation" },
    { "id": 2, "name": "Assessment" },
    { "id": 3, "name": "Implementation" },
    { "id": 4, "name": "Follow Up" },
    { "id": 5, "name": "Sustainability" }
  ]

  constructor( private router: Router,private routeParams: ActivatedRoute) {

    var plantCode = localStorage.getItem("code");
    let routeToSearch = this.routeParams.snapshot.queryParamMap.get('routeToSearch');
    let routeToWorkflow = this.routeParams.snapshot.queryParamMap.get('routeToWorkflow');
    let routeToTask = this.routeParams.snapshot.queryParamMap.get('routeToTask');
    if(routeToSearch && routeToSearch == 'yes'){
      this.searchTerm = this.routeParams.snapshot.queryParamMap.get('searchValue');
      this.router.navigate(['/main/search'], { queryParams: { searchValue: this.searchTerm ,plantCode:plantCode} });

    }else if(routeToWorkflow && routeToWorkflow == 'yes'){
      this.router.navigate(['/main/workflow'], { queryParams: {plantCode:plantCode} });  
    }else if(routeToTask && routeToTask == 'yes'){
      this.router.navigate(['/main/task'], { queryParams: {plantCode:plantCode} });  
    }else{
       this.router.navigate(['/main/main-detail'], { queryParams: {plantCode:plantCode} });  
    }
   }

  ngOnInit(): void {

  }
 
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  select(processid: number) {
 
  }

}
