import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectWavesComponent } from './ProjectWaves.component';

describe('WorkflowHistoryComponent', () => {
  let component: ProjectWavesComponent;
  let fixture: ComponentFixture<ProjectWavesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectWavesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectWavesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
