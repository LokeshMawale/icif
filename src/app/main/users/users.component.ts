
import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, ChangeDetectorRef, HostListener } from '@angular/core';
import { UserService } from '../../services/user.service';
import { RoleService } from '../../services/role.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { User } from '../../model/User';
import { Role } from '../../model/Role';
import { Plant } from './../../model/plant';
import { Plants, Roles } from './../../model/user';
import { DataSource } from '@angular/cdk/collections';
import { AddDialogComponent } from './dialogs/add/add.dialog.component';
import { EditDialogComponent } from './dialogs/edit/edit.dialog.component';
import { DeleteDialogComponent } from './dialogs/delete/delete.dialog.component';
import { BehaviorSubject, fromEvent, merge, Observable, Subject, Subscription } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { BulkUpdateComponent } from './dialogs/bulk-update/bulk-update.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent implements OnInit, AfterViewInit, OnDestroy {

  //components of paggination for angular material table.

  displayedColumns = ['id', 'Name', 'email', /*'imageUrl',*/ 'createDate', 'sapUserId', 'roles', 'plants', 'actions'];
  displayedInitialColumns = ['id', 'Name', 'email', /*'imageUrl',*/ 'createDate', 'sapUserId', 'roles', 'plants', 'actions'];
  displayedColumnForMobilesPhones: string[] = ['id', 'Name', 'email', 'actions'];
  isColumnsMobile: boolean;
  innerWidth: any;
  exampleDatabase: UserService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  id: number = 0;
  newid: number;
  users: User[] = [];
  user: User[]= [{id: 1, name:"test", email: "XXXXreplaceXXXX @holcim.com"}];
  roles: Role[] = [];
//  rolesasstring = this.roles.join(', ');
  plants: Plant[] = [];
  pageSize = 3000; //change to 3000 renombrar  pageSize
  loading: any;
  roleslist: any[] = ['loaded without backend', 'User', 'Admin', 'Cif-Leader', 'site_prod_shift'];
  destroy$: Subject<boolean> = new Subject<boolean>();
  filterValue :any = '';
  private paramsubscriptions: Subscription[] = [];

  constructor(
    public httpClient: HttpClient,
    public dialog: MatDialog,
    public userService: UserService,
    public roleService: RoleService,
    private toastr: ToastrService,
    private cdr: ChangeDetectorRef) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  dataSourcespin = null;

  ngOnInit() {
    this.loadData();
    this.loading = false;
    this.userService.sendGetRequest().pipe(takeUntil(this.destroy$)).subscribe((res: HttpResponse<User[]>) => {
      //console.log(res);
      this.users = res.body;
    });

    this.roleService.sendGetRequest().pipe(takeUntil(this.destroy$)).subscribe((res: HttpResponse<Role[]>) => {
      //console.log(res);
      this.roles = res.body;
    });

    this.innerWidth = window.innerWidth;
    if(this.innerWidth < 850 && !this.isColumnsMobile) {
      //console.log(this.innerWidth);
      this.displayedColumns = this.displayedColumnForMobilesPhones;
      this.isColumnsMobile = true;

    } else if(this.innerWidth >= 850 && this.isColumnsMobile) {

      this.displayedColumns = this.displayedInitialColumns;
      this.isColumnsMobile = false;
    }

  }

  ngAfterViewInit(): void {
    this.cdr.detectChanges;
    this.innerWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.innerWidth = window.innerWidth;
    if(event.target.innerWidth < 930 && !this.isColumnsMobile) {

      this.displayedColumns = this.displayedColumnForMobilesPhones;
      this.isColumnsMobile = true;

    } else if(event.target.innerWidth >= 930 && this.isColumnsMobile) {

      this.displayedColumns = this.displayedInitialColumns;
      this.isColumnsMobile = false;
    }
  }

/*  onPageChanged(e) {
    //let start = (e.pageIndex * e.pageSize) + 1;
    //let end = (start + e.pageSize) - 1;
    //this.paginator.pageSize = (this.paginator.pageSize) + 1;
    console.log(this.paginator.pageIndex);
    //this.userService.getAllUsers(size, page);
  }

  this.exampleDatabase.dataChange
    .subscribe(data => {
      this.dataSourcespin = data;
      this.loading = false;
    },
      error => this.loading = false
    );
    //this.loading = true;
}
*/

  refresh() {
    this.loadData();
    this.loading = true;
  }

  addNew(user?: User) {
    const dialogRef = this.dialog.open(AddDialogComponent, {
      data: { user: user }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside userService
        this.exampleDatabase.dataChange.value.push(this.userService.getDialogData());
        this.refreshTable();
        this.loadData();
      }
    });
  }

  startEdit(i: number, id: number, name: string, email: string, sapUserId: string, roles: Roles[], plants: Plants) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    //console.log(this.index);
    const dialogRef = this.dialog.open(EditDialogComponent, {
      data: { id: id, name: name, email: email, sapUserId: sapUserId, roles: roles, plants: plants }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside userService by id
        const foundIndex = this.exampleDatabase.dataChange.value.findIndex(x => x.id === this.id);
        // Then you update that record using data from dialogData (values you enetered)
        this.exampleDatabase.dataChange.value[foundIndex] = this.userService.getDialogData();
        // And lastly refresh table
        this.refreshTable();
        this.loadData();
      }
    });
  }

  deleteItem(i: number, id: number, name: string, email: string, sapUserId: string, roles: Roles[], plants: Plants) {
    this.index = i;
    this.id = id;
    //this.roles = roles;
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: { id: id, name: name, email: email, sapUserId: sapUserId, roles: roles, plants: plants }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.exampleDatabase.dataChange.value.findIndex(x => x.id === this.id);
        // for delete we use splice in order to remove single object from DataService
        this.exampleDatabase.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
      }
    });
  }

  private refreshTable() {
    // Refreshing table using paginator
    // Thanks yeager-j for tips
    // https://github.com/marinantonio/angular-mat-table-crud/issues/12
    this.paginator._changePageSize(this.paginator.pageSize);

    // If you don't need a filter or a pagination this can be simplified, you just use code from else block
    // OLD METHOD:
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    }
    // else {
    //   this.dataSource.filter = '';
    //   this.dataSource.filter = this.filter.nativeElement.value;
    // }
  }

  public synchUserData(){
    this.dialog.open(BulkUpdateComponent);
  }
  public loadData() {
    this.exampleDatabase = new UserService(this.httpClient, this.toastr);
    this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, this.sort);
    // if(this.filter.nativeElement.value){
    //   this.dataSource.filter = this.filter.nativeElement.value;
    // }
    fromEvent(this.filter.nativeElement, 'keyup')
      //.debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
    this.paramsubscriptions.forEach(subscription => subscription.unsubscribe());
  }
}

export class ExampleDataSource extends DataSource<User> {
  _filterChange = new BehaviorSubject('');
  pageSize: number = 3000;

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: User[] = [];
  renderedData: User[] = [];

  constructor(public _exampleDatabase: UserService,
    public _paginator: MatPaginator,
    public _sort: MatSort) {
    super();
    // Reset to the first page when the user changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
    this._paginator.pageSize = this.pageSize;
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<User[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];
    this._exampleDatabase.getAllUsers(this._paginator.pageSize, this._paginator.pageIndex);
    //this._exampleDatabase.getUsers(this._paginator.pageSize, this._paginator.pageIndex);
    return merge(...displayDataChanges).pipe(map(() => {
      // Filter data
      this.filteredData = this._exampleDatabase.data.slice().filter((user: User) => {
        const searchStr = (user.id + user.name + user.email + user.sapUserId + user.roles + user.plants).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      if(sortedData.length > 0){
        this._paginator.pageSize = 20;
        this.renderedData = sortedData.splice(startIndex,20);
        return this.renderedData;
      }
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;

    }
    ));
  }

  disconnect() { }

  /** Returns a sorted copy of the database data. */
  sortData(data: User[]): User[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';
      let propertyC: Date | string = '';

      switch (this._sort.active) {
        case 'id': [propertyA, propertyB] = [a.id, b.id]; break;
        case 'lastname': [propertyA, propertyB] = [a.name, b.name]; break;
        case 'email': [propertyA, propertyB] = [a.email, b.email]; break;
        //case 'photoURL': [propertyA, propertyB] = [a.imageUrl, b.imageUrl]; break;
        //case 'createdate': [propertyC, propertyC] = [a.createDate, b.createDate]; break;
        case 'sapUserId': [propertyA, propertyB] = [a.sapUserId, b.sapUserId]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      const valueC = isNaN(+propertyC) ? propertyC : +propertyC;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}
