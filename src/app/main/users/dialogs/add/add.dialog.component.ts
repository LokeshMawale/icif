import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { FormGroup, FormControl, Validators, NgModel } from '@angular/forms';
import { User } from '../../../../model/user';
import { Role } from './../../../../model/Role';
import { Plant } from './../../../../model/plant';
import { RoleService } from './../../../../services/role.service';
import { PlantService } from './../../../../services/plant.service';
import { formatDate } from '@angular/common';
import { ExampleDataSource } from './../../users.component';
import { Subscription } from 'rxjs';
import { MatOption } from '@angular/material/core';
import { MatSelect } from '@angular/material/select';

@Component({
  selector: 'app-add.dialog',
  templateUrl: './add.dialog.html',
  styleUrls: ['./add.dialog.scss'],
})

export class AddDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  name: String = "Name";
  idvalue: number;
  todayDate: Date = new Date();
  createDate: string = this.todayDate.toISOString();
  defaultemail: String = "@holcim.com";
  //roles = new FormControl();
  plants: Plant[] = [];
  roles: Role[] = [];
  emailVerified: string;
  allSelected=false;
  private paramsubscriptions: Subscription[] = [];

  ExampleDataSource: ExampleDataSource;
  addForm: FormGroup;
  DefaultFormValues: Object = {
    //createDate: this.createDate = formatDate(new Date(), 'MM/dd/yyyy, h:mm a', 'en'),
    //createDate: this.createDate = formatDate(new Date(), 'yyyy-MM-dd HH:mm:ss', 'en'),
    name: "",
    email: "XXXXreplaceXXXX @lafargeholcim.com",
    sapUserId: "",
    roles: [],
    plants: [],
    emailVerified: true,
    provider: 'google'
  };

  formControl = new FormControl('', [
    Validators.required,
    Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')
  ]);

  //@ViewChild('allPlants') private allPlants: MatOption;
  @ViewChild('select') select: MatSelect;

  constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User,
    public UserService: UserService,
    public RoleService: RoleService,
    public PlantService: PlantService) {
    this.createDate = formatDate(new Date(), 'MM/dd/yyyy, h:mm a', 'en');
    this.name = this.data.name;
    this.addForm = new FormGroup({
      //'createDate': new FormControl(this.createDate, [        Validators.required      ]),
      'name': new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]),
      'email': new FormControl('XXXXreplaceXXXX @holcim.com', [
        Validators.required,
        //Validators.minLength(19),
       // Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')
      ]),
      'sapUserId': new FormControl('', [
        Validators.minLength(3)
      ]),
      'roles': new FormControl([''], [
        Validators.required
      ]),
      'plants': new FormControl('', [
        Validators.required
      ]),
      'emailVerified': new FormControl({ value: 'true', disabled: true }, [Validators.required]),
      'provider': new FormControl({ value: 'google', disabled: true }, [Validators.required])
    });
    this.addForm.setValue(this.DefaultFormValues);
    this.data.provider = "google";
  }

  ngOnInit(): void {
    this.paramsubscriptions.push(this.PlantService.getAll().subscribe(data => this.plants = data.sort((a, b) => {
      return a.name == b.name ? 0 : a.name > b.name ? 1 : -1
    }))
    );

    this.paramsubscriptions.push(this.RoleService.getAllRole().subscribe(data => this.roles = data.sort((a, b) => {
      return a.name == b.name ? 0 : a.name > b.name ? 1 : -1
    }))
    );

    this.data.provider = "google";
    this.data.emailVerified = true;
  }

  ngAfterViewInit(): void {
    this.addForm.setValue(this.DefaultFormValues)
  }

  ngOnDestroy(): void {
    this.paramsubscriptions.forEach(subscription => subscription.unsubscribe());
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
    // emppty stuff
  }

  reset() {
    this.addForm.reset(this.DefaultFormValues);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  /*
  selectAll() {
    if (this.allPlants.selected) {
      this.addForm.controls.plants
        .patchValue([...this.plants.map(item => item)], 0);
    } else {
      this.addForm.controls.plants.patchValue([]);
    }
  }

  tosslePerOne(all){
    if (this.allSelected.selected) {
     this.allSelected.deselect();
     return false;
 }
   if(this.addForm.controls.userType.value.length==this.plants.length)
     this.allSelected.select();
 }
 */


//select all checkbox to select / unselect all values plant

 toggleAllSelection() {
  if (this.allSelected) {
    this.select.options.forEach((item: MatOption) => item.select());
  } else {
    this.select.options.forEach((item: MatOption) => item.deselect());
  }
}



  confirmAdd(): void {
    console.log("add user data: " + JSON.stringify(this.data));
    console.log(JSON.stringify(this.data));

    this.UserService.addUser(this.data);
  }
}
