import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../../../services/user.service';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { RoleService } from './../../../../services/role.service';
import { PlantService } from './../../../../services/plant.service';
import { Role } from './../../../../model/Role';
import { Plant } from './../../../../model/plant';
import { User, Roles, Plants } from './../../../../model/user';
import { MatOption } from '@angular/material/core';
import { MatSelect } from '@angular/material/select';

@Component({
  selector: 'app-edit.dialog',
  templateUrl: './edit.dialog.html',
  styleUrls: ['./edit.dialog.scss']
})

export class EditDialogComponent implements OnInit, OnDestroy {

  roleslist: string[] = ['User', 'Admin', 'Cif-Leader', 'site_prod_shift', 'site_geo_quarry', 'site_maint_plan', 'site_prv_maint', 'site_plant_mgr', 'site_indust_dir', 'site_quality_mgr', 'site_maint_mgr', 'site_disp_mgr', 'site_auto_mgr', 'site_quarry_mgr', 'site_prod_ccr', 'site_prod_coach', 'site_plant_contr', 'site_maint_elec', 'site_prod_mgr', 'site_add_pos_1', 'site_rep_resp', 'site_maint_mech', 'site_proc_opt', 'site_hr_mgr', 'site_add_pos_2', 'site_env_mgr', 'site_pft_resp', 'site_add_pos_3', 'site_add_pos_5', 'site_add_pos_4'];
  editForm: FormGroup;
  numbers: number[] = [];
  id: number;
  imageUrl = '';
  createDate: Date;
  user: User[];
  plants: Plant[];
  roles: Roles[] = [];
  allSelected = false;
  /*
plants: Plant[] = [];
  dataPlant :  Plant[] = [];
  plantArray = [];
  */
  //@ViewChild('allPlants') private allPlants: MatOption;

  @ViewChild('select') select: MatSelect;

  private paramsubscriptions: Subscription[] = [];
  selectedRoles: Roles[];
  selectedPlants: Plant[];
  selectedPlants2: any;
  userForm: FormControl;

  constructor(public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User,
    public userService: UserService,
    public RoleService: RoleService,
    public PlantService: PlantService) {

    this.editForm = new FormGroup({
      'id': new FormControl('data.id', []),
      'name': new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      'email': new FormControl('', [
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'),
      ]),
      'sapUserId': new FormControl('', []),
      'roles': new FormControl('data.role', []),
      'plants': new FormControl('data.plants', [])
      //'createDate': new FormControl('', []),
    });
    this.editForm.setValue({
      id: data.id,
      name: data.name,
      email: data.email,
      sapUserId: data.sapUserId,
      roles: data.roles,
      plants: data.plants
    });
    //this.userForm = new FormControl([this.selectedPlants]);
  }

  ngOnInit(): void {
    this.paramsubscriptions.push(this.PlantService.getAll().subscribe(plant => this.plants = plant.sort((a, b) => {
      return a.name == b.name ? 0 : a.name > b.name ? 1 : -1
    })));

    this.paramsubscriptions.push(this.RoleService.getAllRole().subscribe(role => this.roles = role.sort((a, b) => {
      return a.name == b.name ? 0 : a.name > b.name ? 1 : -1
    })));
    this.id = this.data.id;
    //this.selectedRoles.map(id => id => this.numbers); // [0,1,2,3,4]
    //this.numbers=this.selectedRoles.map(a => a.id);
    /*this.selectedRoles= this.data.roles.map(a=>a.id);
    this.selectedPlants= this.data.plants.map(a=>a.plantId);*/

    this.selectedRoles = this.data.roles;
    this.selectedPlants = this.data.plants;

    //console.log("data loaded: "+JSON.stringify(this.data));
    //console.log("selectedRoles loaded: "+JSON.stringify(this.selectedRoles));
    //console.log("selectedPlants loaded: "+JSON.stringify(this.selectedPlants));
    //console.log("selected role: "+ JSON.stringify(this.selectedRoles));
    //console.log(this.selectedPlants);
  }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' : '';
  }

  ngOnDestroy() {
    this.paramsubscriptions.forEach(subscription => subscription.unsubscribe());
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.userService.updateUser(this.data);
    console.log("data updated: " + JSON.stringify(this.data));
  }

  onChangeRoles(value: Roles) {
    this.data.roles = [];
    this.data.roles.push(value);
    //console.log( JSON.stringify(value) + " Clicked Role!");
  }

  compareRoles(o1: Roles, o2: Roles): boolean {
    if (o1.name == null || o2.name == null) {
      return false;
    }
    return o1.name === o2.name;
  }

  // return o1 && o2 ? o1.id === o2.id : o1 === o2;


  comparePlants(o1: Plants, o2: Plants): boolean {
    return o1 && o2 ? o1.plantId === o2.plantId : o1 === o2;
  }

  /*
    selectAll() {
       if (this.allPlants.selected) {
         this.editForm.controls.plants
           .patchValue([...this.plants.map(item => item)], 0);
       } else {
         this.editForm.controls.plants.patchValue([]);
       }
     }
  */

//select all checkbox to select / unselect all values plant

  toggleAllSelection() {
    if (this.allSelected) {
      this.select.options.forEach((item: MatOption) => item.select());
    } else {
      this.select.options.forEach((item: MatOption) => item.deselect());
    }
  }

  onChangePlants(value: Plants) {
    this.data.plants = [];
    //this.selectedPlants.filter(x => x.plantId === value.plantId);
    //var merged = [].concat.apply([], this.selectedPlants2);    console.log(merged);
    this.selectedPlants2 = value;
    const merge3 = this.selectedPlants2.flat(1); //The depth level specifying how deep a nested array structure should be flattened. Defaults to 1.
    //console.log(merge3);

    this.data.plants.push(merge3);
    this.data.plants = [].concat.apply([], this.data.plants)

    /*
    this.data.plants = [];
    for(var pid of pvalue){
      for(var plant of this.plants){
        if(pid == plant.plantId){
          this.data.plants.push(plant);
          break;
        }

       }
      }

    console.log("changed select plant value");
    */

    //this.data.plants=this.data.plants[0];
    //this.data.plants.push([].concat.apply([], value));

    //this.data.plants=this.data.plants[0];
    //this.data.plants.setValue(this.data.plants[0]);
    //console.log( JSON.stringify(value) + " Clicked Plant!");
    //console.log("1 "+ JSON.stringify(this.data.plants));
    //console.log("selectedPlants: "+ this.selectedPlants);
    //console.log("shift: "+ JSON.stringify(this.data.plants.shift()));
    //console.log("3 "+ JSON.stringify(this.data.plants.shift()));
    //console.log(JSON.stringify(this.selectedPlants.filter(x => x.plantId === value.plantId)));
  }


}
