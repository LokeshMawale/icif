import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { UserService } from '../../../../services/user.service';
@Component({
  selector: 'app-bulk-update',
  templateUrl: './bulk-update.component.html',
  styleUrls: ['./bulk-update.component.scss']
})
export class BulkUpdateComponent  {

  constructor(public dialogRef: MatDialogRef<BulkUpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public userService: UserService) { }

onNoClick(): void {
this.dialogRef.close();
}

confirmUpdate(): void {
  //this.plantService.showMadiUpdate;
  this.dialogRef.close();
  this.userService.bulkUpdateUserData();
}

  ngOnInit(): void {
  
  }

}
