import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatSnackBarRef } from '@angular/material/snack-bar';
import {CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-custom-snack-bar',
  templateUrl: './custom-snack-bar.component.html',
  styleUrls: ['./custom-snack-bar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CustomSnackBarComponent implements OnInit {

  constructor(private snackBarRef: MatSnackBarRef<CustomSnackBarComponent>,
              private cookieService: CookieService) { }

  ngOnInit(): void {
    this.cookieService.set('snackbarset', 'yes');
  }

  closeSnackBar(){
    this.snackBarRef.dismiss();
  }
}
