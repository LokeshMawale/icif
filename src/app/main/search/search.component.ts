import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { SearchDataService } from 'src/app/services/search-data-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import {CookieService } from 'ngx-cookie-service';
import { GoogleAnalyticsService } from 'src/app/services/google-analytics.service';
//import { JsonpClientBackend } from '@angular/common/http';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  toSearch = "";
  content: any =[];  
  pageOfItems:Array<any>;  
  items=[];
  dataSources=[];
  docTypes=[];
  selDataSource="";
  selDocType="";
  searchTerm = "";
  loading = false;
  searchResult:any =[];
  searchContent:any =[];

  constructor(private userService: UserService, private routeParams: ActivatedRoute, 
    private router: Router,private cookieService:CookieService,private searchDataService : SearchDataService,private googleAnalyticsService: GoogleAnalyticsService) {

    //this.router.routeReuseStrategy.shouldReuseRoute = () => false;

  }

  ngOnInit() {
    this.searchTerm = this.routeParams.snapshot.queryParamMap.get('searchValue');
    var plantName = localStorage.getItem("plantName");
    var plantCode = localStorage.getItem("code");
    var email = localStorage.getItem("email");
    this.googleAnalyticsService.eventEmitter("iCIF Search","iCIF Search","iCIF Search",this.searchTerm,this.searchTerm,plantName,plantCode,email);
    this.loading = true;
    this.searchDataService.currentMessage.subscribe(searchData => (this.searchResult = searchData));
    if(this.searchResult.length ==0){
    // this.userService.getMapStatus().subscribe(data => {
     // if( data.message== 'empty'){
        this.userService.getAllSearchResults().subscribe(data => {
          this.searchDataService.changeMessage(data);
          this.searchDataService.currentMessage.subscribe(searchData => (this.searchResult = searchData));
          this.newMethod();
          this.loading = false;
        },error=>{
          console.log(error);
          this.loading = false;
        }
        ); 
    //   }else{
    //     this.loading = true;
    //     this.newMethod();
    //     this.loading = false;
    //   }
      
    // },
    // error => {
    //   console.log('error search result in fatching the data');
      
    //   }
    // );    
    }else{
      this.loading = true;
      this.newMethod();
      this.loading = false;
    }
   
  }

  private newMethod() {
    
    this.searchContent = this.searchResult;
    //this.cookieService.set("searchContent",this.searchContent);
    let backArrow = this.routeParams.snapshot.queryParamMap.get('backArrow');
    if (backArrow) {
      this.selDataSource = localStorage.getItem("selectedDsrc");
      this.selDocType = localStorage.getItem("selectedDocType");
      this.onFilterSearch();
    }
    else if (this.searchTerm) {
      this.fetchDataService();
    }
  }

  fetchDataService() {
    this.loading = true;
    let searchTerm = this.routeParams.snapshot.queryParamMap.get('searchValue');
    let searchSplit :any;
    searchTerm = searchTerm.toLowerCase();
    if(searchTerm){
      searchSplit = searchTerm.split(" ");
    }
    //this.userService.getAllSearchResults().subscribe(data => {
     // localStorage.setItem("jsonD",JSON.stringify(data));
     // this.searchContent =  JSON.parse(JSON.stringify(data));
      for(var doc of this.searchContent.searchResult){
        // var checkID = 6121333729525760;
        // if(doc.id == checkID){
        //   console.log('--');
        // }
        let docStr = doc.name ;
        if(doc.keyTopics){
          for(var keyTopic of  doc.keyTopics){
            docStr = docStr+" "+keyTopic;
          }
        }
        if(doc.tags){
          for(var tag of  doc.tags){
            docStr = docStr+" "+tag;
          }
        }
        docStr = docStr+" "+doc.description;
        docStr = docStr+" "+doc.label;
        docStr = docStr.toLowerCase();

        if(doc.plant){
          docStr = docStr+" "+doc.plant
        }
        
        var strMatch = false;

        var regex = new RegExp("\\b"+str,"gi");

        if(searchSplit && searchSplit.length == 1){
          var regex = new RegExp("\\b"+searchSplit[0],"gi");
          if(docStr.match(regex)){
            strMatch = true;
          }else{
            strMatch = false;
          }
        }else{
          for(var str of searchSplit){
          
            if(docStr.indexOf(str) >-1){
              strMatch = true;
            }else{
              strMatch = false;
              break;
            }
          }
        }
        if(strMatch){
          this.content.push(doc);
        }
      }
      this.loading = false;
      this.dataSources = this.searchContent.dataSource;
      this.docTypes = this.searchContent.documentType;
      //this.content = data.searchResult;viewsCount
    //  this.content = this.content.sort((a, b) => b.label - a.label);
      //this.content = this.content.sort((a, b) => b.viewsCount - a.viewsCount);
     // likesCount
      this.sortContent();
      this.items = this.content;
   // },error =>{
    //  this.loading = false;
   // });
   //var scontent = this.cookieService.get("searchContent");
    
    //  this.userService.searchCIFDocuments(searchTerm).subscribe(data => {
       
    //     this.selDataSource = '';
    //     if(data.selectedDocumentType && data.selectedDocumentType !=null){
    //       this.selDocType = data.selectedDocumentType;
    //     } 
    //     this.dataSources = data.dataSource;
    //     this.docTypes = data.documentType;
    //     this.content = data.searchResult;
    //     this.items = this.content;
    //     this.loading = false;
    //   },

    //   error => {
    //     console.log('error in fatching the data');
    //     this.loading = false;
    //   });

    

  }
  sortContent() {
    this.content = this.content.sort( function( a , b){
      if(a.likesCount === b.likesCount && a.viewsCount == b.viewsCount){
        var labelB ='';
        var labelA = '';
        if(!b.label){
          labelB= b.name;
        }else{
          labelB = b.label
        }
        if(!a.label){
          labelA = a.name;
        }else{
          labelA = a.label;
        }

        if(labelA > labelB){
          return 1;
        }
        if(labelB > labelA){
          return -1;
        }
        return 0;
      }else{
        if(a.likesCount === b.likesCount) {
          return b.viewsCount - a.viewsCount;
        }else {
          return b.likesCount - a.likesCount;
        }
      }
      
    });
  
  }
  
  onFilterSearch(){
    this.loading = true;
    this.searchTerm = this.routeParams.snapshot.queryParamMap.get('searchValue');
    this.items=[];
    this.content = [];
    if(this.searchTerm.trim()!=''){
      // this.userService.searchCIFDocumentsFilter(this.searchTerm, this.selDataSource, this.selDocType).subscribe(data => {
      //   if(data.selectedDataSource && data.selectedDataSource !=null){
      //     this.selDataSource = data.selectedDataSource;
      //   }
      //   if(data.selectedDocumentType && data.selectedDocumentType !=null){
      //     this.selDocType = data.selectedDocumentType;
      //   } 
      //   this.dataSources = data.dataSource;
      //   this.docTypes = data.documentType;
      //   this.content = data.searchResult;
      //   this.items = this.content;
      //   this.loading = false;
      // },

      // error => {
      //   console.log('error in fatching the data');
      //   this.loading = false;
      // });
      let searchSplit :any;
      this.searchTerm = this.searchTerm.toLowerCase();
      if(this.searchTerm){
        searchSplit = this.searchTerm.split(" ");
      }
     // this.userService.getAllSearchResults().subscribe(data => {
        //this.searchContent =  JSON.parse(JSON.stringify(data));
        for(var doc of this.searchContent.searchResult){
          var checkID = 6121333729525760;
          if(doc.id == checkID){
            console.log('--');
          }
          var addDocument = true;
          if(this.selDataSource){
            if(doc.dataSource == this.selDataSource){
              addDocument = true;
            }else{
              addDocument=false;
            }
          }
          if(this.selDocType && addDocument){
            if(doc.docType == this.selDocType){
              addDocument = true;
            }else{
              addDocument=false;
            }
          }
          if(addDocument){
            let docStr = doc.name ;
            if(doc.keyTopics){
              for(var keyTopic of  doc.keyTopics){
                docStr = docStr+" "+keyTopic;
              }
            }
            if(doc.tags){
              for(var tag of  doc.tags){
                docStr = docStr+" "+tag;
              }
            }
            docStr = docStr+" "+doc.description;
            docStr = docStr+" "+doc.label;
            docStr = docStr.toLowerCase();
            this.searchTerm = this.searchTerm.toLowerCase();
            var strMatch = false;
            for(var str of searchSplit){
              if(docStr.indexOf(str) >-1){
                strMatch = true;
              }else{
                strMatch = false;
                break;
              }
            }
            if(strMatch){
              this.content.push(doc);
            }
          }
        }
        this.loading = false;
        this.dataSources = this.searchContent.dataSource;
        this.docTypes = this.searchContent.documentType;
       
        this.sortContent();
        this.items = this.content;
     // },error =>{
     //   this.loading = false;
     // });
    }
    else{
     //alert ("Please enter something to search");
     this.loading = false;
    }
  }

  redirectToURL(event : any){
    window.open(event.documentLink, "_blank");
  }


  onChangePage(pageOfItems: Array<any>) {

    this.pageOfItems = pageOfItems;

  }

  goToDetails(id : any){
    localStorage.setItem("selectedDsrc" , this.selDataSource);
    localStorage.setItem("selectedDocType",this.selDocType);
    localStorage.setItem("searchTerm",this.searchTerm);
    var plantCode = localStorage.getItem("code");
    this.router.navigate(['/main/document-details/'+id], { queryParams: { plantCode:plantCode } });
  }

  

  onDataSrcChange(event: any){
    this.selDataSource = event.target.value;
  }

  onDocTypeChange(event: any){
    this.selDocType = event.target.value;
  }  
}
