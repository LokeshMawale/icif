import { Component, OnInit, HostListener, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ConnectionService } from 'src/app/services/connection.service';
import { Contact } from 'src/app/model/contact';

interface Subject {
  value: string;
  name?: string;
  viewValue?: string;
  Subsubject?: string[];
}

interface SubjectGroup {
  disabled?: boolean;
  name?: string;
  Subject: Subject[];
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})

export class ContactComponent implements OnInit {

  SubjectControl = new FormControl();
  sender : String;
  DefaultBody: String;
  SubjectGroups: SubjectGroup[] = [
    {
      name: 'General',
      Subject: [
        { value: 'iCIF Landing page', viewValue: 'iCIF Landing page', Subsubject: ['iCIF Landing page'] },
        { value: 'CIF Workflow', viewValue: 'CIF Workflow', Subsubject: ['CIF Workflow'] },
        { value: 'iCIF Implementation methodology', viewValue: 'iCIF Implementation methodology', Subsubject: ['iCIF Implementation methodology'] },
        //{value: 'ICIF Tools', viewValue: 'ICIF Tools', Subsubject:['Gap Analyzer','Dashboard','MADI','Smartsheet','Other Tool']},
        { value: 'Other', viewValue: 'Other', Subsubject: [''] }
      ]
    },
    {
      name: 'ICIF Tools',
      //      disabled: true,
      Subject: [
        { value: 'Gap Analyzer', viewValue: 'Gap Analyzer' },
        { value: 'Dashboard', viewValue: 'Dashboard' },
        { value: 'MADI', viewValue: 'MADI' },
        { value: 'Smartsheet', viewValue: 'Smartsheet' },
        { value: 'Other Tool', viewValue: 'Other' },
      ]
    },
  ];

  contactForm: FormGroup;
  disabledSubmitButton: boolean = true;
  optionsSelect: SubjectGroup;
  profile_email: string;
  profile_name: string;
  contact: Contact;
  //photo: string;
  DefaultFormValues: Object = {
    //email: 'XXXXreplaceXXXX @holcim.com',
    sender: '',
    subject: '',
    message: '',
    };

  @HostListener('input') oninput() {

    if (this.contactForm.value.sender && this.contactForm.value.message && this.contactForm.value.subject !='') {
      this.disabledSubmitButton = false;
    }
  }

  constructor(fb: FormBuilder, private connectionService: ConnectionService) {
    this.contactForm = fb.group({
      'sender': new FormControl('', [
        //'sender': new FormControl('josemiguel.pinero@holcim.com', [
        Validators.required,
        Validators.minLength(19),
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')
      ]), //['', Validators.compose([Validators.required, Validators.email])],
      'subject': [''],
      'message': ['', Validators.required],
      // 'contactFormCopy': ['', Validators.requiredTrue],
    });
    this.contactForm.setValue(this.DefaultFormValues);
  }

  ngOnInit() {
    this.profile_email = localStorage.getItem('email');
    this.sender=this.profile_email;
    this.DefaultBody= 'From: '+this.sender +"\n"+ "Text: "+"\n";
    //this.photo = 'https://lh3.googleusercontent.com/a-/AOh14Gh4osLDWVyarZMfbnpwDHjucGmhqVCEJkUE4qFveA=s96-c';
  }


  ngAfterViewInit(): void {
    //this.contactForm.setValue(this.DefaultFormValues);
//    this.profile_email = JSON.parse(localStorage.getItem('email'));
  }

  get email() {
    return this.contactForm.get('sender');
  }
  get subjects() {
    return this.contactForm.get('subject');
  }
  get message() {
    return this.contactForm.get('message');
  }
  //get copy() {
  //  return this.contactForm.get('contactFormCopy');
  //}

  validate(){
    if (this.contactForm.value.sender && this.contactForm.value.message && this.contactForm.value.subject !='') {
      this.disabledSubmitButton = false;
    }
  }

  onSubmit() {
    this.connectionService.sendMessage(this.contactForm.value).subscribe(() => {
      //this.contactForm.reset();
      this.sender=this.profile_email;
      this.clearForm();
      this.disabledSubmitButton = true;
      //this.connectionService.showSuccess();
    }, (error: any) => {
      console.log('Error', error);
    });
  }

  clearForm() {
    this.contactForm.reset({
          'sender': this.profile_email,
          'subject': '',
          'message': '',
    });

    }

}
