import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { UsersComponent } from './users/users.component';
import { RolesComponent } from './roles/roles.component';
import { PlantComponent } from './plant/plant.component';
import { TaskComponent } from './task/task.component';
import { WorkflowComponent } from './workflow/workflow.component';
import { ProjectWavesComponent } from './ProjectWaves/ProjectWaves.component';
import { MainDetailComponent } from './main-detail/main-detail.component';
import { ContactComponent } from './contact/contact.component';
import { MaterialModule } from '../material.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { AddDialogComponent } from './users/dialogs/add/add.dialog.component';
import { EditDialogComponent } from './users/dialogs/edit/edit.dialog.component';
import { DeleteDialogComponent } from './users/dialogs/delete/delete.dialog.component';
import { AddDialogPlantComponent } from './plant/dialogs/add/addPlant.dialog.component';
import { EditDialogPlantComponent } from './plant/dialogs/edit/editPlant.dialog.component';
import { DeleteDialogPlantComponent } from './plant/dialogs/delete/deletePlant.dialog.component';
import { AddRoleDialogComponent } from './roles/dialogs/add/addRole.dialog.component';
import { EditRoleDialogComponent } from './roles/dialogs/edit/editRole.dialog.component';
import { DeleteRoleDialogComponent } from './roles/dialogs/delete/deleteRole.dialog.component';
import { UserService } from '../services/user.service';
import { RegionService } from '../services/region.service';
import { AreaService } from '../services/area.service';
import { OpcoService } from '../services/Opco.service';
import { PlantService } from '../services/plant.service';
import { RoleService } from '../services/role.service';
import { WorkflowService } from '../services/workflow.service';
import { SubtaskService } from '../services/subTask.service';
import { ConnectionService} from '../services/connection.service';
import { FormsModule } from '@angular/forms';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AddWorkflowDialogComponent } from './workflow/dialogs/add/addWorkflow.dialog.component';
import { EditWorkflowDialogComponent } from './workflow/dialogs/edit/editWorkflow.dialog.component';
import { DeleteWorkflowDialogComponent } from './workflow/dialogs/delete/deleteWorkflow.dialog.component';
import { TaskDialogComponent } from './task/task-dialog/task-dialog.component';
import { TypeTaskService } from '../services/typetask.service';
import { ConfirmProcesDialogComponent } from './main-detail/confirm-proces-dialog/confirm-proces-dialog.component';
import { TaskAssignmentComponent } from './task-assignment/task-assignment.component';
import { SearchComponent } from './search/search.component';
import { JwPaginationModule  } from 'jw-angular-pagination';
import { SysComponent } from './sys/sys.component';
import { ExcelService } from './../services/excel.service';
import { ProcessflowComponent } from './processflow/processflow.component';
import { PinchZoomModule } from 'ngx-pinch-zoom';
import { DocumentDetailsComponent } from './document-details/document-details.component';
import { CommentComponent } from './document-details/comment/comment.component';
import { ChartsModule } from 'ng2-charts';
import { UploadService } from '../services/upload.service';
import { DataFolderComponent } from './data-folder/data-folder.component';
import { NewTaskComponent } from './new-task/new-task.component';
import { CustomSnackBarComponent } from './custom-snack-bar/custom-snack-bar.component';
import { GoogleDriveComponent } from './google-drive/google-drive.component';
import { GoogleAnalyticsService} from '../services/google-analytics.service';
import { SearchDataService} from '../services/search-data-service.service';
import { BulkuploadDialogComponent } from './plant/dialogs/bulkupload-dialog/bulkupload-dialog.component';
import { BulkUpdateComponent } from './users/dialogs/bulk-update/bulk-update.component';
import { StarRatingModule } from 'angular-star-rating';
import { WebUrlPreviewService } from '../services/WebUrlPreviewService';
import { LeaderBoardComponent } from './leader-board/leader-board.component';
import { ProgressBarColor } from '../services/progress-bar-color';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    MainRoutingModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatSortModule,
    MatTableModule,
    MatIconModule,
    FlexLayoutModule,
    PinchZoomModule,
    JwPaginationModule,
    ChartsModule,
    StarRatingModule
  ],
  declarations: [
    UsersComponent,
    RolesComponent,
    PlantComponent,
    WorkflowComponent,
    ProjectWavesComponent,
    MainDetailComponent,
    ContactComponent,
    AddDialogComponent,
    EditDialogComponent,
    DeleteDialogComponent,
    AddDialogPlantComponent,
    EditDialogPlantComponent,
    DeleteDialogPlantComponent,
    AddWorkflowDialogComponent,
    EditWorkflowDialogComponent,
    DeleteWorkflowDialogComponent,
    AddRoleDialogComponent,
    EditRoleDialogComponent,
    DeleteRoleDialogComponent,
    TaskComponent,
    TaskDialogComponent,
    ConfirmProcesDialogComponent,
    TaskAssignmentComponent,
    SearchComponent,
    SysComponent,
    ProcessflowComponent,
    DocumentDetailsComponent,
    CommentComponent,
    DataFolderComponent,
    NewTaskComponent,
    CustomSnackBarComponent,
    GoogleDriveComponent,
    BulkuploadDialogComponent,
    BulkUpdateComponent,
    LeaderBoardComponent,
    ProgressBarColor
  ],
  entryComponents: [
    AddDialogComponent,
    EditDialogComponent,
    DeleteDialogComponent,
    AddDialogPlantComponent,
    EditDialogPlantComponent,
    DeleteDialogPlantComponent,
    AddRoleDialogComponent,
    EditRoleDialogComponent,
    DeleteRoleDialogComponent,
    AddWorkflowDialogComponent,
    EditWorkflowDialogComponent,
    DeleteWorkflowDialogComponent
  ],
  exports: [MainDetailComponent, FlexLayoutModule],
  providers: [UserService, RoleService, RegionService, AreaService, OpcoService, PlantService, WorkflowService, TypeTaskService, SubtaskService, ConnectionService, ExcelService, UploadService, GoogleAnalyticsService, SearchDataService, WebUrlPreviewService]
})

export class MainModule { }
