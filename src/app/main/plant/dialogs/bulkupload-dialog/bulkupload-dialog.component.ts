import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { PlantService } from '../../../../services/plant.service';

@Component({
  selector: 'app-bulkupload-dialog',
  templateUrl: './bulkupload-dialog.component.html',
  styleUrls: ['./bulkupload-dialog.component.scss']
})
export class BulkuploadDialogComponent  {



  constructor(public dialogRef: MatDialogRef<BulkuploadDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public plantService: PlantService) { }

onNoClick(): void {
this.dialogRef.close();
}

confirmUpdate(): void {
  this.dialogRef.close();
  this.plantService.updatePlantLinks();
}

}
