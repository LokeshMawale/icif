import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkuploadDialogComponent } from './bulkupload-dialog.component';

describe('BulkuploadDialogComponent', () => {
  let component: BulkuploadDialogComponent;
  let fixture: ComponentFixture<BulkuploadDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulkuploadDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkuploadDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
