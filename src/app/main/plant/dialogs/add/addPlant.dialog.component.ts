import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, Input, OnInit, ɵConsole, AfterViewInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Plant } from './../../../../model/plant';
import { PlantsDataSource } from '../../plant.component';
import { PlantService } from './../../../../services/plant.service';
import { OpcoService } from './../../../../services/Opco.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-addPlant.dialog',
  templateUrl: './addPlant.dialog.html',
  styleUrls: ['./addPlant.dialog.scss'],
})

export class AddDialogPlantComponent implements OnInit, AfterViewInit, OnDestroy {
  name: String = "name";
  idvalue: number;
  type: any[] = [{ id: "1", name: "Cement Plant" }];
  typelist: any[] = [{ id: "1", name: "Cement Plant" }, { id: "2", name: "Grinding Station" }];
  opcoIdList: any[] = [{ opcoId: '1', name: 'Argentina' }, { opcoId: '2', name: 'Brazil' }, { opcoId: '3', name: 'Costa Rica' }];
  PlantsDataSource: PlantsDataSource;
  addForm: FormGroup;
  opcoId: number;
  opcoName: any[] = [];
  private paramsubscriptions: Subscription[] = [];
  DefaultFormValues: Object = {
    plantId: '',
    name: '',
    code: 'XXX',
    smartsheetLink: 'https://',
    madiLink: 'https://',
    gapAnalyzerLink: 'https://',
    type: this.typelist[0],
    opcoId: '',
    opcoName: '',
  };
  PlantComponent: any;
  public myreg = /(^|\s)((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/gi

  constructor(public dialogRef: MatDialogRef<AddDialogPlantComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Plant,
    public plantService: PlantService,
    private opcoService: OpcoService,
    public fb: FormBuilder) {
    this.name = this.data.name;
    this.type = this.data.type;
    this.addForm = new FormGroup({
      'plantId': new FormControl(''),
      'name': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'code': new FormControl('', [Validators.required, Validators.maxLength(3), Validators.minLength(2)]),
      'smartsheetLink': new FormControl('', [Validators.pattern(this.myreg)]),
      'madiLink': new FormControl('', [Validators.pattern(this.myreg)]),
      'gapAnalyzerLink': new FormControl('', [Validators.pattern(this.myreg)]),
      'type': new FormControl('1'),
      'opcoId': new FormControl(''),
      'opcoName': new FormControl('')
    });
    this.addForm.setValue(this.DefaultFormValues);
  }

  formControl = new FormControl('', [Validators.required]);

  ngOnInit(): void {
    this.paramsubscriptions.push(this.opcoService.getAllOpco().subscribe(opcoData => this.opcoIdList = opcoData.sort((a, b) => {
      return a.name == b.name ? 0 : a.name > b.name ? 1 : -1
    }))
    );
  }

    ngAfterViewInit(): void {
      this.addForm.reset(this.DefaultFormValues);
    }

    // get f(){    return this.addForm.controls;  }

    getErrorMessage() {
      return this.formControl.hasError('required') ? 'Required field' :
        this.formControl.hasError('name') ? 'Not a valid name' : '' ||
          this.formControl.hasError('smartsheetLink') ? 'Not a valid smartsheetLink' : '' ||
            this.formControl.hasError('madiLink') ? 'Not a valid madiLink'  : '' ||
            this.formControl.hasError('gapAnalyzerLink') ? 'Not a valid gapAnalyzerLink'  : '';
    }

    submit() {
      // emppty stuff
    }

    reset(){
      this.addForm.reset(this.DefaultFormValues);
    }

    onNoClick(): void {
      this.dialogRef.close();
    }

    confirmAdd(): void {
      this.plantService.addPlant(this.data);
      console.log("Upload data: "+JSON.stringify(this.data));
    }

  ngOnDestroy(): void {
    this.paramsubscriptions.forEach(subscription => subscription.unsubscribe());
  }
  
  OpcoChanged(event){
      //console.log("this event opco change: " + JSON.stringify(event));
      this.data.opcoId = event;
      this.opcoName = this.opcoIdList.filter(opco => opco.opcoId === this.data.opcoId);

      //this.opcoName=this.opcoIdList.filter(x => x.opcoId =this.data.opcoId )
      //console.log(this.data.opcoId);
      //console.log(this.opcoName[0].name);
      this.data.opcoName = this.opcoName[0].name.toString();
      //    this.data.opcoName=event.opcoName;
      //console.log(this.data.opcoId);
      //console.log(this.data.opcoName);
    }
  }
