import { Opco } from './../../../../model/opco';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { PlantService } from './../../../../services/plant.service';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { OpcoService } from 'src/app/services/Opco.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-editPlant.dialog',
  templateUrl: './editPlant.dialog.html',
  styleUrls: ['./editPlant.dialog.scss']
})

export class EditDialogPlantComponent implements OnInit, OnDestroy {
  formControl: FormGroup;
  opcoIdList: Opco[];
  opcoId: number;
  opcoName: any[]= [];
  private paramsubscriptions: Subscription[] = [];
  typelist: any[] = [{id: "1", name: "Cement Plant"}, {id:"2", name:"Grinding Station"}];
  public myreg = /(^|\s)((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/gi

  constructor(public dialogRef: MatDialogRef<EditDialogPlantComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public plantService: PlantService,
              private opcoService: OpcoService,
              public fb: FormBuilder) { 
               this.formControl = new FormGroup({
                 'smartsheetLink': new FormControl('',  [Validators.required, Validators.pattern(this.myreg)]),
                 'madiLink': new FormControl('', [Validators.required, Validators.pattern(this.myreg)]),
                 'gapAnalyzerLink': new FormControl('', [Validators.required, Validators.pattern(this.myreg)])
                })
              }
  
  ngOnInit(): void {
    this.paramsubscriptions.push(this.opcoService.getAllOpco().subscribe(opcoData => this.opcoIdList = opcoData.sort((a, b) => {
      return a.name == b.name ? 0 : a.name > b.name ? 1 : -1    }))    );
  }
  
  getErrorMessage() {
    if (this.formControl.hasError('smartsheetLink')) {
      return 'You must enter a valid URL';
    }
    
    console.log("Data to edit: "+JSON.stringify(this.data));

  }
    /*
    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
      */  
    /*
    return this.formControl.hasError('required') ? 'Required field' }
    if (this.email.hasError('required')) {
    return this.formControl.hasError('name') ? 'Not a valid name' };

      this.formControl.hasError('smartsheetLink') ? 'Not a valid URL' :
      this.formControl.hasError('madiLink') ? 'Not a valid URL' :
        '';
  }
  */
  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.plantService.updatePlant(this.data);
    console.log("Upload edited data: "+JSON.stringify(this.data));
  }

  OpcoChanged(event){
    console.log("this event opco change: "+JSON.stringify(event));
    this.data.opcoId=event;
    this.opcoName = this.opcoIdList.filter(opco => opco.opcoId === this.data.opcoId);
    
    //this.opcoName=this.opcoIdList.filter(x => x.opcoId =this.data.opcoId )
    //console.log (this.data.opcoId);
    //console.log (this.opcoName[0].name);
    this.data.opcoName = this.opcoName[0].name.toString();
//    this.data.opcoName=event.opcoName;
    //console.log (this.data.opcoId);
    //console.log (this.data.opcoName);
  }
  
  ngOnDestroy(): void {
    this.paramsubscriptions.forEach(subscription => subscription.unsubscribe());
  }
  

}
