
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { PlantService } from '../../../../services/plant.service';

@Component({
  selector: 'app-deletePlant.dialog',
  templateUrl: './deletePlant.dialog.html',
  styleUrls: ['./deletePlant.dialog.scss']
})
export class DeleteDialogPlantComponent {

  constructor(public dialogRef: MatDialogRef<DeleteDialogPlantComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public plantService: PlantService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.plantService.deletePlant(this.data.plantId);
  }
}
