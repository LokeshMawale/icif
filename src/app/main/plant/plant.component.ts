import { Component, OnInit, ViewChild, ElementRef, OnDestroy, HostListener, AfterViewInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Plant } from './../../model/plant';
import { PlantService } from './../../services/plant.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/table';
import { AddDialogPlantComponent } from './dialogs/add/addPlant.dialog.component';
import { EditDialogPlantComponent } from './dialogs/edit/editPlant.dialog.component';
import { BulkuploadDialogComponent } from './dialogs/bulkupload-dialog/bulkupload-dialog.component';

import { DeleteDialogPlantComponent } from './dialogs/delete/deletePlant.dialog.component';
import { BehaviorSubject, fromEvent, Observable, merge, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { RegionService } from 'src/app/services/region.service';
import { Region } from './../../model/region';
import { Area } from './../../model/area';
import { Cluster } from './../../model/cluster';
import { Opco } from './../../model/opco';
import { AreaService } from 'src/app/services/area.service';
import { OpcoService } from 'src/app/services/Opco.service';
import { ClusterService } from 'src/app/services/Cluster.service';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
  selector: 'app-plant',
  templateUrl: './plant.component.html',
  styleUrls: ['./plant.component.scss']
})

export class PlantComponent implements OnInit, OnDestroy {

  Regions: any[];
  Areas: any[];
  PlantForm: FormControl;
  displayedColumns = ['plantId', 'name', 'code', 'smartsheetLink', 'madiLink', 'gapAnalyzerLink', 'type', 'opcoName', 'actions'];
  displayedInitialColumns = ['plantId', 'name', 'code', 'smartsheetLink', 'madiLink', 'gapAnalyzerLink', 'type', 'opcoName', 'actions'];
  displayedColumnForMobilesPhones: string[] = ['plantId', 'name', 'code', 'actions'];
  isColumnsMobile: boolean;
  public innerWidth: any;
  PlantsDataBase: PlantService | null;
  dataSource: PlantsDataSource | null;
  index: number;
  plantId: number;
  Plant: any[] = [{ plantid: "1", name: "Cement Plant", code: "CP", smartsheetLink: "test link", madiLink: "test Link", gapAnalyzerLink: "test Link", type: { "id": 1, "name": "Cement Plant" }, opcoId: "Argentina" }];
  opcoId: any[];
  selectedValue: number;

  region: Region;
  area: Area;
  cluster: Cluster;
  opco: Opco;
  //name: string;
  //code: string;
  
  createPlantsForm: FormGroup;
  createRegionForm: FormGroup;
  createAreaForm: FormGroup;
  createClusterForm: FormGroup;
  createOpcoForm: FormGroup;
  createPlantForm: FormGroup;

  regions: Array<any> = [{ name: 'Loading', areas: ['Loading'], clusters: ['Loading'], opcos: ['Loading'], plants: ['Loading'] },];
  areas: Array<any> = [{ name: 'Loading', clusters: ['Loading'], opcos: ['Loading'], plants: ['Loading'] },];
  clusters: Array<any> = [{ name: 'Loading', opcos: ['Loading'], plants: ['Loading'] },];
  opcos: Array<any> = [{ name: 'Loading', plants: ['Loading'] },];
  plants: {};
  formDetail: any;

  selected: string;
  private paramsubscriptions: Subscription []=[];
  FormControl = new FormControl('', [Validators.required]);
  

  constructor(
    public plantService: PlantService,
    public httpClient: HttpClient,
    private toastr: ToastrService,
    public dialog: MatDialog,
    private regionService: RegionService,
    private areaService: AreaService,
    private clusterService: ClusterService,
    private opcoService: OpcoService,
    
    
    public fb: FormBuilder,
    private http: HttpClient
    //private toasterServices: ToasterServices,
  ) {  this.regions.push({ name: 'test1', areas: ['test1'], clusters: ['test1'], opcos: ['test1'], plants: ['test1'] },);
  this.regions.push({ name: 'test2', areas: ['test2'], clusters: ['test2'], opcos: ['test2'], plants: ['test2'] },);
  this.regions.push({ name: 'test3', areas: ['test3'], clusters: ['test3'], opcos: ['test3'], plants: ['test3'] },);
  this.selected = 'Loading'}

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;
  
  @ViewChild('triggerRegion') triggerRegion: MatMenuTrigger;
  @ViewChild('triggerArea') triggerArea: MatMenuTrigger;
  @ViewChild('triggerCluster') triggerCluster: MatMenuTrigger;
  @ViewChild('triggerOpco') triggerOpco: MatMenuTrigger;
  
  ngOnInit(): void {
    this.loadData();
    this.paramsubscriptions.push(this.regionService.getAllRegion().subscribe(data => this.regions = data));
    this.paramsubscriptions.push(this.areaService.getAllArea().subscribe(data => this.areas = data));
    this.paramsubscriptions.push(this.clusterService.getAllCluster().subscribe(data => this.clusters = data));
    this.paramsubscriptions.push(this.opcoService.getAllOpco().subscribe(data=> this.opcos = data));

    this.createRegionForm = this.fb.group({ 
      'name': new FormControl('',  [Validators.required, Validators.minLength(3)]),
      'code': new FormControl('',  [Validators.required, Validators.minLength(3)])});
    this.createAreaForm = this.fb.group({ name: [''], code: [''], regionId: [''] });
    this.createClusterForm = this.fb.group({ name: [''], code: [''], areaId: [''] });
    this.createOpcoForm = this.fb.group({ name: [''], code: [''], clusterId: [''] });
    this.createPlantForm = this.fb.group({ name: [''], code: [''], opcoId:[''] });

    this.createPlantsForm = new FormGroup({
      region: new FormControl(''),
      area: new FormControl(''),
      cluster: new FormControl(''),
      opco: new FormControl(''),
      plant: new FormControl(''),
    });
    
    this.innerWidth = window.innerWidth;
    if(this.innerWidth < 850 && !this.isColumnsMobile) {
      //console.log(this.innerWidth);
      this.displayedColumns = this.displayedColumnForMobilesPhones;
      this.isColumnsMobile = true;
  
    } else if(this.innerWidth >= 850 && this.isColumnsMobile) {
  
      this.displayedColumns = this.displayedInitialColumns;
      this.isColumnsMobile = false;
    }
  }

  ngAfterViewInit(): void {
    this.innerWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.innerWidth = window.innerWidth;
    if(event.target.innerWidth < 850 && !this.isColumnsMobile) {
  
      this.displayedColumns = this.displayedColumnForMobilesPhones;
      this.isColumnsMobile = true;
  
    } else if(event.target.innerWidth >= 850 && this.isColumnsMobile) {
  
      this.displayedColumns = this.displayedInitialColumns;
      this.isColumnsMobile = false;
    }
  }

  getErrorMessage() {
    return this.FormControl.hasError('required') ? 'Required field' :
      this.FormControl.hasError('name') ? 'Not a valid name' : '';
      this.FormControl.hasError('code') ? 'Not a valid name' : '';
      this.FormControl.hasError('regionId') ? 'Not a valid name' : '';
  }

  ngOnDestroy(): void {
    this.paramsubscriptions.forEach(subscription=>subscription.unsubscribe());
  }

  refresh() {
    this.loadData();
  }

  addNew(plant?: Plant) {
    const dialogRef = this.dialog.open(AddDialogPlantComponent, {
      data: { plant: plant }
    });


    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside plantService
        this.PlantsDataBase.dataChange.value.push(this.plantService.getDialogData());
        this.refreshTable();
        this.loadData();
      }
    });
  }

  startEdit(i: number, plantId: number, name: string, code: string, smartsheetLink: string, madiLink: string, gapAnalyzerLink:string, type: any[], opcoId: number, opcoName: string) {
    this.plantId = plantId;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    const dialogRef = this.dialog.open(EditDialogPlantComponent, {
      data: { plantId: plantId, name: name, code: code, smartsheetLink: smartsheetLink, madiLink: madiLink, gapAnalyzerLink: gapAnalyzerLink, type: type, opcoId: opcoId, opcoName: opcoName }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside plantService by id
        const foundIndex = this.PlantsDataBase.dataChange.value.findIndex(x => x.plantId === this.plantId);
        // Then you update that record using data from dialogData (values you enetered)
        this.PlantsDataBase.dataChange.value[foundIndex] = this.plantService.getDialogData();
        // And lastly refresh table
        this.refreshTable();
        this.loadData();
      }
    });
  }

  deleteItem(i: number, plantId: number, name: string, code: string, smartsheetLink: string, madiLink: string, gapAnalyzerLink:string, type: any[], opcoId: string, opcoName: string) {
    this.index = i;
    this.plantId = plantId;
    const dialogRef = this.dialog.open(DeleteDialogPlantComponent, {
      data: { plantId: plantId, name: name, code: code, smartsheetLink: smartsheetLink, madiLink: madiLink, gapAnalyzerLink: gapAnalyzerLink, type: type, opcoId: opcoId, opcoName: opcoName }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.PlantsDataBase.dataChange.value.findIndex(x => x.plantId === this.plantId);
        // for delete we use splice in order to remove single object from DataService
        this.PlantsDataBase.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
        this.loadData();
      }
    });
  }

  private refreshTable() {
    // Refreshing table using paginator
    // Thanks yeager-j for tips
    // https://github.com/marinantonio/angular-mat-table-crud/issues/12
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  /*   // If you don't need a filter or a pagination this can be simplified, you just use code from else block
    // OLD METHOD:
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }*/

  public loadData() {
    this.PlantsDataBase = new PlantService(this.httpClient, this.toastr);
    this.dataSource = new PlantsDataSource(this.PlantsDataBase, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
      // .debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
}

  onChangeRegion(regionId: number) {
    if (regionId) {
      this.areas = this.regions.find(con => con.regionId == regionId).areas;
      this.clusters = null;
      this.opcos = null;
      this.plants = null;
    }
    else {
      this.areas = null;
      this.clusters = null;
      this.opcos = null;
      this.plants = null;
    }
  }

  onChangeArea(areaId: number) {
    if (areaId) {
      //this.regionService.getAllRegion().subscribe(areas => this.areas = areas );
      //this.areaService.getAllArea().subscribe(area => this.areas = area );
      this.clusters = this.areas.find(con => con.areaId == areaId).clusters;
      this.opcos = null;
      this.plants = null;
    }
    else {
      this.clusters = null;
      this.opcos = null;
      this.plants = null;
    }
  }

  onChangeCluster(clusterId: number) {
    if (clusterId) {
      this.opcos = this.clusters.find(con => con.clusterId == clusterId).opcos;
      this.plants = null;
    }
    else {
      this.opcos = null;
      this.plants = null;
    }
  }

  onChangeOpco(opcoId: number) {
    if (opcoId) {
      this.plants = this.opcos.find(con => con.opcoId == opcoId).plants;
    }
    else {
      this.plants = null;
    }
  }

  onChangePlant(plantsId: number) {
    if (plantsId) {
      this.plants,
        console.log(this.plants);
    }
  }

  confirmAddRegion() {
    this.region = {
      name: this.createRegionForm.value['name'],
      code: this.createRegionForm.value['code']
    }
    this.regionService.createRegion(this.region);
    this.triggerRegion.closeMenu();
  
  }

  confirmAddArea() {
    this.area = {
      name: this.createAreaForm.value['name'],
      code: this.createAreaForm.value['code'],
      regionId: this.createAreaForm.value['regionId']
    }
    this.areaService.createArea(this.area);
    this.triggerArea.closeMenu();
    console.log (this.area);    
  }

  confirmAddCluster() {
    this.cluster = {
      name: this.createClusterForm.value['name'],
      code: this.createClusterForm.value['code'],
      areaId: this.createClusterForm.value['areaId'],
    }
    this.clusterService.createCluster(this.cluster);
    this.triggerCluster.closeMenu();
    console.log (this.cluster);
  }

  confirmAddOpco() {
    this.opco = {
      name: this.createOpcoForm.value['name'],
      code: this.createOpcoForm.value['code'],
      clusterId: this.createClusterForm.value['clusterId'],
    }
    this.opcoService.createOpco(this.opco);
    this.triggerOpco.closeMenu();
    console.log (this.opco);
  }

  updateMadiLinks(){
    this.dialog.open(BulkuploadDialogComponent);
  }

}
    //this.regionService.createRegion(this.region).subscribe(region=>{console.log (region)});
    /*
  const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  }

    console.log("salvar region");


     // Muestra los valores en consola
  console.log(this.createRegionForm.value);

  // Asigna valores a la interfaz
  this.region = {
    name: this.createRegionForm.value['name'],
    code: this.createRegionForm.value['code'],
  }

 // Usa el servicio
  
  //  var regionData: any = new FormData();
  //    Region.append("name", this.createRegionForm.get('name').value);
  //Region.append("code", this.createRegionForm.get('code').value);
    this.http.post('http://localhost:8080/createregion', this.region, httpOptions).subscribe(
      (response) => {
        return console.log(response);
      },
      (error) => console.log(error)
    )
    
  }
}*/

export class PlantsDataSource extends DataSource<Plant> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Plant[] = [];
  renderedData: Plant[] = [];

  constructor(public _PlantsDatabase: PlantService,
    public _paginator: MatPaginator,
    public _sort: MatSort) {
    super();
    // Reset to the first page when the Plant changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Plant[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._PlantsDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._PlantsDatabase.getAllPlants();

    return merge(...displayDataChanges).pipe(map(() => {
      // Filter data
      this.filteredData = this._PlantsDatabase.data.slice().filter((Plant: Plant) => {
        const searchStr = (Plant.plantId + Plant.name + Plant.code + Plant.smartsheetLink + Plant.madiLink + Plant.gapAnalyzerLink + Plant.type + Plant.opcoId + Plant.opcoName).toLowerCase();
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    }
    ));
  }

  disconnect() { }

  /** Returns a sorted copy of the database data. */
  sortData(data: Plant[]): Plant[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'plantId': [propertyA, propertyB] = [a.plantId, b.plantId]; break;
        case 'name': [propertyA, propertyB] = [a.name, b.name]; break;
        case 'code': [propertyA, propertyB] = [a.code, b.code]; break;
        case 'smartsheetLink': [propertyA, propertyB] = [a.smartsheetLink, b.smartsheetLink]; break;
        case 'madiLink': [propertyA, propertyB] = [a.madiLink, b.madiLink]; break;
        case 'gapAnalyzerLink': [propertyA, propertyB] = [a.gapAnalyzerLink, b.gapAnalyzerLink]; break;        
        case 'opcoId': [propertyA, propertyB] = [a.opcoId, b.opcoId]; break;
        case 'opcoName': [propertyA, propertyB] = [a.opcoName, b.opcoName]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }

  

}
