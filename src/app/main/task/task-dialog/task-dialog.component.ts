import { taskElements } from './../../main.component';
import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { formatDate } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PlantIdservice } from '../../../services/PlantIdservice';
import { TasksService } from '../../../services/tasks.service';
import { ExcelService } from '../../../services/excel.service';
import { Task } from '../../../model/Task';
import { Comment } from '../../../model/Comment';
import { GoogleAnalyticsService } from '../../../services/google-analytics.service';


@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.scss']
})
export class TaskDialogComponent implements OnInit {

  public task: Task;
  public comment: Comment;
  @ViewChild('textareas') textareas: ElementRef;

  task2: any[];
  //arr: any[];
  imageSource: String = "../../../../assets/images/EXCEL-ICON.png";
  PlantCode2: String = "CS";

  log: String;
  taskName: String = "taskName";

  date: String = "";

  members: String = "";

  comments: String = "";
  commentsList: any[]= [{user: "",comment: "", commentDate: ""}];
  textarea: String="";
  somePlaceholder: String = "Add Comment here"

  content: String = "";

  addForm: FormGroup;
  CommentForm: FormGroup;

  links: Array<String> = [];

  showSkip: Boolean = false;

  disableButton: Boolean = false;

  hideAssign: Boolean = false;

  showErrorMessage: Boolean = false;

  loading: Boolean = false;

  nextTaskInformation: Boolean = false;

  isLastTask: Boolean = false;

  nextTaskDetail: any;

  currentProcess: String;

  user: any;

  userName: String = "";

  public userData = [];

  assignWindow: Boolean = false;

  errors: any;

  assignmentSuccess: Boolean = false;

  isProcessPaused: Boolean = false;

  assignedTo: String = "";

  showCompletedBy: Boolean = false;

  pendingSequence: String = "";

  reopenFlag : Boolean = false;

  postponeFlag : Boolean = false;

  taskType : String = "";

  reOpenSuccess : Boolean = false;

  postponeSuccess : Boolean = false;

  isFloatingTask : boolean = false;

  isFacilitatorOrAdmin : String = 'no';

  isCorporateUser : String = 'no';

  completedBy : String = '';

  reload : Boolean = false;
  
  disablebywave: Boolean = false;

  disableSave : Boolean = false;

  DefaultFormValues: Object = {
    taskName: this.taskName = "",
    date: '',
    members: '',
    comments: '',
    links: [],
    user: Object,
  };

  constructor(public dialogRef: MatDialogRef<TaskDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private taskService: TasksService,
    private dialog: MatDialog,
    private PlantCodeserv: PlantIdservice,
    private excelService: ExcelService,
    private router: Router,
    private snackBar: MatSnackBar,
    private googleAnalyticsService: GoogleAnalyticsService) {

    dialogRef.disableClose = true;
   // this.arr = this.data.taskDetail.result || [];
    this.PlantCode2 = localStorage.getItem('code');
    // this.userName = localStorage.getItem('userName');
    this.loading = true;

    if (data.error && data.error.error) {
      if (data.error.error.status == 405) {
        if (data.error.error.message.indexOf('Please Complete previous task') >= 0) {
          this.pendingSequence = data.error.error.message.split("=")[1];
          data.message = "PreviousTaskIncomplete";
        } else {
          data.message = data.error.error.message;
        }


      } else {
        data.message = "Internal Error Occured . Please Try After Sometime !!"
      }
      this.loading = false;
      this.showErrorMessage = true;
    } else {
      let plantID = parseInt(localStorage.getItem("plantID"));
      this.isProcessPaused = data.taskDetail.isProcessPaused;
      this.isFacilitatorOrAdmin =localStorage.getItem('facilitatorAdmin');
      if(localStorage.getItem('isCorporate')){
        this.isCorporateUser = localStorage.getItem('isCorporate');
      }
      if (!localStorage.getItem('facilitatorAdmin')) {
        this.hideAssign = true;
      }
      if (localStorage.getItem('isCorporate')) {
        this.disableSave = true;
        this.disableButton = true;
      }
      this.taskType = data.taskDetail.type;
      this.isFloatingTask = data.taskDetail.floating;
      if (data.taskDetail.type == 'completed') {

        this.disableButton = true;
        this.hideAssign = true;
        this.showCompletedBy = true;

        if(data.taskDetail.completedBy){
          this.completedBy = data.taskDetail.completedBy.split("=")[1];
        }
        this.disableButton = true;
      }

      if (data.taskDetail.type == 'notstarted' && !this.isProcessPaused) {
        this.disableButton = true;
        this.snackBar.open('Task is not yet started', 'Close', {
          verticalPosition: 'top',
          panelClass: ['red-snackbar'],
          
        });
      }
      if (this.isProcessPaused) {
        this.disableButton = true;
        this.snackBar.open('Process is in Paused State', 'Warning', {
          verticalPosition: 'top',
          panelClass: ['red-snackbar'],
        });

      }

      let userID = 'No';
      if (data.taskDetail.statusId == 3) {
        userID = data.taskDetail.completedBy.split("=")[0];
      }

      this.taskService.getUsersInPlant(plantID, userID).subscribe(userDetails => {
        let userid = data.taskDetail.assignedTo;
        if (userid) {
          userid = userid.split("=")[0];
        }
        if (data.taskDetail.type == 'completed') {
          userid = userID.split("=")[0];;
        }
        for (var detail of userDetails) {
          var obj = {
            "id": detail[0],
            "email": detail[1],
            "name": detail[2]
          }
          if (detail[0].toString() == userid) {
            this.assignedTo = detail[2];
          }
          this.userData.push(obj);
          this.loading = false;
        }
        if(userDetails && userDetails.length ==0){
          this.loading = false;
        }
      },
        error => {
          console.log("eror in task " + error.message);
          this.loading = false;
        });

      if(data.taskDetail.startDate){
        this.date = formatDate(new Date(data.taskDetail.startDate), 'MM/dd/yyyy', 'en');
      }

      if (data.taskDetail.type == 'completed') {

        
      }

      if (data.taskDetail.typeId == 2) {
        this.showSkip = true;
      }

/*      this.addForm = new FormGroup({
        'taskName': new FormControl(data.taskDetail.taskName, [
        ]),
        'date': new FormControl(this.date, [
        ]),
        'members': new FormControl(this.members, [
        ]),
        'comments': new FormControl(this.comments, [
        ]),
        'links': new FormControl(this.links, [
        ]), 
        'user': new FormControl(this.user, [Validators.required]),
      });

      this.addForm.setValue(this.DefaultFormValues);
      //this.comments = data.taskDetail.commentsList;
*/    
// ************************************** log ***************************************
      console.log("Data--=" + this.date + " --- " + JSON.stringify(this.data, null, " "));
// ************************************** log ***************************************t

      /* this.CommentForm = new FormGroup({
        'taskTransId': new FormControl(this.data.taskDetail.transactionId, [
        ]),
        'commentText': new FormControl(this.data.taskDetail.commentsList, [
        ]),
        'completedBy': new FormControl(this.userName, [
        ]),
        'commentDate': new FormControl(this.date, [
        ]),
      })
*/
      /*
      this.data.taskDetail.linksToDoc[0]="4 "+this.data.taskDetail.linksToDoc[0];
      this.data.taskDetail.linksToDoc[1]="2 "+this.data.taskDetail.linksToDoc[1];
      this.data.taskDetail.linksToDoc[2]="3 "+this.data.taskDetail.linksToDoc[2];
      this.data.taskDetail.linksToDoc[3]="1 "+this.data.taskDetail.linksToDoc[3];
      
      this.data.taskDetail.linksToDoc = this.data.taskDetail.linksToDoc.sort();

      let a = this.data.taskDetail.linksToDoc[0];
      let b = this.data.taskDetail.linksToDoc[1];
      let c = this.data.taskDetail.linksToDoc[2];
      let d = this.data.taskDetail.linksToDoc[3];
      console.log("links: "+ a,b,c,d);

      if(!isNaN(a.charAt(0))){a = a.substring(a.split("h", 1)[0].length)};
      if(!isNaN(b.charAt(0))){b = b.substring(b.split("h", 1)[0].length)};
      if(!isNaN(c.charAt(0))){c = c.substring(c.split("h", 1)[0].length)};
      if(!isNaN(d.charAt(0))){d = d.substring(d.split("h", 1)[0].length)};

      console.log(a);
      console.log(b);
      console.log(c);
      console.log(d);
      
      //this.data.taskDetail.linksToDoc=[a+", "+b+", "+c+", "+d];
      this.data.taskDetail.linksToDoc=[a,b,c,d];
      console.log(JSON.stringify(this.data.taskDetail.linksToDoc));
*/
    }
  }

  ngOnInit(): void {
    
    this.userName = localStorage.getItem('userName');
    let processId = parseInt(localStorage.getItem("processID"));
    if (processId == 1) {
      this.currentProcess = "Preparation";
      this.data.taskDetail.processId = 1;
    }
    if (processId == 2) {
      this.currentProcess = "Assessment";
      this.data.taskDetail.processId = 2;
    }
    if (processId == 3) {
      this.currentProcess = "Implementation";
      this.data.taskDetail.processId = 3;
    }
    if (processId == 4) {
      this.currentProcess = "Follow Up";
      this.data.taskDetail.processId = 4;
    }
    if (processId == 5) {
      this.currentProcess = "Sustainability";
      this.data.taskDetail.processId = 5;
    }
    
     console.log("Data commentlist--=" + this.date + " --- " + JSON.stringify(this.data.taskDetail.commentList, null, " "));
  
    this.commentsList = this.data.taskDetail.commentList;
    //console.log(this.commentsList);
    this.checkWave();
    //console.log(this.checkWave());
  }

  

  save(buttonValue): void {
    this.loading = true;
    this.task = new Task();
    this.task.comment = this.comments;
    this.task.dependency = this.data.taskDetail.dependency;
    this.task.input = buttonValue;
    this.task.userId = Number(localStorage.getItem("userID"));
    this.task.plantId = Number(localStorage.getItem("plantID"));
    this.task.processId = this.data.taskDetail.processId;
    this.task.sequenceNo = this.data.taskDetail.sequenceNumber;
    this.task.taskTransId = this.data.taskDetail.transactionId;
    this.task.taskType = this.data.taskDetail.typeId;
    this.task.totalNoOfTask = this.data.taskDetail.totalNoOfTask;
    this.task.taskId = this.data.taskDetail.taskId;
    this.task.processTrnsID = this.data.taskDetail.processTransId;
    this.task.floating = this.isFloatingTask;
    this.task.taskFlag = this.data.taskDetail.taskFlag;
	  this.task.version = this.data.taskDetail.version;
    this.task.completedBy = localStorage.getItem("userID") + "=" + localStorage.getItem("userName");
    let prID = localStorage.getItem("processID");
    var splitPrId = prID.split("=");
    if (splitPrId.length > 1) {
      this.task.sprint = splitPrId[1];
    }
   
    if(this.data.taskDetail.floating && this.data.taskDetail.totalNonFloatigTask == 0){
      this.task.completeProcess  = true;
       
    }
    if(!this.data.taskDetail.floating && this.data.taskDetail.totalNFloatingTask == 1){
      this.task.completeProcess  = true;
    }
	  this.task.completedTasks = this.data.taskDetail.completedTasks;
    this.task.wave = localStorage.getItem("nwaveSelected");
    this.taskService.processTask(this.task).subscribe(taskDetail => {
      this.nextTaskDetail = taskDetail;
      //console.log(taskDetail);
      this.loading = false;
      this.isLastTask = this.nextTaskDetail.lastTask;
      this.nextTaskInformation = true;
      //this.googleAnalyticsService.eventEmitter("Task Completed", "Task  ", "Task Completed", "Task Completed", "");
      var plantName = localStorage.getItem("plantName");
      var plantCode = localStorage.getItem("code");
      var email = localStorage.getItem("email");
      this.googleAnalyticsService.eventEmitter("Task Completion","Task Completion","Task Completion",this.data.taskDetail.name,this.data.taskDetail.transactionId,plantName,plantCode,email);
    },
      error => {
        console.log("eror in task " + error.message);
        this.loading = false;
        this.dialogRef.close();

        this.showErrorMessage = true;
        const dialogRef = this.dialog.open(TaskDialogComponent, { data: { error: error } });
      });
    }

  closePopUp() {

    let taskData = localStorage.getItem("taskOfPersonPerPlant");

    this.loading = true;

    this.snackBar.dismiss();
	  if(this.isLastTask){	
    let processID = localStorage.getItem("processID");	
    let routeSprint ='S1';	
    if(processID.indexOf("3=") >= 0 ){	
      let sprintArray = processID.split("=");	
      let sprint = sprintArray[1];	
       if(sprint == 'S1'){	
          routeSprint = 'S2';	
       }	
        if(sprint == 'S2'){	
          routeSprint = 'S3';	
        }	
        if(sprint == 'S3'){	
          routeSprint = 'S4';	
        }	
        if(sprint == 'S4'){	
          routeSprint = 'S5';	
        }	
        localStorage.setItem("processID", "3="+routeSprint);	
      }	
      
    }

    if (this.nextTaskInformation || this.assignmentSuccess || this.reOpenSuccess || this.postponeSuccess || this.reload) {
      this.reloadTasks();
    }else {
      this.dialogRef.close();
    }

  }

  reloadTasks() {
    this.dialogRef.close();
    this.PlantCodeserv.changeLoadingValue(true);
    this.taskService.retrieveuserTaskData(localStorage.getItem("processID")).subscribe(data => {
      data.startProcess = 'yes';
      data.showAddTask = true;
      let totalNumberOfTask: number = this.getTotalNumberOfTask(data);
      let completedTaskNo: number = data.taskTrans[0].completedTask.length;
      var totalCompletePercent: any;
      if (completedTaskNo == 0) {
        totalCompletePercent = 0;
      } else {
        totalCompletePercent = Math.round(Number(((completedTaskNo / totalNumberOfTask) * 100).toFixed(2)));
      }
      data.totalStatusPercent = totalCompletePercent;
	  let processID = localStorage.getItem("processID");	
      if(processID == '3=S2'){	
        this.data.implS2Selected = true;	
     }else if(processID == '3=S3'){	
        this.data.implS3Selected = true;	
        localStorage.setItem("processID", "3=S3");	
      }else if(processID == '3=S4'){	
        this.data.implS4Selected = true;	
      }else if(processID == '3=S5'){	
        this.data.implS5Selected = true;	
      }
      this.PlantCodeserv.changeTaskContent(data);
      this.PlantCodeserv.changeLoadingValue(false);
    },
      error => {
        error.startProcess = 'yes';
        this.PlantCodeserv.changeTaskContent(error);
        this.PlantCodeserv.changeLoadingValue(false);
      });
  }

  getTotalNumberOfTask(content: any) {
    let totalNoOfTask = content.taskTrans[0].completedTask.length +
      content.taskTrans[0].dueSoon.length +
      content.taskTrans[0].toDo.length +
      content.taskTrans[0].notSorted.length+
      content.taskTrans[0].overDue.length;
    return totalNoOfTask;
  }

  assignTo() {
    if (this.user) {
      this.errors = 'no';
      this.data.taskDetail.transactionId
      this.assignWindow = true;
    } else {
      this.errors = 'yes';
    }
  }
  assigneYes() {

    this.loading = true;
    let name = this.user.id + "=" + this.user.name;
    this.taskService.assignTask(this.data.taskDetail, name, this.user.email).subscribe(data => {
      this.assignmentSuccess = true;
      this.loading = false;
      this.assignTo = this.user.name;
      //this.googleAnalyticsService.eventEmitter("Task Assigned", "Task  Assignment", "Task Assigned", "Task Assigned","");
      var plantName = localStorage.getItem("plantName");
      var plantCode = localStorage.getItem("code");
      var email = localStorage.getItem("email");
      this.googleAnalyticsService.eventEmitter("Task Assigned","Task Assigned","Task Assigned",this.data.taskDetail.name,this.data.taskDetail.transactionId,plantName,plantCode,email);
    },
      error => {
        console.log("");
      }
    );
  }

  removeErrorIfAny() {
    this.errors = 'no';
  }

  exportAsXLSX() {
    this.task2 = JSON.parse(JSON.stringify(this.data.taskDetail)), this.data.taskDetail.result || [];
    var arr = Object.entries(this.task2).map(([type, value]) => ({ type, value }));

    //console.log(arr);
    this.excelService.exportAsExcelFile(arr, 'Task detail');

    //console.log("tarea: " + JSON.stringify(this.task2));
    //console.log("data stringify: " + JSON.stringify(this.data.taskDetail));
    //console.log("data parse: "+ JSON.parse(this.data.taskDetail));
  }

  goToLinkWithCodePlant(url: string) {
    window.open(url + String(this.PlantCode2), "_blank");
  }

  goToLinkoutplant(url: string) {
    window.open(url, "_blank");
  }

  public localStorageItem(): boolean {
    if (localStorage.getItem('URLgapAnalyzerLink') !== ""){
      //console.log(true);
      return true;
    } else {
      //console.log("false");
      return false;
    };
  }

  checkStatus(){
      return localStorage.getItem('URLgapAnalyzerLink') !== null;
  }

  reopen(){
    this.reopenFlag = true;
  }

  postpone(){
    this.postponeFlag = true;
  }

  reopenTask(reopen){
    if(reopen == 'yes'){
      this.loading = true;
      this.task = new Task();
      this.task.taskTransId = this.data.taskDetail.transactionId;
      this.task.userId = Number(localStorage.getItem("userID"));
      this.task.plantId = Number(localStorage.getItem("plantID"));
      this.task.processId = this.data.taskDetail.processId;
      this.task.taskId = this.data.taskDetail.taskId;
      this.task.taskFlag = this.data.taskDetail.taskFlag;
      this.task.reopenedBy =  localStorage.getItem("userID") + "=" + localStorage.getItem("userName");
      this.taskService.reopen(this.task).subscribe(data => {
        this.loading = false;
        this.reOpenSuccess = true;
        this.reopenFlag = false;
        //this.googleAnalyticsService.eventEmitter("Task Reopen", "Task  Reopen", "Task Reopened", "Task Reopned","");
        var plantName = localStorage.getItem("plantName");
        var plantCode = localStorage.getItem("code");
        var email = localStorage.getItem("email");
        this.googleAnalyticsService.eventEmitter("Task Reopen","Task Reopen","Task Reopen",this.data.taskDetail.name,this.data.taskDetail.transactionId,plantName,plantCode,email);
      },
        error => {
          this.reOpenSuccess = false;
          this.loading = false;
          this.reopenFlag = false;
        }
      );
    }else{
      this.reopenFlag = false;
    }

  }


  postponeTask(postpone){
    if(postpone == 'yes'){
      this.loading = true;
      this.task = new Task();
      this.task.taskTransId = this.data.taskDetail.transactionId;
      this.task.userId = Number(localStorage.getItem("userID"));
      this.task.plantId = Number(localStorage.getItem("plantID"));
      this.task.processId = this.data.taskDetail.processId;
      this.task.processTrnsID = this.data.taskDetail.processTransId;
      this.task.taskId = this.data.taskDetail.taskId;
      this.task.postponedBy =  localStorage.getItem("userID") + "=" + localStorage.getItem("userName");
      let prID = localStorage.getItem("processID");
      var splitPrId = prID.split("=");
      if (splitPrId.length > 1) {
        this.task.sprint = splitPrId[1];
      }
      this.task.completeProcess  = false;

      let totalEffectiveTask = this.data.taskDetail.totalNoOfTask - this.data.taskDetail.totalFloatingTask;
      if(totalEffectiveTask == 1){
        this.task.completeProcess  = true;
      }
      this.taskService.postpone(this.task).subscribe(data => {
        this.loading = false;
        this.postponeSuccess = true;
        this.postponeFlag = false;
        //this.googleAnalyticsService.eventEmitter("Task Postpone", "Task  Postpone", "Task Postponed", "Task Postponed","");
        var plantName = localStorage.getItem("plantName");
        var plantCode = localStorage.getItem("code");
        var email = localStorage.getItem("email");
        this.googleAnalyticsService.eventEmitter("Task Postpone","Task Postpone","Task Postpone",this.data.taskDetail.name,this.data.taskDetail.transactionId,plantName,plantCode,email);
      },
        error => {
          this.postponeSuccess = false;
          this.loading = false;
          this.postponeFlag = false;
        }
      );
    }else{
      this.postponeFlag = false;
    }

  }

UpdateComment(value: String): void{
  this.disableSave = true;
  this.comments=this.data.taskDetail.commentsList;
 
  //this.log =`${this.date}`+" "+`${this.userName}:`+"\n"+`${value}'\n`;
  this.log += this.date +" "+this.userName+":"+"\n"+`${value}`+"\n";
   this.comments +=`${value}\n`;
   // console.log(this.comments);
   //   console.log(this.log);
     let comment: Comment = { 
      taskTransId: this.data.taskDetail.transactionId,
      comment: value,
      user: this.userName,
      completedBy: this.userName,
      commentDate:  formatDate(new Date(), 'MM/dd/yyyy', 'en'),
      taskFlag : this.data.taskDetail.taskFlag
      // You can initialize/set other properties as well
    };
  this.commentsList.push(comment);
  
  
   this.taskService.writeComment(comment).subscribe(taskDetail => {
    // console.log('comment written');
    this.content='';
    //this.somePlaceholder= 'Saved.., any more?'
    this.reload = true;
    this.disableSave = false;
    //this.textareas.nativeElement.value='saved';
   }
        
    );
   
}
isProjectWavesRoute() {
  return this.router.url.includes("/ProjectWaves");
}

checkWave() :boolean{
  let nwavemax = Number(JSON.parse(localStorage.getItem('nwavemax')));
  let nwaveSelected = Number(JSON.parse(localStorage.getItem('nwaveSelected')));
  let processID = Number(this.data.taskDetail.processId);
  //let processID = Number(JSON.parse(localStorage.getItem('processID')));
  //console.log(nwavemax +" "+ nwaveSelected);
  //console.log (processID);
  if(nwavemax === nwaveSelected || processID > 3)
  {this.disablebywave=true; return true;}
  else
  {this.disablebywave=false; return false;}
};

/* 
addComment(){
  let comment: Comment = { 
    comment: this.comments, 
    taskTransId: this.data.taskDetail.transactionId,
    completedBy: this.userName,
    commentDate: this.date
    // You can initialize/set other properties as well
  };
 this.taskService.writeComment(comment);
}
*/

}
