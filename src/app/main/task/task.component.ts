import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { ThemePalette } from '@angular/material/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { PlantIdservice } from '../../services/PlantIdservice';
import { HttpClient } from "@angular/common/http";
import { TasksService } from 'src/app/services/tasks.service';
import { MatDialog } from '@angular/material/dialog';
import { TaskDialogComponent } from './task-dialog/task-dialog.component';
import {NewTaskComponent} from 'src/app/main/new-task/new-task.component';
@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
  //,changeDetection: ChangeDetectionStrategy.OnPush
})

export class TaskComponent implements OnInit {
  //@Input() data: MyData;
  @Input() PlantId;
  
  Total: 'string';
  color: ThemePalette = 'accent';
  mode: ProgressBarMode = 'determinate';
  bufferValue = 75;
  //PlantId: Object;
  content: any;
  taskDetail: any;
  startdate: Date;
  loading: any
  preparationSelected: Boolean = true;
  assessmentSelected: Boolean = false;
  implementationSelected: Boolean = false;
  followupSelected: Boolean = false;
  sustanabilitySelected: Boolean = false;
  isError: Boolean = false;
  processID: any = 1;
  panelOpenState = false;
  mouseoverText = false;
  isFacilitatorOrAdmin : String = 'no';
  nwaveSelected: number;


  constructor(private httpClient: HttpClient,
    private PlantIdserv: PlantIdservice, private taskService: TasksService, private dialog: MatDialog) { }

  ngOnInit(): void {
    this.PlantIdserv.currentPlantid.subscribe(PlantId => this.PlantId = PlantId);
    this.PlantIdserv.newcontent.subscribe(content => this.content = content);
    this.PlantIdserv.isLoading.subscribe(loading => this.loading = loading);
    this.content = {};
    this.loading = true;
    this.getProcessID();
    this.getTaskData();
    this.isFacilitatorOrAdmin =localStorage.getItem('facilitatorAdmin');
    this.nwaveSelected=JSON.parse(localStorage.getItem('nwaveSelected'));
  }

  ngOnChanges() {
    // create header using child_id
    console.log(this.PlantId);
  }

  getProcessID() {
    if (localStorage.getItem("processID")) {
      if (localStorage.getItem("processID") == "1") {
        this.processID = 1;
        this.preparationSelected = true;
      } if (localStorage.getItem("processID") == "2") {
        this.preparationSelected = false;
        this.assessmentSelected = true;
        this.processID = 2;
      } if (localStorage.getItem("processID") == "3") {
        this.preparationSelected = false;
        this.content.implS1Selected = true;
        this.processID = 3;
      } if (localStorage.getItem("processID") == "4") {
        this.preparationSelected = false;
        this.followupSelected = true;
        this.processID = 4;
      } if (localStorage.getItem("processID") == "5") {
        this.preparationSelected = false;
        this.sustanabilitySelected = true;
        this.processID = 5;
      }
      if (localStorage.getItem("processID") == "3=S1"){ 
        this.content.implS1Selected = true;
        this.preparationSelected = false;
        this.processID = "3=S1";
      }
      if (localStorage.getItem("processID") == "3=S2"){ 
        this.content.implS2Selected = true;
        this.preparationSelected = false;
        this.processID = "3=S2";
      }
      if (localStorage.getItem("processID") == "3=S3"){ 
        this.content.implS3Selected = true;
        this.preparationSelected = false;
        this.processID = "3=S3";
      }
      if (localStorage.getItem("processID") == "3=S4"){ 
        this.content.implS4Selected = true;
        this.preparationSelected = false;
        this.processID = "3=S4";
      }
      if (localStorage.getItem("processID") == "3=S5"){ 
        this.content.implS5Selected = true;
        this.preparationSelected = false;
        this.processID = "3=S5";
      }
    }
  }


  openTask(taskDetail, taskType,isProcessPaused) {
    taskDetail.type = taskType;
    let totalNoOfTask = this.getTotalNumberOfTaskInTodo(this.content);

    let totalFloatingTask =  this.getTotalFloatingTask(this.content);
    let totalNFloatingTask =  this.getTotalNonFloatingTask(this.content);
    taskDetail.userDetails = this.content.userDetails;
    taskDetail.totalNoOfTask = totalNoOfTask;
    taskDetail.totalFloatingTask = totalFloatingTask;
    taskDetail.totalNFloatingTask = totalNFloatingTask;
    let completedTasks = 0;
    if(this.content.taskTrans[0].completedTask && this.content.taskTrans[0].completedTask.length){
      completedTasks = this.content.taskTrans[0].completedTask.length;
    }
    taskDetail.completedTasks = completedTasks
    taskDetail.comments = taskDetail.observation;
    taskDetail.isProcessPaused = this.content.isProcessPaused;
    this.startdate = taskDetail.endDate;
    const dialogRef = this.dialog.open(TaskDialogComponent, { data: { taskDetail },autoFocus: false,
      maxHeight: '90vh' });
  }

  setProcessID(processID) {

    this.loading = true;
    this.preparationSelected = false;
    this.assessmentSelected = false;
    this.implementationSelected = false;
    this.followupSelected = false;
    this.sustanabilitySelected = false;

    if (processID == 1) {
      this.processID = 1;
      this.preparationSelected = true;
      localStorage.setItem("processID", "1");
    } else if (processID == 2) {
      this.processID = 2;
      this.assessmentSelected = true;
      localStorage.setItem("processID", "2");
    } else if (processID == 3) {
      this.processID = 3;
      this.implementationSelected = true;
      localStorage.setItem("processID", "3");
    } else if (processID == 4) {
      this.processID = 4;
      this.followupSelected = true;
      localStorage.setItem("processID", "4");
    }
    else if (processID == 5) {
      this.processID = 5;
      this.sustanabilitySelected = true;
      localStorage.setItem("processID", "5");
    }else if(processID == '3=S1'){
      this.processID = '3=S1';
      this.content.implS1Selected = true;
      localStorage.setItem("processID", "3=S1");
    }else if(processID == '3=S2'){
      this.processID = '3=S2';
      this.content.implS2Selected = true;
      localStorage.setItem("processID", "3=S2");
    }else if(processID == '3=S3'){
      this.processID = '3=S3';
      this.content.implS3Selected = true;
      localStorage.setItem("processID", "3=S3");
    }else if(processID == '3=S4'){
      this.processID = '3=S4';
      this.content.implS4Selected = true;
      localStorage.setItem("processID", "3=S4");
    }else if(processID == '3=S5'){
      this.processID = '3=S5';
      this.content.implS5Selected = true;
      localStorage.setItem("processID", "3=S5");
    }
    this.getTaskData();
  }

  checkContentForProcess() {
    this.preparationSelected = true;
    this.sustanabilitySelected = false;
    this.implementationSelected = false;
    this.followupSelected = false;
    this.assessmentSelected = false;
  }
  
  onChipSelect(value) {
    console.log('--');
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  getTaskData() {
    this.taskService.retrieveuserTaskData(this.processID).subscribe(data => {
      this.content = data;
      if (localStorage.getItem("processID") == "3" ) {
        this.content.implS1Selected = true;
      }
      if (localStorage.getItem("processID") == "3=S1"){
        this.content.implS1Selected = true;
      }
      if (localStorage.getItem("processID") == "3=S2"){
        this.content.implS2Selected = true;
      }
      if (localStorage.getItem("processID") == "3=S3"){
        this.content.implS3Selected = true;
      }
      if (localStorage.getItem("processID") == "3=S4"){
        this.content.implS4Selected = true;
      }
      if (localStorage.getItem("processID") == "3=S5"){
        this.content.implS5Selected = true;
      }
      if (!this.content.isProcessPaused) {
        let totalNumberOfTask: number = this.getTotalNumberOfTask(this.content);
        let completedTaskNo: number = this.content.taskTrans[0].completedTask.length;
        var totalCompletePercent: any;
        if (completedTaskNo == 0) {
          totalCompletePercent = 0.00;
        } else {
          totalCompletePercent = ((completedTaskNo / totalNumberOfTask) * 100).toFixed(2);
        }
        this.content.totalStatusPercent = Math.round(Number(totalCompletePercent));
      }
      this.content.showAddTask = false;
      if(this.content.processTransactionID){
        this.content.showAddTask = true;
      }
      this.loading = false;
    },
      error => {
        this.loading = false;
        this.isError = true;
        this.content = error;
      }
    );
  }
  
  getTotalNumberOfTask(content: any) {
    let totalNoOfTask = content.taskTrans[0].completedTask.length +
      content.taskTrans[0].dueSoon.length +
      content.taskTrans[0].toDo.length +
      content.taskTrans[0].notSorted.length +
      content.taskTrans[0].overDue.length;
    return totalNoOfTask;
  }

  getTotalNumberOfTaskInTodo(content: any) {
    let totalNoOfTask = content.taskTrans[0].dueSoon.length +
      content.taskTrans[0].toDo.length +
      content.taskTrans[0].notSorted.length+
      content.taskTrans[0].overDue.length;
    return totalNoOfTask;
  }

  getTotalFloatingTask(content :any){
    let totalFloatigTask = 0;
    if(content.taskTrans[0].dueSoon){
      for(var dueSoon of content.taskTrans[0].dueSoon){
        if(dueSoon.floating){
          totalFloatigTask ++;
        }
      }
    }

    if(content.taskTrans[0].toDo){
      for(var toDo of content.taskTrans[0].toDo){
        if(toDo.floating){
          totalFloatigTask ++;
        }
      }
    }

    if(content.taskTrans[0].notSorted){
      for(var notSorted of content.taskTrans[0].notSorted){
        if(notSorted.floating){
          totalFloatigTask ++;
        }
      }
    }

    if(content.taskTrans[0].overDue){
      for(var overDue of content.taskTrans[0].overDue){
        if(overDue.floating){
          totalFloatigTask ++;
        }
      }
    }
    return totalFloatigTask;
  }


  getTotalNonFloatingTask(content :any){
    let totalNonFloatigTask = 0;
    if(content.taskTrans[0].dueSoon){
      for(var dueSoon of content.taskTrans[0].dueSoon){
        if(!dueSoon.floating){
          totalNonFloatigTask ++;
        }
      }
    }

    if(content.taskTrans[0].toDo){
      for(var toDo of content.taskTrans[0].toDo){
        if(!toDo.floating){
          totalNonFloatigTask ++;
        }
      }
    }

    if(content.taskTrans[0].notSorted){
      for(var notSorted of content.taskTrans[0].notSorted){
        if(!notSorted.floating){
          totalNonFloatigTask ++;
        }
      }
    }

    if(content.taskTrans[0].overDue){
      for(var overDue of content.taskTrans[0].overDue){
        if(!overDue.floating){
          totalNonFloatigTask ++;
        }
      }
    }
    return totalNonFloatigTask;
  }

  goToWorkflowDiagram() {
    window.open("https://drive.google.com/file/d/1W2CLImjiCLPrrwjbzpAbp64wWcupZPtu/view?usp=sharing" + "_blank");
  }

  newTask(){
    const dialogRef = this.dialog.open(NewTaskComponent, { data: { processTransactionId:this.content.processTransactionID} });
  }
}

