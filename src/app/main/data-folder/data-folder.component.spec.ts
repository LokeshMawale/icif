import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataFolderComponent } from './data-folder.component';

describe('DataFolderComponent', () => {
  let component: DataFolderComponent;
  let fixture: ComponentFixture<DataFolderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataFolderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataFolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
