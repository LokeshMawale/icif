import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';
import { RoleService } from './../../../../services/role.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-editRole.dialog',
  templateUrl: './editRole.dialog.html',
  styleUrls: ['./editRole.dialog.scss']
})

export class EditRoleDialogComponent implements OnInit {
  
  constructor(public dialogRef: MatDialogRef<EditRoleDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public RoleService: RoleService) { }
  
  ngOnInit(): void { }

  formControl = new FormControl('', [
    Validators.required
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('name') ? 'Not a valid name' :
        '';
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.RoleService.updateRole(this.data);
  }
}
