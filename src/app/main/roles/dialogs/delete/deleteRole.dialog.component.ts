
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { RoleService } from '../../../../services/role.service';

@Component({
  selector: 'app-deleteRole.dialog',
  templateUrl: './deleteRole.dialog.html',
  styleUrls: ['./deleteRole.dialog.scss']
})
export class DeleteRoleDialogComponent {

  constructor(public dialogRef: MatDialogRef<DeleteRoleDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public RoleService: RoleService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.RoleService.deleteRole(this.data.id);
  }
}
