import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, Input, OnInit, ɵConsole, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Role } from './../../../../model/Role';
import { RolesComponent, RolesDataSource } from '../../roles.component';
import { RoleService } from '../../../../services/role.service';
import { OpcoService } from './../../../../services/Opco.service';

@Component({
  selector: 'app-addRole.dialog',
  templateUrl: './addRole.dialog.html',
  styleUrls: ['./addRole.dialog.scss'],
})

export class AddRoleDialogComponent implements OnInit , AfterViewInit {
  name: String = "name";
  idvalue: number;
  type: any[] = [{id: "1", name: "Cement Plant"}];
  typelist: any[] = [{id: "1", name: "Cement Plant"}, {id:"2", name:"Grinding Station"}];
  opcoIdList: any[] = [{opcoId:'1',name:'Argentina'}, {opcoId:'2',name:'Brazil'}, {opcoId:'3', name:	'Costa Rica'}];
  RolesDataSource: RolesDataSource;
  addForm: FormGroup;
  DefaultFormValues: Object ={
    name: '',
    type: '',
    description: ''
  };
  RolesComponent: any;

  constructor(public dialogRef: MatDialogRef<AddRoleDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Role,
              public RoleService: RoleService,
              public fb: FormBuilder)  { 
              this.name=this.data.name;
              this.addForm = new FormGroup({
                  'name': new FormControl('', [Validators.required, Validators.maxLength(40), Validators.minLength(3)]),
                  'type': new FormControl('', [Validators.required, Validators.maxLength(40), Validators.minLength(2)]),
                  'description': new FormControl('', [Validators.required, Validators.maxLength(40), Validators.minLength(2)]),
                });
                this.addForm.setValue(this.DefaultFormValues);
              }
              formControl = new FormControl('', [Validators.required]);
              
  ngOnInit(): void { }
  
  ngAfterViewInit(): void {
    this.addForm.reset(this.DefaultFormValues);    
  }
  
  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('name') ? 'Not a valid name' : '';
  }
  
  submit() {
  // emppty stuff
  }

  reset(){
    this.addForm.reset(this.DefaultFormValues);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.RoleService.addRole(this.data);
    console.log(this.data);
  }
}
