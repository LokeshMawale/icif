import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Role } from './../../model/role';
import { RoleService } from './../../services/role.service';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/table';
import { AddRoleDialogComponent } from './dialogs/add/addRole.dialog.component';
import { EditRoleDialogComponent } from './dialogs/edit/editRole.dialog.component';
import { DeleteRoleDialogComponent } from './dialogs/delete/deleteRole.dialog.component';
import { BehaviorSubject, fromEvent, Observable, merge, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})

export class RolesComponent implements OnInit, OnDestroy {

  Regions: any[];
  Areas: any[];
  PlantForm: FormControl;
  displayedColumns = ['id', 'name', 'type', 'description', 'actions'];
  RolesDataBase: RoleService | null;
  dataSource: RolesDataSource | null;
  index: number;
  id:number;
  selectedValue: number;
  formDetail: any;
  selected: string;
  private paramsubscriptions: Subscription[] = [];
  FormControl = new FormControl('', [Validators.required]);
  createRolesForm: FormGroup;

  constructor(
    private roleService: RoleService,
    public httpClient: HttpClient,
    public dialog: MatDialog,
    private toastr: ToastrService,
    public fb: FormBuilder
    //private toasterServices: ToasterServices,
  ) { this.selected = 'Loading' }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  ngOnInit(): void {
    this.loadData();
  }

  getErrorMessage() {
    return this.FormControl.hasError('required') ? 'Required field' :
      this.FormControl.hasError('name') ? 'Not a valid name' : '';
  }

  ngOnDestroy(): void {
    this.paramsubscriptions.forEach(subscription => subscription.unsubscribe());
  }

  refresh() {
    this.loadData();
  }

  addNew(role?: Role) {
    const dialogRef = this.dialog.open(AddRoleDialogComponent, {
      data: { role: role }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside plantService
        this.RolesDataBase.dataChange.value.push(this.roleService.getDialogData());
        this.refreshTable();
        this.loadData();
      }
    });
  }

  startEdit(i: number, id: number, name: string, type: string, description: string) {
    this.id = id;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    const dialogRef = this.dialog.open(EditRoleDialogComponent, {
      data: { id: id, name: name, type: type, description: description}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside plantService by id
        const foundIndex = this.RolesDataBase.dataChange.value.findIndex(x => x.id === this.id);
        // Then you update that record using data from dialogData (values you enetered)
        this.RolesDataBase.dataChange.value[foundIndex] = this.roleService.getDialogData();
        // And lastly refresh table
        this.refreshTable();   
        this.loadData();
      }
    });
  }

  deleteItem(i: number, id: number, name: string, type: string, description: string) {
    this.index = i;
    this.id = id;
    const dialogRef = this.dialog.open(DeleteRoleDialogComponent, {
      data: { id: id, name: name, type: type, description: description}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.RolesDataBase.dataChange.value.findIndex(x => x.id === this.id);
        // for delete we use splice in order to remove single object from DataService
        this.RolesDataBase.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
        this.loadData();
      }
    });
  }

  private refreshTable() {
    this.paginator._changePageSize(this.paginator.pageSize);
  }

  /*   // If you don't need a filter or a pagination this can be simplified, you just use code from else block
    // OLD METHOD:
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }*/

  public loadData() {
    this.RolesDataBase = new RoleService(this.httpClient, this.toastr);
    this.dataSource = new RolesDataSource(this.RolesDataBase, this.paginator, this.sort);
    fromEvent(this.filter.nativeElement, 'keyup')
      // .debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });
  }
}

export class RolesDataSource extends DataSource<Role> {
  _filterChange = new BehaviorSubject('');

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Role[] = [];
  renderedData: Role[] = [];

  constructor(public _RolesDatabase: RoleService,
    public _paginator: MatPaginator,
    public _sort: MatSort) {
    super();
    // Reset to the first page when the Plant changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Role[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._RolesDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    this._RolesDatabase.getAllRoles();

    return merge(...displayDataChanges).pipe(map(() => {
      // Filter data
      this.filteredData = this._RolesDatabase.data.slice().filter((Role: Role) => {
        const searchStr = (Role.id + Role.name + Role.type + Role.description).toLowerCase();
        console.log(Role.id);
        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    }
    ));
  }

  disconnect() { }

  /** Returns a sorted copy of the database data. */
  sortData(data: Role[]): Role[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string = '';
      let propertyB: number | string = '';

      switch (this._sort.active) {
        case 'id': [propertyA, propertyB] = [a.id, b.id]; break;
        case 'name': [propertyA, propertyB] = [a.name, b.name]; break;
        case 'type': [propertyA, propertyB] = [a.type, b.type]; break;
        case 'description': [propertyA, propertyB] = [a.description, b.description]; break;
      }
      
      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}