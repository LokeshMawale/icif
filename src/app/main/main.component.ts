import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit, AfterContentChecked, ViewChild, HostListener } from '@angular/core';
import { SocialAuthService } from 'angularx-social-login';
import { Socialusers } from '../model/socialusers';
import { ActivatedRoute, RouterStateSnapshot } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { TokenInterceptor } from './../services/interceptor';
import { TokenStorageService } from './../services/token-storage.service';
import { PlantIdservice } from '../services/PlantIdservice';
import { ProfileService } from '../services/profile.service';
import { Profile } from './../model/profile';
import { TasksService } from '../services/tasks.service';
import { formatDate } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';
import { MatTableDataSource } from '@angular/material/table';
import { slideInAnimation } from '../route-animation';
import { MediaObserver } from '@angular/flex-layout';
import { BehaviorSubject } from 'rxjs';
import { MatSidenav } from '@angular/material/sidenav';
import { environment } from 'src/environments/environment.prod';
import { SearchDataService } from 'src/app/services/search-data-service.service';
import { GoogleAnalyticsService } from 'src/app/services/google-analytics.service';


interface Plant {
  plantId?: number;
  name?: string;
  code?: string;
  smartsheetLink?: string;
  madiLink?: string;
  gapAnalyzerLink?: string
}

export interface taskElements {
  date: string;
  status: string;
  category: string;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  animations: [slideInAnimation]
})

export class MainComponent implements OnInit, AfterContentChecked {

  socialusers = new Socialusers();
  TokenInterceptor = new TokenInterceptor();
  loading: any;
  loadSearch :any;
  //Parameters of total advance % progress bar
  Total: 'string';
  profileForm: FormGroup;
  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'determinate';
  value: number;
  //bufferValue: number = 75;
  Router: any;
  plt: any[];
  events: string[] = [];
  opened: boolean;
  plantControl = new FormControl('', Validators.required);
  selectFormControl = new FormControl('', Validators.required);
  //load default value in case not answer of api data from me data of the users plants related.
  plants: Plant[] = [{ plantId: 1, name: 'Loadding plants...', code: 'CS', smartsheetLink: 'test', madiLink: 'test', gapAnalyzerLink: 'test' }];
  public summaries: any[];
  PlantId: Object;
  PlantCode: string;
  PlantCode2: string;
  URLSmartSheetlink: String = "";
  URLMADIToollink: String = "";
  URLgapAnalyzerLink: String = "";
  profile: Profile = { id: 0, name: "", email: "", imageUrl: "", plants: [{ plantId: 0, name: "", code: "", smartsheetLink: "", madiLink: "", gapAnalyzerLink: "" }] };
//  waves: number[]= [1,2,3];
  waves: number[];
  nwaves: number;
  nwavemax: number;
  nwaveSelected: number;
  //nwaveSelectedHistory: number;
  errorMessage: any;
  content: any = Object;
  isAdmin: Boolean = false;
  noUserAccessRole: Boolean = false;
  noPlantsAssigned: Boolean = false;
  isSmartsheetURL: Boolean = false;
  ELEMENT_DATA: taskElements[] = [];
  environmentTitle : String = ''
  environment : String = 'dev';

  screenWidth: number;
  private screenWidth$ = new BehaviorSubject<number>(window.innerWidth);
  @ViewChild('sidenav') sidenav: MatSidenav;
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth$.next(event.target.innerWidth);
  }
  constructor(
    private PlantIdserv: PlantIdservice,
    private PlantCodeserv: PlantIdservice,
    private NArrayWavesserv: PlantIdservice,
    private NWavesserv: PlantIdservice,
    public userService: UserService,
    public authService: SocialAuthService,
    public media: MediaObserver,
    private route: ActivatedRoute,
    private TokenStorageService: TokenStorageService,
    private fb: FormBuilder,
    private router: Router,
    private ProfileService: ProfileService,
    public OAuth: SocialAuthService,
    private taskServive: TasksService,
    private cookieService: CookieService,
    private cdr: ChangeDetectorRef,
    private searchDataService: SearchDataService,
    private googleAnalyticsService: GoogleAnalyticsService) {
    const snapshot: RouterStateSnapshot = router.routerState.snapshot;
  }

  ngOnInit(): void {
    // get the Plant name to progress bar name plant'
    if (window.location.hostname === 'localhost' || window.location.hostname == 'dev-dot-emea-lh-cif.appspot.com') {
      this.environmentTitle = environment.devTitle;
      this.environment = 'dev';
    }

    if (window.location.hostname === 'emea-lh-cif.appspot.com') {
      this.environmentTitle = environment.testTitle;
      this.environment = 'test';
    }
    if (window.location.hostname === 'cem-icif-prod.ew.r.appspot.com') {
      this.environmentTitle = environment.prodTitle;
      this.environment = 'prod';
    }

    this.loading = true;
    this.loadSearch = true;
    // review size screen
    this.screenWidth$.subscribe(width => { this.screenWidth = width;});
    // this.PlantIdserv.GetPlantPercent();
    // this.PlantIdserv.getidplant(this.profile.plants[0].name);
    // listerner for plants information from profile "me" api data.
    this.PlantIdserv.currentPlantid.subscribe(PlantId => this.PlantId = PlantId);
    this.PlantCodeserv.currentPlantCode.subscribe(PlantCode => this.PlantCode = PlantCode);

    // this.NArrayWavesserv.ArrayWaves.subscribe(waves => this.waves = waves);

    // make the login to social network using firebase properly configuration.
    this.socialusers = JSON.parse(localStorage.getItem('socialusers'));
    const firstParam: string = this.route.snapshot.queryParamMap.get('token');
    // localStorage.setItem('token', firstParam);
    // suscribe the get list plants from api to show in dropdown list in the main screen
    this.ProfileService.GetProfile().subscribe(profile => {
      this.profile = profile;
      console.log(this.profile);
      this.profile.plants.sort((a, b) => a.name.localeCompare(b.name));
      //this.PlantId= this.profile.plants[0].name;
      this.plants = this.profile.plants;
      this.PlantCode2 = localStorage.getItem('code');
      //this.PlantCode2 = this.profile.plants[0].code;
      this.URLSmartSheetlink = localStorage.getItem('URLSmartSheetlink');
      this.URLMADIToollink = localStorage.getItem('URLMADIToollink');
      this.URLgapAnalyzerLink = localStorage.getItem('URLgapAnalyzerLink');
      //this.URLSmartSheetlink = this.profile.plants[0].smartsheetLink;
      //make the output to the log
      //console.log(this.profile);
      //console.log(this.plants);
      //Added on function checklocalStorage for plantID.
      //localStorage.setItem("userID", this.profile.id.toString());
      //localStorage.setItem("plants", JSON.stringify(this.plants))
      //let plants = JSON.parse(localStorage.getItem("plants")); localStorage.setItem("plantID", plants[0].plantId);
      //localStorage.setItem("plantName", plants[0].name);
      //this.PlantId = localStorage.getItem('plantName');
      this.checklocalstorage();
      this.checkIfUserRoles();
      this.loadSearch = false;
      this.loadSearchData();
      this.loading = false;
      this.cdr.detectChanges;
    },
      error => {
        console.log(error);
        setTimeout(() => this.noUserAccessRole = true);
        this.loading = false;
        this.cdr.detectChanges;
      }),
      error => this.errorMessage = <any>error;
    //=this.profile[0].plants[0].name;
    //this.ProfileService.GetProfile(this.profile)=this.profile;
    /*console.log(this.URLSmartSheetlink);
    if(this.URLSmartSheetlink!=""){ //check if smartsheet link contain data or not.
      this.isSmartsheetURL=true}
      else{
        this.isSmartsheetURL=false};
        console.log(this.isSmartsheetURL);*/

  }

  loadSearchData() {

    this.loadSearch= true;

    this.userService.getMapStatus().subscribe(data => {

      if( data.message== 'empty'){
        localStorage.removeItem("allsearchResults");
      }
      this.getAllSearchResults();
    },
    error => {
      console.log('error search result in fatching the data');
      this.loadSearch = false;
      }
   );

  }

  private getAllSearchResults() {
    if (!localStorage.getItem("allsearchResults")) {

      this.userService.getAllSearchResults().subscribe(data => {
        localStorage.setItem("allsearchResults", "set");
        this.searchDataService.changeMessage(data);
        this.loadSearch = false;
      },
        error => {
          console.log('error search result in fatching the data');
          this.loadSearch = false;
        }
      );
    } else {
      this.loadSearch = false;
    }
  }

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }

  checklocalstorage() {
    if (!localStorage.getItem('plantName')) {
      //this.PlantId = localStorage.getItem('plantName');
      localStorage.setItem("userID", this.profile.id.toString());
      //localStorage.setItem("userName", JSON.stringify(this.profile.name));
      localStorage.setItem("userName", this.profile.name);
      localStorage.setItem("email", this.profile.email);
      localStorage.setItem("URLSmartSheetlink", this.profile.plants[0].smartsheetLink);
      localStorage.setItem("URLMADIToollink", this.profile.plants[0].madiLink);
      localStorage.setItem("URLgapAnalyzerLink", this.profile.plants[0].gapAnalyzerLink);
      localStorage.setItem("code", this.profile.plants[0].code);
      localStorage.setItem("plants", JSON.stringify(this.plants));
      let plants = JSON.parse(localStorage.getItem("plants")); localStorage.setItem("plantID", plants[0].plantId);
      localStorage.setItem("plantName", plants[0].name);
      this.PlantId = localStorage.getItem('plantName');
      this.URLSmartSheetlink = localStorage.getItem('URLSmartSheetlink');
      this.URLMADIToollink = localStorage.getItem('URLMADIToollink');
      this.URLgapAnalyzerLink = localStorage.getItem('URLgapAnalyzerLink');
      //console.log(this.PlantId);
      //console.log(JSON.stringify(plants[0].name));
    } else {
      this.PlantId = localStorage.getItem('plantName');
      //      this.PlantId = localStorage.getItem('plantName');
      //console.log("localstorage with data: " + this.PlantId);
    }

    this.PlantIdserv.getAllClosedWave()
    .subscribe(data => {
      this.nwaves=data;
      //this.nwaves=(Number(this.nwaves)+2);
      localStorage.setItem('waves', JSON.stringify(this.rangewaves(Number(this.nwaves)+1)));
      localStorage.setItem('Nowaves', data);
      this.nwaveSelected = this.waves.reduce((a, b)=>Math.max(a, b));
      localStorage.setItem('nwaveSelected', JSON.stringify(this.nwaveSelected));
      this.nwavemax = this.waves.reduce((a, b)=>Math.max(a, b));
      localStorage.setItem('nwavemax', JSON.stringify(this.nwavemax));
      //this.nwaveSelectedHistory=this.nwaveSelectedHistoryFunc(this.nwaves);
      //localStorage.setItem('nwaveSelectedHistory', JSON.stringify(this.nwaveSelectedHistory));
      this.NArrayWavesserv.changeNArrayWaves(this.waves);
      this.NWavesserv.changeWaveSelected(this.nwaveSelected);
      //localStorage.setItem('waves', JSON.stringify(this.waves));
      localStorage.setItem('nwaveSelected', JSON.stringify(this.nwaveSelected));
      localStorage.setItem('viewClosedWave', '1');
    });
  }

  //Check that the return this.PlantIdserv.getAllClosedWave() allways was 1 wave not 0 or empty wave
  nwaveSelectedHistoryFunc(nwaves: number){
  //let nwaves = localStorage.getItem('waves');
  if (localStorage.getItem("Nowaves") === ""){
    this.nwaves=this.nwaves+1;
    localStorage.setItem('Nowaves', JSON.stringify(Number(this.nwaves)));
    console.log("sutituido valor 0 de nwaves al venir vacio");
    return Number(this.nwaves);
  }
    else{
      return Number(nwaves);
    }
}

  checkIfUserRoles() {
    if (this.profile && this.profile.roles) {
      for (var role of this.profile.roles) {
        if (role.name == 'Admin') {
          this.isAdmin = true;
          localStorage.setItem('facilitatorAdmin', 'yes');
          this.content.isFacilitatorAdmin = true;
        }
        if (role.name == 'Cif_Facilitator') {
          localStorage.setItem('facilitatorAdmin', 'yes');
          this.content.isFacilitatorAdmin = true;
        }
        if (role.name == 'Corporate_Role') {
          localStorage.setItem('isCorporate', 'yes');
        }
      }
    }
  }

  //function to load the changes from dropdown list in main menu

  navigateTo(value) {
    this.PlantIdserv.changeplantid(value);
    this.PlantCodeserv.changeplantcode(value);
    this.value = Number(this.Total);
    let plants = JSON.parse(localStorage.getItem("plants"));
    for (var plant of plants) {
      if (plant.name == value) {
        localStorage.setItem("plantID", plant.plantId);
        localStorage.setItem("plantName", plant.name);
        localStorage.setItem("URLSmartSheetlink", plant.smartsheetLink);
        localStorage.setItem("URLMADIToollink", plant.madiLink);
        localStorage.setItem("URLgapAnalyzerLink", plant.gapAnalyzerLink);
        localStorage.setItem("code", plant.code);
        var userName = localStorage.getItem("userName");
        var email = localStorage.getItem("email");
        this.googleAnalyticsService.eventEmitter("Plant "+plant.name,"Plant "+plant.name,"Plant "+plant.name,plant.code,email,plant.name,plant.code,email);
        this.PlantCode2 = plant.code;
        this.URLSmartSheetlink = plant.smartsheetLink;
        if (plant && plant.madiLink) {
          this.URLMADIToollink = plant.madiLink;
        }
        //console.log(plant.gapAnalyzerLink);
        if (!plant.gapAnalyzerLink) {
          localStorage.setItem("URLgapAnalyzerLink", "");
        } else {this.URLgapAnalyzerLink = plant.gapAnalyzerLink};
        //this.URLMADIToollink = plant.madiLink;
        this.ngOnInit();
        //console.log(this.URLSmartSheetlink);
        //console.log(this.PlantId);
        break;
      }
    }

    let currentRoute = this.router.url.split("?")[0];
    if (currentRoute == '/main/task') {
      //this.PlantCodeserv.changeLoadingValue(true);
      let prcesID = 1;
      if (localStorage.getItem("processID")) {
        prcesID = parseInt(localStorage.getItem("processID"));
      }else{
        localStorage.setItem("processID","1");
      }

      // this.taskServive.retrieveuserTaskData(prcesID).subscribe(data => {
      //   data.startProcess = 'yes';
      //   if (!data.isProcessPaused) {
      //     let totalNumberOfTask: number = this.getTotalNumberOfTask(data);
      //     let completedTaskNo: number = data.taskTrans[0].completedTask.length;
      //     var totalCompletePercent: any;
      //     if (completedTaskNo == 0) {
      //       totalCompletePercent = 0.00;
      //     } else {
      //       totalCompletePercent = ((completedTaskNo / totalNumberOfTask) * 100).toFixed(2);
      //     }
      //     data.totalStatusPercent = Math.round(Number(totalCompletePercent));
      //   }
      //   this.PlantCodeserv.changeTaskContent(data);
      //   this.PlantCodeserv.changeLoadingValue(false);
      // },
      //   error => {
      //     error.startProcess = 'yes';
      //     this.PlantCodeserv.changeTaskContent(error);
      //     this.PlantCodeserv.changeLoadingValue(false);
      //   });

      this.router.navigate(['/main/task-assignment'],{ queryParams: { routeToTask : 'yes',} });
    }
    if (currentRoute == '/main/main-detail'  || currentRoute == '/main') {
      // this.PlantCodeserv.changeLoadingValue(true);
      // this.content = {};
      // let isFacilitatorAdmin = localStorage.getItem('facilitatorAdmin');
      // this.content.isFacilitatorAdmin = false;
      // if (isFacilitatorAdmin == 'yes') {
      //   this.content.isFacilitatorAdmin = true;
      // }
      // var plantData = JSON.parse(localStorage.getItem(value));
      // if (plantData) {
      //   this.PlantCodeserv.changeTotalPercentContent(plantData.totalPercentages.toFixed(2));
      //   this.populatePlantData(plantData);
      //   this.getTaskPanelOverviewData();
      //   // this.PlantCodeserv.changeLoadingValue(false);
      // } else {
      //   this.PlantIdserv.selectedPlantPercentages().subscribe(data => {
      //     for (var fPlantData of data) {
      //       localStorage.setItem(fPlantData.plant_Name, JSON.stringify(fPlantData))
      //       plantData = fPlantData;
      //       this.PlantCodeserv.changeTotalPercentContent(plantData.totalPercentages.toFixed(2));
      //       this.populatePlantData(plantData);
      //       this.getTaskPanelOverviewData();
      //       // this.PlantCodeserv.changeLoadingValue(false);
      //     }
      //   },
      //     error => {
      //       this.PlantCodeserv.changeLoadingValue(false);
      //     }
      //   );
      // }
      this.router.navigate(['/main/task-assignment']);

    }

    this.PlantIdserv.getAllClosedWave()
    .subscribe(data => {
      this.nwaves=data;
      //this.nwaves=(Number(this.nwaves)+2);
      localStorage.setItem('waves', JSON.stringify(this.rangewaves(Number(this.nwaves)+1)));
      localStorage.setItem('Nowaves', data);
      this.nwaveSelected = this.waves.reduce((a, b)=>Math.max(a, b));
      localStorage.setItem('nwaveSelected', JSON.stringify(this.nwaveSelected));
      this.nwavemax = this.waves.reduce((a, b)=>Math.max(a, b));
      localStorage.setItem('nwavemax', JSON.stringify(this.nwavemax));
      // this.nwaveSelectedHistory=this.nwaveSelectedHistoryFunc(this.nwaves);
      //localStorage.setItem('nwaveSelectedHistory', JSON.stringify(this.nwaveSelectedHistory));
      this.NArrayWavesserv.changeNArrayWaves(this.waves);
      this.NWavesserv.changeWaveSelected(this.nwaveSelected);
      //localStorage.setItem('waves', JSON.stringify(this.waves));
      localStorage.setItem('nwaveSelected', JSON.stringify(this.nwaveSelected));
      localStorage.setItem('viewClosedWave', '1');
    });
  }
  searchDocument(value: any) {
    this.router.navigate(['/main/task-assignment'],{ queryParams: { searchValue: value,routeToSearch : 'yes'} });
    //this.router.navigate(['/main/search'], { queryParams: { searchValue: value } });
  }

  populatePlantData(plantData) {
    var processData = plantData.processPercentages;
    let percentPrep = 0;
    let percentAssesment = 0;
    let percentFollowUp = 0;
    let percentImplementation = 0;
    let percentSustain = 0;
    if (processData && processData.length > 0) {
      for (var process of processData) {
        if (process.processName == 'Preparation') {
          //  this.getCircleOptionPreparation(process.processPercentage.toFixed(2));
          percentPrep = process.processPercentage.toFixed(2);
          if (process.processStartDate) {
            this.content.prepStartDate = formatDate(new Date(process.processStartDate), 'MM/dd/yyyy', 'en');
          }
          if (process.statusID) {
            this.content.prepStatusID = process.statusID;
          }
          if (process.processPausedDate) {
            this.content.prepPauseDate = formatDate(new Date(process.processPausedDate), 'MM/dd/yyyy', 'en');
          }
          if (process.processCompletedDate) {
            this.content.prepCompletedDate = formatDate(new Date(process.processCompletedDate), 'MM/dd/yyyy', 'en');
          }
          if (process.processTransactID) {
            this.content.processTransactID = process.processTransactID;
          }
          if (process.processTransactID) {
            this.content.processPrepTransactID = process.processTransactID;
          }
          this.PlantCodeserv.changePreparationContent(process.processPercentage.toFixed(2));
        }
        if (process.processName == 'Assessment') {
          percentAssesment = process.processPercentage.toFixed(2);
          this.PlantCodeserv.changeAssesmentContent(process.processPercentage.toFixed(2));
          if (process.processStartDate) {
            this.content.asmentStartDate = formatDate(new Date(process.processStartDate), 'MM/dd/yyyy', 'en');
          }
          if (process.statusID) {
            this.content.assmntStatusID = process.statusID;
          }
          if (process.processPausedDate) {
            this.content.asmntPauseDate = formatDate(new Date(process.processPausedDate), 'MM/dd/yyyy', 'en');
          }
          if (process.processCompletedDate) {
            this.content.assmntCompletedDate = formatDate(new Date(process.processCompletedDate), 'MM/dd/yyyy', 'en');
          }
          if (process.processTransactID) {
            this.content.processTransactID = process.processTransactID;
          }
          if (process.processTransactID) {
            this.content.processAsmntTransactID = process.processTransactID;
          }
        }
        if (process.processName == 'Implementation') {
          percentImplementation = process.processPercentage.toFixed(2);
          this.PlantCodeserv.changeImplementationContent(process.processPercentage.toFixed(2));
          if (process.processStartDate) {
            this.content.implStartDate = formatDate(new Date(process.processStartDate), 'MM/dd/yyyy', 'en');
          }
          if (process.statusID) {
            this.content.implStatusID = process.statusID;
          }
          if (process.processPausedDate) {
            this.content.implPauseDate = formatDate(new Date(process.processPausedDate), 'MM/dd/yyyy', 'en');
          }
          if (process.processCompletedDate) {
            this.content.implCompletedDate = formatDate(new Date(process.processCompletedDate), 'MM/dd/yyyy', 'en');
          }
          if (process.processTransactID) {
            this.content.processImplTransactID = process.processTransactID;
          }
        }
        if (process.processName == 'Followup') {
          percentFollowUp = process.processPercentage.toFixed(2);
          this.PlantCodeserv.changeImplementationContent(process.processPercentage.toFixed(2));
          if (process.processStartDate) {
            this.content.foloupStartDate = formatDate(new Date(process.processStartDate), 'MM/dd/yyyy', 'en');
          }
          if (process.statusID) {
            this.content.foloupStatusID = process.statusID;
          }
          if (process.processPausedDate) {
            this.content.foloupPausedDate = formatDate(new Date(process.processPausedDate), 'MM/dd/yyyy', 'en');
          }
          if (process.processCompletedDate) {
            this.content.foloupCompletedDate = formatDate(new Date(process.processCompletedDate), 'MM/dd/yyyy', 'en');
          }
          if (process.processTransactID) {
            this.content.processFoloupTransactID = process.processTransactID;
          }
        }
        if (process.processName == 'Sustainability') {
          percentSustain = process.processPercentage.toFixed(2);
          this.PlantCodeserv.changeSustainpContent(process.processPercentage.toFixed(2));
          if (process.processStartDate) {
            this.content.sustainStartDate = formatDate(new Date(process.processStartDate), 'MM/dd/yyyy', 'en');
          }
          if (process.processPausedDate) {
            this.content.sustainPausedDate = formatDate(new Date(process.processPausedDate), 'MM/dd/yyyy', 'en');
          }
          if (process.statusID) {
            this.content.sustainStatusID = process.statusID;
          }
          if (process.processCompletedDate) {
            this.content.sustainCompletedDate = formatDate(new Date(process.processCompletedDate), 'MM/dd/yyyy', 'en');
          }
          if (process.processTransactID) {
            this.content.processSustainTransactID = process.processTransactID;
          }
        }
      }
    } else {
      this.PlantCodeserv.changePreparationContent(0);
      this.PlantCodeserv.changeAssesmentContent(0);
      this.PlantCodeserv.changeImplementationContent(0);
      this.PlantCodeserv.changeImplementationContent(0);
      this.PlantCodeserv.changeSustainpContent(0.00);
      this.content.prepStartDate = undefined;
      this.content.asmentStartDate = undefined;
      this.content.implStartDate = undefined;
      this.content.foloupStartDate = undefined;
      this.content.sustainStartDate = undefined;

    }

    this.checkStartButton(percentPrep, percentAssesment, percentImplementation, percentFollowUp, percentSustain);
  }

  checkStartButton(percentPrep: number, percentAssesment: number, percentImplementation: number, percentFollowUp: number, percentSustain: number) {
    if (percentPrep == 0) {
      this.content.enablePrep = true;
      this.content.enableAssement = false;
      this.content.enableImpl = false;
      this.content.enableFoloup = false;
      this.content.enableSustain = false;
    }

    if (percentAssesment == 0 && percentPrep == 100) {
      this.content.enablePrep = false;
      this.content.enableAssement = true;
      this.content.enableImpl = false;
      this.content.enableFoloup = false;
      this.content.enableSustain = false;
    }
    if (percentImplementation == 0 && percentAssesment == 100) {
      this.content.enablePrep = false;
      this.content.enableAssement = false;
      this.content.enableImpl = true;
      this.content.enableFoloup = false;
      this.content.enableSustain = false;
    }
    if (percentImplementation == 100 && percentFollowUp == 0) {
      this.content.enablePrep = false;
      this.content.enableAssement = false;
      this.content.enableImpl = false;
      this.content.enableFoloup = true;
      this.content.enableSustain = false;
    }
    if (percentSustain == 0 && percentFollowUp == 100) {
      this.content.enablePrep = false;
      this.content.enableAssement = false;
      this.content.enableImpl = false;
      this.content.enableFoloup = false;
      this.content.enableSustain = true;
    }
    this.PlantCodeserv.changeStartButtonEnableConte(this.content);
  }

  getTaskPanelOverviewData() {

    let plantData: any = JSON.parse(localStorage.getItem(localStorage.getItem('plantName')));
    let plantProcessData: any = plantData.processPercentages;
    let processID: any;
    let categ = "";
    for (var process of plantProcessData) {
      if (process.statusID == 1) {
        processID = process.processId
        if (processID == 1) {
          categ = "Preparation";
        }
        if (processID == 2) {
          categ = "Assessment";
        }
        if (processID == 3) {
          categ = "Implementation";
        }
        if (processID == 4) {
          categ = "Followup";
        }
        if (processID == 5) {
          categ = "Sustainability";
        }
        break;
      }
    }

    if (plantProcessData && processID) {
      this.ELEMENT_DATA = [];
      this.userService.getTasksForOverView(processID).subscribe(data => {
        console.log(data);

        if (data && data.taskTrans) {
          if (data.taskTrans[0].toDo) {
            for (var toDo of data.taskTrans[0].toDo) {
              let endate = formatDate(new Date(toDo.endDate), 'MM/dd/yyyy', 'en');
              var elem = {
                date: endate, status: 'To Do', category: categ
              }
              this.ELEMENT_DATA.push(elem);
            }

            for (var dueSoon of data.taskTrans[0].dueSoon) {
              let endate = formatDate(new Date(dueSoon.endDate), 'MM/dd/yyyy', 'en');
              var elem = {
                date: endate, status: 'Due Soon', category: categ
              }
              this.ELEMENT_DATA.push(elem);
            }

            for (var overDue of data.taskTrans[0].overDue) {
              let endate = formatDate(new Date(overDue.endDate), 'MM/dd/yyyy', 'en');
              var elem = {
                date: endate, status: 'Over Due', category: categ
              }
              this.ELEMENT_DATA.push(elem);
            }

            // for(var notSorted of data.taskTrans[0].notSorted){
            //   let endate = formatDate(new Date(notSorted.endDate), 'MM/dd/yyyy', 'en');
            //   var elem ={
            //      date: endate, status: 'Not Started', category: categ
            //   }
            //   ELEMENT_DATA.push(elem);
            // }
          }
        }
        this.content.dataSource = new MatTableDataSource<any>(this.ELEMENT_DATA);
        this.PlantIdserv.changeLoadingValue(false);
      }, error => {
        console.log("");
      }
      );
    } else {
      this.PlantIdserv.changeLoadingValue(false);
    }
  }

  getTotalNumberOfTask(content: any) {
    let totalNoOfTask = content.taskTrans[0].completedTask.length +
      content.taskTrans[0].dueSoon.length +
      content.taskTrans[0].toDo.length +
      content.taskTrans[0].notSorted.length;
    return totalNoOfTask;
  }
  //func. to load the url base for Dashboar data as final parameter of url
  goToLink(url: string) {
    window.open(url + '"' + this.PlantCode2 + '"', "_blank");
    //encodeURIComponent
  }

  goToDashBoardLink(url: string) {
    window.open(url + '"' + this.PlantCode2 + '"', "_blank");
  }
  goToLinkWithCodePlant(url: string) {
    var plantName = localStorage.getItem("plantName");
    var email = localStorage.getItem("email");
    var plantCode = localStorage.getItem("code");

    this.googleAnalyticsService.eventEmitter("Dashboard","Dashboard","Dashboard","Dashboard","Dashboard",plantName,plantCode,email);
    window.open(url + String(this.PlantCode2), "_blank");
  }

  goToLinkithPlantCode(url){
    var plantCode = localStorage.getItem("code");
    this.router.navigate([url], { queryParams: { plantCode:plantCode } });
  }
  openSite(URLSmartSheetlink) {
    window.open("//" + URLSmartSheetlink, '_blank');
    console.log(this.URLSmartSheetlink);
  }
  goToLinkoutplant(url: string) {
    //window.open(url, "_blank");
    window.open(url, "_blank");
  }
  //make the logOut
  logout() {
    //remove on exit the token key in the browser
    localStorage.clear();
    this.cookieService.deleteAll();
    //disconect from sociallogin google if exits.
    this.OAuth.signOut().then(data => {
      debugger;
    });
    //navigate on exit to login page
    this.router.navigateByUrl('/login');
  }

  goToWorkflow(version){
    localStorage.setItem('PROCESSVERSION',version);
    this.router.navigate(['/main/task-assignment'],{ queryParams: { routeToWorkflow : 'yes',} });
  }

rangewaves(nwaves){
  const range = Array.from({length: nwaves}, (_, i) => i + 1);
  this.waves=range;
  //localStorage.setItem('waves', JSON.stringify(this.waves));
  //console.log("range: "+range);
  //console.log("Array: "+Array.from({length: nwaves}, (_, i) => i + 1));
  return range;
  }

}
