import { Component, OnInit, Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute,Router } from '@angular/router';
import { SearchDataService } from 'src/app/services/search-data-service.service';
import { GoogleAnalyticsService } from 'src/app/services/google-analytics.service';


@Component({
  selector: 'app-document-details',
  templateUrl: './document-details.component.html',
  styleUrls: ['./document-details.component.scss']
})

export class DocumentDetailsComponent implements OnInit {

  constructor(private userService: UserService,private route: ActivatedRoute,
    private router: Router,private searchDataService: SearchDataService,private googleAnalyticsService: GoogleAnalyticsService ) { }

  src :any;
  id: any;
  obj = {documentId: '', liked:false, userId: ''};
  content: any;
  likedBy: number
  status: boolean = false;
  contentOfpeople: any;
  isCliked = false;
  showComment = false;
  isLiked = false;
  receivedSearchString: string;
  searchResult : any;

  show_Hide_Comment(){

    if(this.showComment)
      this.showComment=false;   
    else
      this.showComment= true;
    
  }

  likedByPeople(){
    
    this.userService.retrieveDocumentLikedBy(this.id).subscribe(
      data =>{
        this.contentOfpeople = data;
        this.isCliked=true;
      },
      error=>{
       console.log('error during like...');
      }
    );

  }

  likeBtn(documentId, liked) {

    this.obj.documentId=documentId;
    this.obj.liked=liked;
    var likesCount = 0;
    if(liked){
       likesCount = this.content.record.likesCount +1;
    }else{
       likesCount = this.content.record.likesCount -1;
    }

    for(var searchResult of this.searchResult.searchResult){
      if(this.id == searchResult.id){
        searchResult.likesCount = likesCount;
        var plantName = localStorage.getItem("plantName");
        var plantCode = localStorage.getItem("code");
        var email = localStorage.getItem("email");
        //this.googleAnalyticsService.eventEmitter("Document Like", "Document Like ", "Document Like ", "Task Completed", "docID");
        this.googleAnalyticsService.eventEmitter("Document Like","Document Like","Document Like",this.content.record.label,searchResult.likesCount,plantName,plantCode,email);

        break;
      }
    }

    // this.userService.increaseDocumentLike(documentId,likesCount).subscribe(data => {
    //   console.log('Like increased');
    //  },
    //  error => {
    //    console.log("error in increasing Like");
    //  });

    this.userService.document_likes(this.obj).subscribe(
      data =>{
        this.isLiked = liked;
      },
      error=>{
        this.isLiked = !liked;
      }
    );

  }

  backToSearch(theLink){
    console.log(theLink);
  }

  ngOnInit() {

    console.log('search : '+ this.userService.csdlSearchString);
    this.receivedSearchString = localStorage.getItem("searchTerm");
    this.id = this.route.snapshot.params.id;
    this.searchDataService.currentMessage.subscribe(searchData => (this.searchResult = searchData));
   
    
   
    this.userService.retrieve_Documente_Data(this.id).subscribe(data => {
      this.content = data;

      var plantName = localStorage.getItem("plantName");
      var plantCode = localStorage.getItem("code");
      var email = localStorage.getItem("email");
      this.googleAnalyticsService.eventEmitter("Document View","Document View","Document View",this.content.record.label,this.content.record.viewsCount,plantName,plantCode,email);
      this.isLiked = this.content.like;
      for(var searchResult of this.searchResult.searchResult){
        if(this.id == searchResult.id){
          searchResult.viewsCount = this.content.record.viewsCount;
          break;
        }
      }
      // var viewCount = this.content.record.viewsCount;
      // this.userService.increaseDocumentView(this.id,viewCount).subscribe(data => {
      //   console.log('view increased');
      //  },
      //  error => {
      //    console.log("erdor in increasing count");
      //  });
    },
    error => {
      console.log("eror in retriveTodo");
    });

  }

  goToSearch(value: any) {
    var plantCode = localStorage.getItem("code");
    this.router.navigate(['/main/search'], { queryParams: { searchValue: value,backArrow:'yes',plantCode:plantCode } });
  }

}