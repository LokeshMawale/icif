import { LeaderBoardComponent } from './leader-board/leader-board.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main.component';
import { UsersComponent } from './users/users.component';
import { RolesComponent } from './roles/roles.component';
import { PlantComponent } from './plant/plant.component';
import { WorkflowComponent } from './workflow/workflow.component';
import { ProcessflowComponent } from './processflow/processflow.component';
// import { ProjectWavesComponent } from './ProjectWaves/ProjectWaves.component';
import { MainDetailComponent } from './main-detail/main-detail.component';
import { TaskComponent } from './task/task.component';
import { TaskAssignmentComponent } from './task-assignment/task-assignment.component';
import { SearchComponent } from './search/search.component';
import { ContactComponent } from './contact/contact.component';
import { SysComponent } from './sys/sys.component';
import { DocumentDetailsComponent } from './document-details/document-details.component';
import { DataFolderComponent } from './data-folder/data-folder.component';
import { NewTaskComponent } from './new-task/new-task.component';
// import { AuthGuard } from '../auth/auth.guard';

const adminRoutes: Routes = [
  {
    path: '',
    component: MainComponent,
  //  canActivate: [AuthGuard],
    children: [
      {
        path: '',
//        canActivateChild: [AuthGuard],
        children: [
          { path: '', component: MainDetailComponent, data: {animation: 'Home'}},
          { path: 'main-detail', component: MainDetailComponent, data: {animation: 'main-detail'}},
          { path: 'users', component: UsersComponent, data: {animation: 'users'}},
          { path: 'plant', component: PlantComponent, data: {animation: 'plant'}},
          { path: 'roles', component: RolesComponent, data: {animation: 'roles'}},
          { path: 'task', component: TaskComponent, data: {animation: 'task'}},
          { path: 'workflow', component: WorkflowComponent, data: {animation: 'workflow'}},
          { path: 'processflow', component: ProcessflowComponent, data: {animation: 'processflow'}},
          // { path: 'ProjectWaves', component: ProjectWavesComponent, data: {animation: 'ProjectWaves'}},
          { path: 'DataFolder', component: DataFolderComponent, data: {animation: 'DataFolderComponent'}},
          { path: 'leader-board', component: LeaderBoardComponent, data: {animation: 'LeaderBoardComponent'}},
          { path: 'task-assignment', component: TaskAssignmentComponent, data: {animation: 'task-assignment'}},
          { path: 'search', component: SearchComponent, data: {animation: 'search'}},
          { path: 'contact', component: ContactComponent, data: {animation: 'contact'}},
          { path: 'system-entry', component: SysComponent, data: {animation: 'system-entry'}},
          { path: 'document-details/:id', component: DocumentDetailsComponent},
          { path: 'new-task', component: NewTaskComponent},
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class MainRoutingModule {}
