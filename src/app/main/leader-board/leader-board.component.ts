import { Component, OnInit } from '@angular/core';
import { ExcelService } from '../../services/excel.service';
import { ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { LeaderboardService } from '../../services/LeaderboardService';
import { Color } from 'angular-bootstrap-md';
import { style } from '@angular/animations';
import { MatPaginatorModule } from '@angular/material/paginator';

export interface UserData {
  ranking: string;
  plantName: string;
  plantCode: string;
  status: string;
  currentPhase: string;
  madiScore: string;
}

@Component({
  selector: 'app-leader-board',
  templateUrl: './leader-board.component.html',
  styleUrls: ['./leader-board.component.scss']
})
export class LeaderBoardComponent implements OnInit {

  displayedColumns = ['ranking', 'plantName', 'status', 'currentPhase', 'madiScore'];
  dataSource: MatTableDataSource<UserData>;
  content: any = Object;
  TaskElements: any[];
  isLoading = false;
  progressVal = 10; // parseFloat(this.data.status);
  plantName = '';

  color = '#03A9F4';
  mode = 'determinate';
  value = 50;
  bufferValue = 75;



  @ViewChild(MatTable) table: MatTable<UserData>;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

 COMPETENCY_DATA: UserData[] = [];
  //  { ranking: '1', plantName: 'sttat', plantCode: 'SA', Status: '51%', currentPhase: 'Implementation', madiScore: 'yellow'},
  //  { ranking: '2', plantName: 'sAZXDfttatZ', plantCode: 'CA', Status: '21%', currentPhase: 'Follow Up', madiScore: 'yellow'}];

 taskExport: any[];

  constructor(
    private excelService: ExcelService,
    private leaderboardService: LeaderboardService
    ) { }

  //    ngAfterViewInit(): void {
  //      this.content.dataSource.paginator = this.paginator;
  //      this.content.dataSource.sort = this.sort;
  //  }

  ngOnInit(): void {
   this.getLeaderboard();
   this.getPlantName();
  }

  getPlantName() {
    this.plantName = localStorage.getItem('plantName');
    // console.log(JSON.stringify(this.plantName));
  }

  getLeaderboard() {
    // clean array
    // this.COMPETENCY_DATA = [];
    this.leaderboardService.getLeaderBoard().subscribe((data) => {
      // console.log(JSON.stringify(data));
      // setTimeout(() => this.content.dataSource.paginator = this.paginator; //), 1000;
      for (data of data) {
        const ranking = data.ranking;
        const plantName = data.plantName;
        const plantCode = data.plantCode;
        const status = data.status;
        const currentPhase = data.currentPhase;
        const madiScore = data.madiScore;
        // tslint:disable-next-line:no-shadowed-variable
        const UserData = {
          ranking,
          plantName,
          plantCode,
          status,
          currentPhase,
          madiScore,
        };
        this.COMPETENCY_DATA.push(UserData);
      }
      this.content.dataSource = new MatTableDataSource<UserData>(
        this.COMPETENCY_DATA
      );
      this.content.dataSource.paginator = this.paginator;
      this.content.dataSource.sort = this.sort;
    });
    // this.table.renderRows();
    // this.content.dataSource.paginator = this.paginator;
  }

  getColor(madiScore: number) {
    let color = '';
    if (madiScore === null){
       // color = '#F44336';
       color = 'grey';
      }
      else if (madiScore > 0.00 && madiScore < 1.00)
      {
        color = '#F44336'; // red
      }
      else if (madiScore >= 1.00 && madiScore < 2.00)
      {
        color = '#FFEB3B'; // yellow
      }
      else if (madiScore >= 2.00 && madiScore < 3.00)
      {
        color = '#4CAF50'; // green
      }
      else if (madiScore >= 3.00)
      {
        color = '#03A9F4'; // blue
      }

    return color;
  }

  // uncomment in case to use specific colors as rainbow bar in mat-progress-bars and change in [appProgressBarcolor func in html file the call to this func updateColorspecified->updateColorasRainbow]
  // updateColorasRainbow(perc) {
  //   let r = 0;
  //   let g = 0;
  //   const b = 0;

  //   if (perc < 50) {
  //     r = 255;
  //     g = Math.round(5.1 * perc);
  //   }
  //   else {
  //     g = 255;
  //     r = Math.round(510 - 5.10 * perc);
  //   }
  //   const h = r * 0x10000 + g * 0x100 + b * 0x1;
  //   return '#' + ('000000' + h.toString(16)).slice(-6);
  // }

  updateColorspecified(status){
    if (status === 0){
      // color = '#F44336';
      return 'grey';
     }
     else if (status > 0 && status <= 25)
     {
      return '#F44336';
     }
     else if (status > 25 && status <= 50)
     {
      return '#FFEB3B';
     }
     else if (status > 50 && status <= 75)
     {
      return '#4CAF50';
     }
     else if (status > 75)
     {
      return '#03A9F4';
     }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.isLoading = true;
    this.content.dataSource.filter = filterValue.trim().toLowerCase();
    this.isLoading = false;
  }

  exportAsXLSX2() {
    this.taskExport = JSON.parse(JSON.stringify(this.COMPETENCY_DATA));
    this.excelService.exportAsExcelFile(this.taskExport, 'Competency detail');
    // console.log("tarea: " + JSON.stringify(this.task2));
  }
}
