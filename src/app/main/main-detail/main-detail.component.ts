import { GoogleDriveComponent } from './../google-drive/google-drive.component';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from "@angular/core";
import { SocialAuthService } from "angularx-social-login";
import { WorkflowService } from "./../../services/workflow.service";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { ThemePalette } from "@angular/material/core";
import { ProgressBarMode } from "@angular/material/progress-bar";
import { Socialusers } from "./../../model/socialusers";
import { Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";
import { MatPaginator } from "@angular/material/paginator";
import { MatTable, MatTableDataSource } from "@angular/material/table";
import { CircleProgressComponent } from "ng-circle-progress";
import { PlantIdservice } from "../../services/PlantIdservice";
import { CompetencyService } from "../../services/CompetencyService";
import { ConfirmProcesDialogComponent } from "./confirm-proces-dialog/confirm-proces-dialog.component";
import { MatDialog, MatDialogConfig, throwMatDialogContentAlreadyAttachedError } from "@angular/material/dialog";
import { formatDate } from "@angular/common";
import { SafeResourceUrl } from "@angular/platform-browser";
import { DomSanitizer } from "@angular/platform-browser";
import { MatSort } from "@angular/material/sort";
import { ExcelService } from "src/app/services/excel.service";
import { TasksService } from "src/app/services/tasks.service";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { ChartOptions, ChartType, ChartDataSets } from "chart.js";
import {
  MultiDataSet,
  Label,
  SingleDataSet,
  BaseChartDirective,
  ChartsModule,
  Color,
} from 'ng2-charts';
import { GoogleAnalyticsService } from 'src/app/services/google-analytics.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CustomSnackBarComponent } from './../custom-snack-bar/custom-snack-bar.component';
import { CookieService } from 'ngx-cookie-service';
import {
  ClickEvent,
  HoverRatingChangeEvent,
  RatingChangeEvent,
  StarRatingComponent,
} from 'angular-star-rating';
import { Processes } from './../../model/Workflow';
import { PlantId } from './../../model/PlantId';
import { Profile } from './../../model/profile';
import { delay } from 'rxjs/internal/operators/delay';
import { WebUrlPreviewService } from 'src/app/services/WebUrlPreviewService';
import { HttpResponse } from '@angular/common/http';
import { SecurityContext } from '@angular/core';
import { Course } from 'src/app/model/Course';
import { IfStmt } from '@angular/compiler';
import * as Chart from 'chart.js';
import { constants } from 'buffer';
import { BLACK_ON_WHITE_CSS_CLASS } from '@angular/cdk/a11y/high-contrast-mode/high-contrast-mode-detector';
// import { GoogleSheetsDbService } from 'ng-google-sheets-db';
import { Observable } from 'rxjs';
import { Character, characterAttributesMapping } from 'src/app/model/charater-model';
import { environment } from 'src/environments/environment';

export interface TaskElements {
  date: string;
  wave: number;
  status: string;
  category: string;
  name: string;
}

export interface TaskElements2 {
  courseName: string;
  enrolled: number;
  inProgress: number;
  completed: number;
  impactVote: number;
  averageRating: number;
}

@Component({
  selector: "app-main-detail",
  templateUrl: "./main-detail.component.html",
  styleUrls: ["./main-detail.component.scss"],
})
export class MainDetailComponent implements OnInit, AfterViewInit {

  // ELEMENT_DATA: TaskElements[];
  /*
    ELEMENT_DATA: TaskElements[] = [
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "H" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "H" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "H" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "e" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "w" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "rt" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "6" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "2" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "3" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "3" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "3" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "3" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "3" },
      { date: "12/12/2020", name: "Hydrogen", status: "todo", category: "4" },
    ];
  */

  /* COMPETENCY_DATA: TaskElements2[] = [
    { courseName: 'Team Effectiveness', enrolled: 5, inProgress: 3, completed: 8, impactVote: 2.00, averageRating: 20},
    { courseName: 'Basic', enrolled: 3, inProgress: 2, completed: 1, impactVote: 1.00, averageRating: 20},
    { courseName: 'Discipline in execution', enrolled: 2, inProgress: 0, completed: 3, impactVote: 2.00, averageRating: 20},
    { courseName: 'Team Effectiveness2', enrolled: 8, inProgress: 3, completed: 8, impactVote: 2.00, averageRating: 20},
    { courseName: 'Team Effectiveness3', enrolled: 4, inProgress: 3, completed: 8, impactVote: 1.00, averageRating: 20},
    { courseName: 'Team Effectiveness4', enrolled: 3, inProgress: 2, completed: 2, impactVote: 3.00, averageRating: 20}
  ];
 */
  onerror = '';
  // tslint:disable-next-line:variable-name
  error_cors: string;
  repsonseData: string;
  // tslint:disable-next-line:variable-name
  urlTX268_2: string;
  log: string;
  // tslint:disable-next-line:ban-types
  PlantId: Object;
  plantID = 0;
  totalPercentage: any;
  socialusers = new Socialusers();
  Total: string;
  profileForm: FormGroup;
  color: ThemePalette = "accent";
  mode: ProgressBarMode = "determinate";
  bufferValue = 100;
  Router: any;
  transparent = "transparent";
  events: string[] = [];
  opened: boolean;
  plantControl = new FormControl("", Validators.required);
  selectFormControl = new FormControl("", Validators.required);
  profile: Profile;
  Plantid: PlantId;
  code: string;
  data: any;
  status = false;
  pageSize = 10;
  loading: any;
  Processes: Processes;
  totalPrecentPreparation = 0;
  totalPrecentAssessment = 0;
  totalPrecentImplementation = 0;
  totalPrecentFollowUp = 0;
  totalPrecentSustainability = 0;
  tasklistContent: any; // number[]=[0,0,0,0,0];
  percentPrep: any;
  percentAssesment: any;
  percentImplementation: any;
  percentFollowUp: any;
  percentSustain: any;
  content: any = Object;
  content2: any = Object;
  taskData1: any = [];
  taskData2: any = [];
  taskData3: any = [];
  taskData4: any = [];
  taskData5: any = [];
  breakpoint: number;
  Arraypercents: number[];
  // tslint:disable-next-line:ban-types
  isMobile: Boolean = false;
  processStart: true;
  // isCloseWavebuttonEnable: Boolean= false;
  // tslint:disable-next-line:ban-types
  isDisabledStarts: Boolean = false;
    // tslint:disable-next-line:ban-types
  isVotesHide: Boolean = false;
  averagedownload = 0;
  mainAverage = 0;
  waves: number[]; // = [1,2,3];
  nwavemax: number;
  nwaves: number;
  nwaveSelected: number;
  onRatingChangeResult: RatingChangeEvent;
  // nwaveSelectedHistory: number;
  displayedColumns: string[] = [
    "date",
    "wave",
    "status",
    "category",
    "name",
    "actions",
  ];
  displayedColumns2: string[] = [
    "courseName",
    "enrolled",
    "inProgress",
    "completed",
    "impactVote",
    "averageRating",
    "actions",
  ];
  ELEMENT_DATA: TaskElements[];
  COMPETENCY_DATA: TaskElements2[];
  userTaskDetails: any;

  // COMPETENCY_DATA: TaskElements2[];
  // dataSource = new MatTableDataSource<TaskElements>(this.ELEMENT_DATA);
  // dataSource2 = new MatTableDataSource<TaskElements>(this.COMPETENCY_DATA);
  // dataSource = new MatTableDataSource();
  // dataSource1 = new MatTableDataSource();
  todayDate: Date = new Date();
  createDate: string = this.todayDate.toISOString();
  task2: any[];
  task3: any[];
  // title = "Example Angular 10 Material Dialog";
  // today: any=new Date();
  // dayClosePopUp: Date;
  // imageSource: String = "../../../../assets/images/EXCEL-ICON.png";
  // myWeek: Date = $filter('date')(todayDate, 'ww');

  urlTX268: SafeResourceUrl;
  urlTX266: SafeResourceUrl;
  urlCH79: SafeResourceUrl;
  urlCH92: SafeResourceUrl;
  urlCH83: SafeResourceUrl;
  urlCH128: SafeResourceUrl;
  urlDashboardlink: string;
  urlDashboardlink2: any;
  // urlDashboardlink: SafeResourceUrl;
  @ViewChild(MatTable) table: MatTable<TaskElements2>;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatPaginator, { static: false }) paginator2: MatPaginator;
  @ViewChild('sorter1', { static: false }) sorter1: MatSort;
  @ViewChild('sorter2', { static: false }) sorter2: MatSort;
  // @ViewChild(MatSort, { static: false }) table1Sort: MatSort;
  // @ViewChild(MatSort, { static: false }) table2Sort: MatSort;

  @ViewChild('circleProgress') circleProgress: CircleProgressComponent;
  /* @ViewChild('iframe', { static: false }) iframe: ElementRef; */
  // @ViewChild(BaseChartDirective) public chart: BaseChartDirective;
  // @ViewChild(BaseChartDirective) public doughnutChartPreparation: BaseChartDirective;
  // @ViewChild(BaseChartDirective) public doughnutChartAssessment: BaseChartDirective;
  // dashboardUrl: string = "https://qv.lhinside.com/QvAJAXZfc/singleobject.htm?document=userdocs%20itind%5Ccif%20dashboard.qvw&object=TX266"
  // src: string;

  public doughnutChartLabels: Label[] = [
    'Completed',
    'ToDo',
    'Due soon',
    'Over Due',
    'Not started',
  ];
  public doughnutChartDataPreparation: number[] = [0, 0, 0, 0, 100];
  public doughnutChartDataAssessment: number[] = [0, 0, 0, 0, 100];
  public doughnutChartDataImplementation: number[] = [0, 0, 0, 0, 100];
  public doughnutChartDataFollowup: number[] = [0, 0, 0, 0, 100];
  public doughnutChartDataSustainability: number[] = [0, 0, 0, 0, 100];
  public doughnutChartType: ChartType = 'doughnut';
  public donutColors = [
    {
      backgroundColor: ['#29c17b', '#1f8efa', '#f08c00', '#e6280f', '#dae0f7'],
    },
  ];
  // colors:                                green,     blue,      orange,      red,     grey
  // types:                             completed,     todo,    due soon, over due,  notSorted

  public barChartOptions: ChartOptions = {
    tooltips: { enabled: false }, // remove to see tooltips on hover
    hover: { mode: null }, // remove to see tooltips on hover
    cutoutPercentage: 75,
    // responsive: true,
    // maintainAspectRatio: true,
    // aspectRatio: 2,
    // responsiveAnimationDuration: 1,
    legend: {
      display: false, // enable to true the view of labels in donut sector chart.js
      align: 'center',
      position: 'top',
      fullWidth: true,
      labels: {
        boxWidth: 100,
        padding: 16,
        fontSize: 10,
        usePointStyle: true,
      },
    },
  };
  options: {
    circumference: number;
    rotation: number;
    animation: { onComplete: () => void };
  };
  course: typeof Course;
  // doughnutChartData: any;

// line chart on dashboard

lineChartData: ChartDataSets[] = [
  { data: [85, 72, 78, 65, 67, 75], label: '% iCIF progress',  backgroundColor: ['#B3C1CE'], pointBackgroundColor: '#000000', pointBorderColor: '#000000',  pointBorderWidth: 3, pointRadius: 2},
  { data: [90, 56, 80, 70, 80, 90], label: 'Middlestone',  backgroundColor: ['#E7E7E7'], pointBackgroundColor: '#000000', pointBorderColor: '#000000',  pointBorderWidth: 3, pointRadius: 2}
];

// Un comment in case apply any legend to the axis.
lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June'];

lineChartOptions = {
  responsive: true,
  showTooltips: true,
  tension: 0,
  fill: true,
  // fillColor: '#000000',
  hover: {
    animationDuration: 0
  },
    animation: {
      duration: 1,
        onComplete() {
          // tslint:disable-next-line:one-variable-per-declaration
          const chartInstance = this.chart,
              ctx = chartInstance.ctx;

          ctx.textAlign = 'center';
          ctx.textBaseline = 'bottom';

          // tslint:disable-next-line:only-arrow-functions
          this.data.datasets.forEach(function(dataset, i)
                                     {
            const meta = chartInstance.controller.getDatasetMeta(i);
            // tslint:disable-next-line:only-arrow-functions
            meta.data.forEach(function(bar, index) {
              const data = dataset.data[index];
              ctx.fillText(data, bar._model.x + 15 , bar._model.y + 5);
            });
          });
        }
    },
    legend: {
      display: false,
      position: 'left'
    },
    scales: { xAxes: [{}], yAxes: [{
      ticks: {
        beginAtZero: false,
        stepSize: 10
      },
      scaleLabel: {
      display: true,
      labelString: '98% Planned',
      fontSize: 18
    }}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end'
      }
    }
};

lineChartColors: Color[] = [
  {
    borderColor: '#B3C1CE',
    backgroundColor: '#B3C1CE'
  },
];

lineChartLegend = true;
lineChartPlugins = [];
lineChartType = 'line';

// Bar chart on Dashboard:
  public barChartOptions2: ChartOptions = {
    responsive: true,
    hover: {
      animationDuration: 0
    },
      animation: {
        duration: 1,
          onComplete() {
            // tslint:disable-next-line:one-variable-per-declaration
            const chartInstance = this.chart,
                ctx = chartInstance.ctx;

            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';

            // tslint:disable-next-line:only-arrow-functions
            this.data.datasets.forEach(function(dataset, i)
                                       {
              const meta = chartInstance.controller.getDatasetMeta(i);
              // tslint:disable-next-line:only-arrow-functions
              meta.data.forEach(function(bar, index) {
                const data = dataset.data[index];
                ctx.fillText(data, bar._model.x + 15 , bar._model.y + 5);
              });
            });
          }
      },
    legend: {
      display: false,
      position: 'left'
    },
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{
        ticks: {
            display: false}
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true,
          stepSize: 1,
        },
        scaleLabel: {
        display: true,
        labelString: 'Worksstreams',
        fontSize: 18
      }}]
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end'
      }
    }
  };

  // colors bar char dashboard:
  // green = '#2CA02C';
  // orange = '#FF7F0E';
  // red = '#D62728';v
  green = '#8BE77C';
  orange = '#F5A574';
  red = '#FF5A5A';
  goal: any[] = [75];
  borderColor: string[] = [];


  // Googledata
  GoogleData = 'GoogleData';
  characters$: Observable<Character[]>;

  public fillbackgroundColor = [];
  public barChartLabels2: Label[] = [];
  public barChartType2: ChartType = 'horizontalBar';
  public barChartLegend2 = true;
  public barChartPlugins2 = [];
  public barChartData2: ChartDataSets[];
  public barChartColors2: Color[] = [{backgroundColor: []}]; // [{ backgroundColor: ['#5cb8ff', '#ffc107', '#d9534f', '#5cb8ff', '#ffc107', '#d9534f', '#5cb8ff', '#ffc107', '#d9534f', '#5cb8ff', '#ffc107', '#d9534f', '#5cb8ff', '#ffc107', '#d9534f', '#5cb8ff', '#ffc107', '#d9534f']} ]; // ['#5cb8ff', '#ffc107', '#d9534f']   'rgb(31, 78, 120)'

  // tslint:disable-next-line:ban-types
  isCorporate: Boolean = false;

  constructor(
    private PlantIdserv: PlantIdservice,
    private NArrayWavesserv: PlantIdservice,
    private NWavesserv: PlantIdservice,
    public userService: UserService,
    public authService: SocialAuthService,
    // tslint:disable-next-line:no-shadowed-variable
    public WorkflowService: WorkflowService,
    public taskService: TasksService,
    // tslint:disable-next-line:no-shadowed-variable
    public CompetencyService: CompetencyService,
    private router: Router,
    // tslint:disable-next-line:no-shadowed-variable
    private PlantIdservice: PlantIdservice,
    public breakpointObserver: BreakpointObserver,
    public OAuth: SocialAuthService,
    private dialog: MatDialog,
    // private matDialog: MatDialog,
    private excelService: ExcelService,
    public sanitizer: DomSanitizer,
    private snackBar: MatSnackBar,
    private cookieService: CookieService,
    private webUrlPreviewService: WebUrlPreviewService,
    private googleAnalyticsService: GoogleAnalyticsService,
    private changeDetectorRefs: ChangeDetectorRef,
    // private googleSheetsDbService: GoogleSheetsDbService
    ) {
    breakpointObserver
      .observe(
        // [Breakpoints.Handset]
        ['(max-width: 599px)']
      )
      .subscribe((result) => {
        this.isMobile = result.matches;
      });
    // this.today=Date.now();
    // this.dayClosePopUp= new Date(2021, 1, 12);
  }


  ngAfterViewInit(): void {
    setTimeout(() => {
      this.content.dataSource.sort = this.sorter1;
    }, 2000);
    setTimeout(() => {
      this.content2.dataSource2.sort = this.sorter2;
    }, 2000);
  }

  ngOnInit(): void {

    // this.characters$ = this.googleSheetsDbService.getActive<Character>(
    //   environment.characters.spreadsheetId, environment.characters.worksheetName, characterAttributesMapping, 'Active');

    // this.breakpoint = (window.innerWidth <= 500) ? 1 : 12;
    // this.openDialog();
    // console.log(new Date(this.today));
    // console.log(this.dayClosePopUp);
    // console.log(window.innerWidth); //to review size width of screen
    /*
    this.PlantIdserv.getAllClosedWave()
    .subscribe(data => {
      this.nwaves=data;
      //this.nwaves=(Number(this.nwaves)+2);
      this.rangewaves(Number(this.nwaves)+2);
      localStorage.setItem('Nowaves', data);
      //localStorage.setItem('nwaveSelected', data);
      //if (localStorage.getItem("nwaveSelected") === null) {
      //localStorage.setItem('nwaveSelected', JSON.stringify(Number(this.nwaves)+2));
      //}else{console.log (this.nwaveSelected)};
      localStorage.setItem('nwaveSelected', JSON.stringify(Number(this.nwaves)+2));
      localStorage.setItem('viewClosedWave', '1');
    });
    this.nwaveSelected = this.waves.reduce((a, b)=>Math.max(a, b));
    //this.nwaveSelected=Number(localStorage.getItem('nwaveSelected'));
    console.log(this.nwaveSelected);
    */
    this.loading = true;
    const plantName = localStorage.getItem("plantName");
    const userName = localStorage.getItem("userName");
    const email = localStorage.getItem("email");
    const code = localStorage.getItem("code");
    // this.waves=JSON.parse(localStorage.getItem("waves"));
    // this.nwaveSelected = this.waves.reduce((a, b)=>Math.max(a, b));
    this.waves = JSON.parse(localStorage.getItem("waves"));
    this.nwavemax = JSON.parse(localStorage.getItem("nwavemax"));
    localStorage.setItem("nwaveSelected", JSON.stringify(this.nwavemax));
    this.nwaveSelected = Number(localStorage.getItem("nwaveSelected"));
    if (!localStorage.getItem("loginAnalytics")) {
      this.googleAnalyticsService.eventEmitter(
        "iCIF Login",
        "iCIF Login",
        "iCIF Login",
        plantName,
        email,
        plantName,
        code,
        email
      );
      // this.googleAnalyticsService.eventEmitter("Plant "+code,"Plant "+code,"iCIF Plant",plantName,userName);
      this.googleAnalyticsService.eventEmitter(
        "Plant " + plantName,
        "Plant " + plantName,
        "Plant " + plantName,
        code,
        email,
        plantName,
        code,
        email
      );
      localStorage.setItem("loginAnalytics", "sent");
      this.router.navigate(["/main"], { queryParams: { plantcode: code } });
    }
    this.PlantIdserv.newcontent.subscribe(
      (content) => (this.tasklistContent = content)
    );
    // this.taskService.retrieveuserTaskData(1).subscribe(data =>  {this.taskData1 = data;
    //   //console.log("taskData1 :"+ JSON.stringify(this.taskData1));
    //   this.getTotalNumberOfTask1(this.taskData1);
    //   this.getNumberOfTaskPerProcess1(this.taskData1);
    //   this.setPercentage()
    //   //console.log("totalPrecentPreparation: "+JSON.stringify(this.totalPrecentPreparation));
    //   });
    // this.taskService.retrieveuserTaskData(2).subscribe(data =>  {this.taskData2 = data;
    //   //console.log("taskData2 :"+ JSON.stringify(this.taskData2));
    //   this.getTotalNumberOfTask2(this.taskData2);
    //   this.getNumberOfTaskPerProcess2(this.taskData2);
    //   this.setPercentage()
    //   //console.log("totalPrecentAssessment: "+JSON.stringify(this.totalPrecentAssessment));
    //   //console.log(this.totalPrecentAssessment);
    //   });
    //   this.taskService.retrieveuserTaskData(3).subscribe(data =>  {this.taskData3 = data;
    //   //console.log("taskData3 :"+ JSON.stringify(this.taskData3));
    //   this.getTotalNumberOfTask3(this.taskData3);
    //   this.getNumberOfTaskPerProcess3(this.taskData3);
    //   this.setPercentage()
    //   //console.log("totalPrecentImplementation: "+JSON.stringify(this.totalPrecentImplementation));
    //   //console.log(this.totalPrecentImplementation);
    //   });
    // this.taskService.retrieveuserTaskData(4).subscribe(data =>  {this.taskData4 = data;
    //   //console.log("taskData4 :"+ JSON.stringify(this.taskData4));
    //   this.getTotalNumberOfTask4(this.taskData4);
    //   this.getNumberOfTaskPerProcess4(this.taskData4);
    //   this.setPercentage()
    //   //console.log("totalPrecentFollowUp: "+JSON.stringify(this.totalPrecentFollowUp));
    //   });
    // this.taskService.retrieveuserTaskData(5).subscribe(data =>  {this.taskData5 = data;
    //   //console.log("taskData5 :"+ JSON.stringify(this.taskData5));
    //   this.getTotalNumberOfTask5(this.taskData5);
    //   this.getNumberOfTaskPerProcess5(this.taskData5);
    //   this.setPercentage()
    //   //console.log("totalPrecentSustainability: "+JSON.stringify(this.totalPrecentSustainability));
    //   });

    // if(!localStorage.getItem("allsearchResults")){
    //   this.loading= true;
    //   this.userService.getAllSearchResults().subscribe(data => {
    //     this.loading = false;
    //     localStorage.setItem("allsearchResults", data);
    //  },
    //  error => {
    //   console.log('error search result in fatching the data');
    //   this.loading = false;
    // }
    //  );
    // }

    this.PlantIdserv.isLoading.subscribe((loading) => (this.loading = loading));
    this.content = {};
    this.content2 = {};
    this.PlantIdserv.preparationcontent.subscribe(
      (percentPrep) => (this.percentPrep = percentPrep)
    );
    this.PlantIdserv.assessmentcontent.subscribe(
      (percentAssesment) => (this.percentAssesment = percentAssesment)
    );
    this.PlantIdserv.implementationcontent.subscribe(
      (percentImplementation) =>
        (this.percentImplementation = percentImplementation)
    );
    this.PlantIdserv.followupcontent.subscribe(
      (percentFollowUp) => (this.percentFollowUp = percentFollowUp)
    );
    this.PlantIdserv.sustaincontent.subscribe(
      (percentSustain) => (this.percentSustain = percentSustain)
    );
    this.PlantIdserv.totalPercentContent.subscribe(
      (totalPercentage) =>
        (this.totalPercentage = Math.round(Number(totalPercentage)))
    );
    // tslint:disable-next-line:no-shadowed-variable
    this.PlantIdserv.currentPlantid.subscribe((PlantId) => (this.PlantId = PlantId)
    );
    this.PlantIdserv.startButtonContent.subscribe(
      (content) => (this.content = content)
    );
    this.PlantIdserv.isLoading.subscribe((loading) => (this.loading = loading));
    this.NArrayWavesserv.ArrayWaves.subscribe((waves) => (this.waves = waves));
    this.NWavesserv.waveselected.subscribe(
      (waves) => (this.nwaveSelected = waves)
    );
    this.socialusers = JSON.parse(localStorage.getItem('socialusers'));
    this.PlantIdserv.selectedPlantPercentages().subscribe(
      (data) => {
        for (const plantData of data) {
          localStorage.setItem(plantData.plant_Name, JSON.stringify(plantData));
        }

        if (data[0] && data[0].userTaskDetails) {
          this.userTaskDetails = data[0].userTaskDetails;
          for (const taskDetails of data[0].userTaskDetails) {
            const processID = taskDetails.processID;
            if (processID) {
              if (processID === '1') {
                this.getTotalNumberOfTask1(taskDetails);
                this.getNumberOfTaskPerProcess1(taskDetails);
                this.taskData1.onlyFloatingTasksPending =
                  taskDetails.onlyFloatingTasksPending;
              }
              if (processID === '2') {
                this.getTotalNumberOfTask2(taskDetails);
                this.getNumberOfTaskPerProcess2(taskDetails);
                this.taskData2.onlyFloatingTasksPending =
                  taskDetails.onlyFloatingTasksPending;
              }

              if (processID === '3') {
                this.getTotalNumberOfTask3(taskDetails);
                this.getNumberOfTaskPerProcess3(taskDetails);
                this.taskData3.onlyFloatingTasksPending =
                  taskDetails.onlyFloatingTasksPending;
              }

              if (processID === '4') {
                this.getTotalNumberOfTask4(taskDetails);
                this.getNumberOfTaskPerProcess4(taskDetails);
                this.taskData4.onlyFloatingTasksPending =
                  taskDetails.onlyFloatingTasksPending;
              }

              if (processID === '5') {
                this.getTotalNumberOfTask5(taskDetails);
                this.getNumberOfTaskPerProcess5(taskDetails);
                this.taskData5.onlyFloatingTasksPending =
                  taskDetails.onlyFloatingTasksPending;
              }
            }
          }
        }
        this.setPercentage();
        this.PlantId = localStorage.getItem('plantName');
        this.plantID = Number(localStorage.getItem('plantID'));
        this.setPercentage();
        this.getTaskPanelOverviewData();
        this.getTaskPanelCompetencyData();

        // if (!this.cookieService.get('snackbarset')){
        // this.snackBar.openFromComponent(CustomSnackBarComponent,
        //   {verticalPosition: 'top',
        //    panelClass : ['red-snackbar']}, );
        // }
        const htmlMessage = '';
      }
    );
    if (!this.cookieService.get('snackbarset')){
      this.snackBar.openFromComponent(CustomSnackBarComponent,
        {verticalPosition: 'top',
         panelClass : ['red-snackbar']}, );
      }
    this.getWeekYear();
    this.getWeek();

    // https://qsqadigital.lhinside.com/sso/single/?appid=d14480f1-9429-4345-a70c-35fe0fede0d9&obj=xWukr&opt=nointeraction&select=YearWeek.2021-06&select=Plant%20Code,CS
    // https://qsqadigital.lhinside.com/sso/single/?appid=d14480f1-9429-4345-a70c-35fe0fede0d9&obj=yDnDD&opt=nointeraction&select=YearWeek.2021-06&select=Plant%20Code,CG
    // https://qsqadigital.lhinside.com/sso/single/?appid=d14480f1-9429-4345-a70c-35fe0fede0d9&obj=pfwfBgn&opt=nointeraction&select=YearWeek.2021-06&select=Plant%20Code,CG
    // https://qsqadigital.lhinside.com/sso/single/?appid=d14480f1-9429-4345-a70c-35fe0fede0d9&obj=mbsNnMw&opt=nointeraction&select=YearWeek.2021-06&select=Plant%20Code,CG

    // https://qsqadigital.lhinside.com/sso/single/?appid=d14480f1-9429-4345-a70c-35fe0fede0d9&obj=xWukr&opt=nointeraction&select=YearWeek.2021-23&select=Plant%20Code,CR
    // <iframe src='https://qsqadigital.lhinside.com/sso/single/?appid=d14480f1-9429-4345-a70c-35fe0fede0d9&obj=yDnDD&opt=nointeraction&select=clearall' style='border:none;width:100%;height:100%;'></iframe>

    /* <iframe src="https://qsqadigital.lhinside.com/iFrame/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=xWukr&opt=ctxmenu&select=YearWeek,2021-06&select=Plant%20Code,CG" style="border:none;width:100%;height:100%;"></iframe>
    <iframe src="https://qsqadigital.lhinside.com/iFrame/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=yDnDD&opt=ctxmenu &select=YearWeek,2021-06&select=Plant%20Code,CG " style="border:none;width:100%;height:100%;"></iframe>
    <iframe src="https://qsqadigital.lhinside.com/iFrame/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=znVpZ&opt=ctxmenu&select=YearWeek,2021-06&select=Plant%20Code,CG" style="border:none;width:100%;height:100%;"></iframe>
    <iframe src="https://qsqadigital.lhinside.com/iFrame/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=mbsNnMw&opt=ctxmenu&select=YearWeek,2021-06&select=Plant%20Code,CG" style="border:none;width:100%;height:100%;"></iframe> */
    // https://qsqadigital.lhinside.com/iFrame/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=yDnDD&opt=ctxmenu&select=YearWeek
    // <iframe src="https://qsqadigital.lhinside.com/iFrame/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=znVpZ&opt=ctxmenu&select=YearWeek,2021-06&select=Plant%20Code,CG" style="border:none;width:100%;height:100%;"></iframe>

    /* //new dashboard 1
    //% in orange numbers
    // tslint:disable-next-line:max-line-length
    this.urlTX268 = this.sanitizer.bypassSecurityTrustResourceUrl('https://qsqadigital.lhinside.com/sso/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=znVpZ&opt=ctxmenu&select=YearWeek.'+ this.getWeekYear() + "-" + (this.getWeek()-1) +'&select=Plant%20Code,'+ localStorage.getItem('code'));//+ '"'+ "&output=embed");
    // tslint:disable-next-line:max-line-length
    //https://qsqadigital.lhinside.com/iFrame/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=znVpZ&opt=nointeraction&select=YearWeek.2021-06&select=Plant%20Code,CG
    //dots chats grey blue
    // tslint:disable-next-line:max-line-length
    this.urlCH79 = this.sanitizer.bypassSecurityTrustResourceUrl('https://qsqadigital.lhinside.com/sso/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=xWukr&opt=ctxmenu&select=YearWeek.'+ this.getWeekYear() + "-" + (this.getWeek()-1) +'&select=Plant%20Code,'+ localStorage.getItem('code'));//+ '"'+ "&output=embed");
    //https://qsqadigital.lhinside.com/iFrame/single/?appid=d14480f1-9429-4345-a70c-35fe0fede0d9&obj=xWukr&opt=nointeraction&select=YearWeek,2021-06&select=Plant%20Code,CG
    //% grey total percent
    // tslint:disable-next-line:max-line-length
    this.urlCH92 = this.sanitizer.bypassSecurityTrustResourceUrl('https://qsqadigital.lhinside.com/sso/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=mbsNnMw&opt=ctxmenu&select=YearWeek.'+ this.getWeekYear() + "-" + (this.getWeek()-1) +'&select=Plant%20Code,'+ localStorage.getItem('code'));//+ '"'+ "&output=embed");
    // tslint:disable-next-line:max-line-length
    //https://qsqadigital.lhinside.com/iFrame/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=mbsNnMw&opt=ctxmenu&select=YearWeek,2021-06&select=Plant%20Code,CG
    //chart bars colors
    // tslint:disable-next-line:max-line-length
    this.urlCH83 = this.sanitizer.bypassSecurityTrustResourceUrl('https://qsqadigital.lhinside.com/sso/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=yDnDD&opt=ctxmenu&select=YearWeek.'+ this.getWeekYear() + "-" + (this.getWeek()-1) +'&select=Plant%20Code,'+ localStorage.getItem('code'));//+ '"'+ "&output=embed");
    // tslint:disable-next-line:max-line-length
    //https://qsqadigital.lhinside.com/iFrame/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=yDnDD&opt=nointeraction&select=YearWeek.2021-06&select=Plant%20Code,CG
 */

    // //new dashboard 2 the actual
    // //% in orange numbers
    // tslint:disable-next-line:max-line-length
    // this.urlTX268 = this.sanitizer.bypassSecurityTrustResourceUrl('https://qsqadigital.lhinside.com/iframe/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=znVpZ&opt=nointeraction&select=YearWeek.'+ this.getWeekYear() + "-" + (this.getWeek()-1) +'&select=Plant%20Code,'+ localStorage.getItem('code'));//+ '"'+ "&output=embed");
    // tslint:disable-next-line:max-line-length
    // //https://qsqadigital.lhinside.com/iFrame/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=znVpZ&opt=nointeraction&select=YearWeek.2021-06&select=Plant%20Code,CG
    // //dots chats grey blue
    // tslint:disable-next-line:max-line-length
    // this.urlCH79 = this.sanitizer.bypassSecurityTrustResourceUrl('https://qsqadigital.lhinside.com/iframe/single/?appid=d14480f1-9429-4345-a70c-35fe0fede0d9&obj=xWukr&opt=nointeraction&select=YearWeek.'+ this.getWeekYear() + "-" + (this.getWeek()-1) +'&select=Plant%20Code,'+ localStorage.getItem('code'));//+ '"'+ "&output=embed");
    // tslint:disable-next-line:max-line-length
    // //https://qsqadigital.lhinside.com/iFrame/single/?appid=d14480f1-9429-4345-a70c-35fe0fede0d9&obj=xWukr&opt=nointeraction&select=YearWeek,2021-06&select=Plant%20Code,CG
    // //% grey total percent
    // tslint:disable-next-line:max-line-length
    // this.urlCH92 = this.sanitizer.bypassSecurityTrustResourceUrl('https://qsqadigital.lhinside.com/iframe/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=mbsNnMw&opt=ctxmenu&select=YearWeek.'+ this.getWeekYear() + "-" + (this.getWeek()-1) +'&select=Plant%20Code,'+ localStorage.getItem('code'));//+ '"'+ "&output=embed");
    // tslint:disable-next-line:max-line-length
    // //https://qsqadigital.lhinside.com/iFrame/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=mbsNnMw&opt=ctxmenu&select=YearWeek,2021-06&select=Plant%20Code,CG
    // //chart bars colors
    // tslint:disable-next-line:max-line-length
    // this.urlCH83 = this.sanitizer.bypassSecurityTrustResourceUrl('https://qsqadigital.lhinside.com/iframe/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=yDnDD&opt=nointeraction&select=YearWeek.'+ this.getWeekYear() + "-" + (this.getWeek()-1) +'&select=Plant%20Code,'+ localStorage.getItem('code'));//+ '"'+ "&output=embed");
    // tslint:disable-next-line:max-line-length
    // //https://qsqadigital.lhinside.com/iFrame/single/?appid=17710d8e-fcea-4fb7-85f0-c93222be2b8d&obj=yDnDD&opt=nointeraction&select=YearWeek.2021-06&select=Plant%20Code,CG

    // tslint:disable-next-line:max-line-length
    // //'https://qv.lhinside.com/QvAJAXZfc/singleobject.htm?document=userdocs%20itind%5Ccif%20dashboard.qvw&object=TX266&Select=LB33,' + this.getWeekYear() + "-" + (this.getWeek()-1) + '&Select=LB40, "' + localStorage.getItem('code') + '"'+ "&output=embed");
    // //console.log (this.urlTX268);

    this.urlTX268 = this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://qv.lhinside.com/QvAJAXZfc/singleobject.htm?document=userdocs%20itind%5Ccif%20dashboard.qvw&object=TX268&Select=LB33,' +
        this.getWeekYear() +
        '-' +
        (this.getWeek() - 1) +
        '&Select=LB40, "' +
        localStorage.getItem('code') +
        '"'
    );

    this.urlTX266 = this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://qv.lhinside.com/QvAJAXZfc/singleobject.htm?document=userdocs%20itind%5Ccif%20dashboard.qvw&object=TX266&Select=LB33,' +
        this.getWeekYear() +
        '-' +
        (this.getWeek() - 1) +
        '&Select=LB40, "' +
        localStorage.getItem('code') +
        '"' +
        '&output=embed'
    );
    this.urlCH79 = this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://qv.lhinside.com/QvAJAXZfc/singleobject.htm?document=userdocs%20itind%5Ccif%20dashboard.qvw&object=CH79&Select=LB33,' +
        this.getWeekYear() +
        '-' +
        (this.getWeek() - 1) +
        '&Select=LB40, "' +
        localStorage.getItem('code') +
        '"' +
        '&output=embed'
    );
    this.urlCH92 = this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://qv.lhinside.com/QvAJAXZfc/singleobject.htm?document=userdocs%20itind%5Ccif%20dashboard.qvw&object=CH92&Select=LB33,' +
        this.getWeekYear() +
        '-' +
        (this.getWeek() - 1) +
        '&Select=LB40, "' +
        localStorage.getItem('code') +
        '"' +
        '&output=embed'
    );
    this.urlCH83 = this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://qv.lhinside.com/QvAJAXZfc/singleobject.htm?document=userdocs%20itind%5Ccif%20dashboard.qvw&object=CH83&Select=LB33,' +
        this.getWeekYear() +
        '-' +
        (this.getWeek() - 1) +
        '&Select=LB40, "' +
        localStorage.getItem('code') +
        '"' +
        '&output=embed'
    );
    this.urlCH128 = this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://qv.lhinside.com/QvAJAXZfc/singleobject.htm?document=userdocs%20itind%5Ccif%20dashboard.qvw&object=CH128&Select=LB33,' +
        this.getWeekYear() +
        '-' +
        (this.getWeek() - 1) +
        '&Select=LB40, "' +
        localStorage.getItem('code') +
        '"' +
        '&output=embed'
    );

    // tslint:disable-next-line:max-line-length
    // this.urlDashboardlink = this.sanitizer.bypassSecurityTrustResourceUrl('https://qv.lhinside.com/QvAJAXZfc/singleobject.htm?document=userdocs%20itind%5Ccif%20dashboard.qvw&Select=LB33,'+ this.getWeekYear()+ "-" + this.getWeek()+'&Select=LB37,' + localStorage.getItem('plantName'));
    // console.log(this.urlCH128);
    // this.urlTX268_2= JSON.stringify(this.urlTX268);
    // tslint:disable-next-line:max-line-length
    // this.urlTX268_2=this.sanitizer.sanitize(SecurityContext.RESOURCE_URL, this.sanitizer.bypassSecurityTrustResourceUrl('https://qv.lhinside.com/QvAJAXZfc/singleobject.htm?document=userdocs%20itind%5Ccif%20dashboard.qvw&object=TX268&Select=LB33,' + this.getWeekYear() + "-" + (this.getWeek()-1) + '&Select=LB40, "' + localStorage.getItem('code') + '"'));

    this.func1();
    this.fillDashboard();
    // console.log(this.urlTX268_2);

    /*      this.webUrlPreviewService.headRequest(this.urlTX268_2).subscribe(
          (data: HttpResponse<any>) => {
            console.log("data: "+ JSON.stringify(data));
            this.repsonseData= JSON.stringify(data);
             console.log("error ETag: "+ data.headers.get('ETag'));
             console.log("error in data: "+data.headers.get('error'));
             console.log("error X-Frame-Options: "+data.headers.get('X-Frame-Options'));
           },
           (error: HttpResponse<any>)=>{
             console.log("error_cors: ", JSON.parse(JSON.stringify(error)).message);
           this.error_cors=JSON.parse(JSON.stringify(error)).message;
    //       console.log(this.urlTX268_func());

          }
        )
  }
 */
    /*
   urlTX268_func(){
    return this.error_cors.includes('Unknown Error');
  }
*/
  }


  func1() {
    window.onerror = function(message, source, lineno, colno, error) {
      console.log(message), (this.onerror = error);
      // alert(message + ' ' + source + ' ' + lineno + ' ' + colno + ' ' + error);
    };
    // tslint:disable-next-line:only-arrow-functions
    window.onunhandledrejection = function(e) {
      console.log(e);
    };
  }

  // func2(){
  //   const cookies = document.cookie.split(';'); // contains all the cookies
  //   const cookieName = []; // to contain name of all the cookies
  //   let i: any;
  //   console.log (JSON.stringify(cookies));
  //   for (i = 0; i < cookies.length; i++) {
  //       cookieName[i] = cookies[i].split('=')[0].trim();
  //   }

  //   if (cookieName.indexOf('https://qv.lhinside.com') > -1) {
  //           console.log('EXISTS');
  //       } else {
  //           console.log('DOESN\'T EXIST');
  //       }
  // }

  /*   urlTX268_3(){
    return this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://qv.lhinside.com/QvAJAXZfc/singleobject.htm?document=userdocs%20itind%5Ccif%20dashboard.qvw&object=TX268&Select=LB33,' +
     this.getWeekYear() + "-" + (this.getWeek()-1) + '&Select=LB40, "' + localStorage.getItem('code') + '"');
  }
 */
  /*
  //chart.js
  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    //console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    //console.log(event, active);
  }

  /*
  //Initial dialog screen popupDialog
  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = { name: "some name"};
    let dialogRef = this.dialog.open(PopupDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(value => {
      console.log(`Dialog sent: ${value}`);
    });
  }

*/
  /*
rangewaves(nwaves){
  const range = Array.from({length: nwaves}, (_, i) => i + 1);
  this.waves=range;
  localStorage.setItem('waves', JSON.stringify(this.waves));
  //console.log("range: "+range);
  //console.log("Array: "+Array.from({length: nwaves}, (_, i) => i + 1));
  return range;
  }
*/

  // Check if the selected wave it the last wave in progress or not to blink the border color on mat-select wave's selector
  check() {
    const a = JSON.parse(localStorage.getItem("nwavemax"));
    if (a === this.nwaveSelected) {
      return true;
    } else {
      return false;
    }
  }

  // select the selector wave if change the value populate the correct data in the view.
  nwaveSelectChange(value) {
    localStorage.setItem("nwaveSelected", value);
    // this.nwavemax = this.waves.reduce((a, b)=>Math.max(a, b));
    this.nwavemax = JSON.parse(localStorage.getItem("nwavemax"));
    this.nwaveSelected = Number(localStorage.getItem("nwaveSelected"));
    this.doughnutChartDataPreparation = [0, 0, 0, 0, 100];
    this.doughnutChartDataAssessment = [0, 0, 0, 0, 100];
    this.doughnutChartDataImplementation = [0, 0, 0, 0, 100];
    this.doughnutChartDataFollowup = [0, 0, 0, 0, 100];
    this.doughnutChartDataSustainability = [0, 0, 0, 0, 100];
    // this.nwavemax = Number(this.waves.reduce((a, b) => Math.max(a, b)));

    // this.nwavemax = Number(JSON.parse(localStorage.getItem("waves")).reduce((a, b)=>Math.max(a, b)));
    this.PlantIdserv.selectedPlantPercentages().subscribe(
      (data) => {
        for (const plantData of data) {
          localStorage.setItem(plantData.plant_Name, JSON.stringify(plantData));
        }
        this.PlantId = localStorage.getItem("plantName");
        this.plantID = Number(localStorage.getItem("plantID"));

        if (data[0] && data[0].userTaskDetails) {
          for (const taskDetails of data[0].userTaskDetails) {
            const processID = taskDetails.processID;
            if (processID) {
              if (processID === "1") {
                this.getTotalNumberOfTask1(taskDetails);
                this.getNumberOfTaskPerProcess1(taskDetails);
                this.taskData1.onlyFloatingTasksPending =
                  taskDetails.onlyFloatingTasksPending;
              }
              if (processID === "2") {
                this.getTotalNumberOfTask2(taskDetails);
                this.getNumberOfTaskPerProcess2(taskDetails);
                this.taskData2.onlyFloatingTasksPending =
                  taskDetails.onlyFloatingTasksPending;
              }

              if (processID === "3") {
                this.getTotalNumberOfTask3(taskDetails);
                this.getNumberOfTaskPerProcess3(taskDetails);
                this.taskData3.onlyFloatingTasksPending =
                  taskDetails.onlyFloatingTasksPending;
              }

              if (processID === "4") {
                this.getTotalNumberOfTask4(taskDetails);
                this.getNumberOfTaskPerProcess4(taskDetails);
                this.taskData4.onlyFloatingTasksPending =
                  taskDetails.onlyFloatingTasksPending;
              }

              if (processID === "5") {
                this.getTotalNumberOfTask5(taskDetails);
                this.getNumberOfTaskPerProcess5(taskDetails);
                this.taskData5.onlyFloatingTasksPending =
                  taskDetails.onlyFloatingTasksPending;
              }
            }
          }
        }
        this.setPercentage();
        this.getTaskPanelOverviewData();
        this.getTaskPanelCompetencyData();

        const htmlMessage = "";

        // this.taskService.retrieveuserTaskData(1).subscribe(data =>  {this.taskData1 = data;
        //   //console.log("taskData1 :"+ JSON.stringify(this.taskData1));
        //   this.getTotalNumberOfTask1(this.taskData1);
        //   this.getNumberOfTaskPerProcess1(this.taskData1);
        //   this.setPercentage()
        //   //console.log("totalPrecentPreparation: "+JSON.stringify(this.totalPrecentPreparation));
        //   });
        // this.taskService.retrieveuserTaskData(2).subscribe(data =>  {this.taskData2 = data;
        //   //console.log("taskData2 :"+ JSON.stringify(this.taskData2));
        //   this.getTotalNumberOfTask2(this.taskData2);
        //   this.getNumberOfTaskPerProcess2(this.taskData2);
        //   this.setPercentage()
        //   //console.log("totalPrecentAssessment: "+JSON.stringify(this.totalPrecentAssessment));
        //   //console.log(this.totalPrecentAssessment);
        //   });
        //   this.taskService.retrieveuserTaskData(3).subscribe(data =>  {this.taskData3 = data;
        //   //console.log("taskData3 :"+ JSON.stringify(this.taskData3));
        //   this.getTotalNumberOfTask3(this.taskData3);
        //   this.getNumberOfTaskPerProcess3(this.taskData3);
        //   this.setPercentage()
        //   //console.log("totalPrecentImplementation: "+JSON.stringify(this.totalPrecentImplementation));
        //   //console.log(this.totalPrecentImplementation);
        //   });
        // this.taskService.retrieveuserTaskData(4).subscribe(data =>  {this.taskData4 = data;
        //   //console.log("taskData4 :"+ JSON.stringify(this.taskData4));
        //   this.getTotalNumberOfTask4(this.taskData4);
        //   this.getNumberOfTaskPerProcess4(this.taskData4);
        //   this.setPercentage()
        //   //console.log("totalPrecentFollowUp: "+JSON.stringify(this.totalPrecentFollowUp));
        //   });
        // this.taskService.retrieveuserTaskData(5).subscribe(data =>  {this.taskData5 = data;
        //   //console.log("taskData5 :"+ JSON.stringify(this.taskData5));
        //   this.getTotalNumberOfTask5(this.taskData5);
        //   this.getNumberOfTaskPerProcess5(this.taskData5);
        //   this.setPercentage()
        //   //console.log("totalPrecentSustainability: "+JSON.stringify(this.totalPrecentSustainability));
        //   });
      }
    );

    if (!this.cookieService.get('snackbarset')){
      this.snackBar.openFromComponent(CustomSnackBarComponent,
      {
        verticalPosition: 'top',
        panelClass : ['red-snackbar']
      }, );
     }

  }

  /*
nwaveSelect(){
  if(Number(localStorage.getItem('nwaveSelected'))===null){
  this.nwaveSelected= this.waves.reduce((a, b)=>Math.max(a, b));
      this.nwaveSelected=Number(localStorage.getItem('nwaveSelected'));
  }
      else
      {
        console.log("nowaves");
      }
    }
*/

  getWeek() {
    const date = new Date();
    date.setHours(0, 0, 0, 0);
    // Thursday in current week decides the year.
    date.setDate(date.getDate() + 3 - ((date.getDay() + 6) % 7));
    // January 4 is always in week 1.
    const week1 = new Date(date.getFullYear(), 0, 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return (
      1 +
      Math.round(
        ((date.getTime() - week1.getTime()) / 86400000 -
          3 +
          ((week1.getDay() + 6) % 7)) /
          7
      )
    );
  }

  getWeekYear() {
    const date = new Date();
    date.setDate(date.getDate() + 3 - ((date.getDay() + 6) % 7));
    return date.getFullYear();
  }

  getTaskPanelOverviewData() {
    const plantData: any = JSON.parse(
      localStorage.getItem(localStorage.getItem("plantName"))
    );
    const plantProcessData: any = plantData.processPercentages;
    let processID: any;
    let categ = "";
    // for (var process of plantProcessData) {
    //   if (process.statusID == 1 || process.statusID == 4) {
    //     processID = process.processId
    //     if (processID == 1) {
    //       categ = "Preparation";
    //     }
    //     if (processID == 2) {
    //       categ = "Assessment";
    //     }
    //     if (processID == 3) {
    //       categ = "Implementation";
    //     }
    //     if (processID == 4) {
    //       categ = "Followup";
    //     }
    //     if (processID == 5) {
    //       categ = "Sustainability";
    //     }
    //     break;
    //   }
    // }
    processID = 1;
    if (plantProcessData && processID) {
      this.ELEMENT_DATA = [];
      this.userService.getTasksForOverView(processID).subscribe(
        (data) => {
          // console.log(data);

          if (data && data.taskTrans) {
            if (data.taskTrans[0].toDo) {
              for (const toDo of data.taskTrans[0].toDo) {
                const endate = formatDate(
                  new Date(toDo.endDate),
                  "MM/dd/yyyy",
                  "en"
                );
                const wave = toDo.wave;
                const taskName = toDo.name;
                categ = this.getCategory(toDo);
                const elem = {
                  date: endate,
                  wave,
                  status: "To Do",
                  category: categ,
                  name: taskName,
                };
                this.ELEMENT_DATA.push(elem);
              }

              for (const dueSoon of data.taskTrans[0].dueSoon) {
                const endate = formatDate(
                  new Date(dueSoon.endDate),
                  "MM/dd/yyyy",
                  "en"
                );
                const wave = dueSoon.wave;
                const taskName = dueSoon.name;
                categ = this.getCategory(dueSoon);
                const elem = {
                  date: endate,
                  wave,
                  status: "Due Soon",
                  category: categ,
                  name: taskName,
                };
                this.ELEMENT_DATA.push(elem);
              }

              for (const overDue of data.taskTrans[0].overDue) {
                const endate = formatDate(
                  new Date(overDue.endDate),
                  "MM/dd/yyyy",
                  "en"
                );
                const wave = overDue.wave;
                const taskName = overDue.name;
                categ = this.getCategory(overDue);
                const elem = {
                  date: endate,
                  wave,
                  status: "Over Due",
                  category: categ,
                  name: taskName,
                };
                this.ELEMENT_DATA.push(elem);
              }

              // for(var notSorted of data.taskTrans[0].notSorted){
              //   let endate = formatDate(new Date(notSorted.endDate), 'MM/dd/yyyy', 'en');
              //   var elem ={
              //      date: endate, status: 'Not Started', category: categ
              //   }
              //   ELEMENT_DATA.push(elem);
              // }
            }
          }
          this.content.dataSource = new MatTableDataSource<TaskElements>(
            this.ELEMENT_DATA
          );
          // setTimeout(() => this.content.dataSource.paginator = this.paginator; //), 1000;
          this.content.dataSource.paginator = this.paginator;
          // this.content.dataSource.sort = this.sorter1;
          this.PlantIdserv.changeLoadingValue(false);
        },
        // (error) => {
          // console.log("ELEMENT_DATA: " + this.ELEMENT_DATA);
        // }
      );
    } else {
      this.PlantIdserv.changeLoadingValue(false);
    }
  }

  getCategory(task: any) {
    let categ = '';
    if (task.processId === 1) {
      categ = 'Preparation';
    }
    if (task.processId === 2) {
      categ = 'Assessment';
    }
    if (task.processId === 3) {
      categ = 'Implementation';
    }
    if (task.processId === 4) {
      categ = 'Followup';
    }
    if (task.processId === 5) {
      categ = 'Sustainability';
    }
    return categ;
  }

  // getTaskPanelCompetencyData(){
  // console.log("this access Competency func");
  // this.content2.dataSource2 = new MatTableDataSource<TaskElements2>(this.COMPETENCY_DATA);
  // setTimeout(() => this.content.dataSource.paginator = this.paginator; //), 1000;
  // this.content2.dataSource2.paginator2 = this.paginator2;
  //    console.log ("hello");
  // this.content2.dataSource2.sort2 = this.sorter2;
  // setTimeout(() => this.content2.dataSource2.sort2 = this.sort2), 1000;
  // }

  setPercentage() {
    const plantName = localStorage.getItem('plantName');
    const plantData = JSON.parse(localStorage.getItem(plantName));
    if (plantData) {
      this.totalPercentage = Math.round(
        Number(plantData.totalPercentages.toFixed(2))
      );
    }
    if (this.totalPercentage === 0.0) {
      this.totalPercentage = 0;
    }
    this.content = {};
    const isFacilitatorAdmin = localStorage.getItem('facilitatorAdmin');
    this.content.isFacilitatorAdmin = false;
    if (isFacilitatorAdmin === 'yes') {
      this.content.isFacilitatorAdmin = true;
    }
    // this.value=Number(this.totalPercentage);
    // if (this.totalPercentage == 0.00) {
    this.percentPrep = 0;
    this.percentAssesment = 0;
    this.percentFollowUp = 0;
    this.percentImplementation = 0;
    this.percentSustain = 0;
    if (
      plantData &&
      plantData.processPercentages &&
      plantData.processPercentages.length > 0
    ) {
      const processData = plantData.processPercentages;
      for (const process of processData) {
        if (process.processName === "Preparation") {
          this.percentPrep = process.processPercentage.toFixed(2);
          if (process.processStartDate) {
            this.content.prepStartDate = formatDate(
              new Date(process.processStartDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.processPausedDate) {
            this.content.prepPauseDate = formatDate(
              new Date(process.processPausedDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.statusID) {
            this.content.prepStatusID = process.statusID;
          }
          if (process.processCompletedDate) {
            this.content.prepCompletedDate = formatDate(
              new Date(process.processCompletedDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.processTransactID) {
            this.content.processPrepTransactID = process.processTransactID;
          }
        }
        if (process.processName === "Assessment") {
          this.percentAssesment = process.processPercentage.toFixed(2);
          if (process.processStartDate) {
            this.content.asmentStartDate = formatDate(
              new Date(process.processStartDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.statusID) {
            this.content.assmntStatusID = process.statusID;
          }
          if (process.processPausedDate) {
            this.content.asmntPauseDate = formatDate(
              new Date(process.processPausedDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.processCompletedDate) {
            this.content.assmntCompletedDate = formatDate(
              new Date(process.processCompletedDate),
              "MM/dd/yyyy",
              "en"
            );
          }

          if (process.processTransactID) {
            this.content.processAsmntTransactID = process.processTransactID;
          }
        }
        if (process.processName === "Implementation") {
          this.percentImplementation = process.processPercentage.toFixed(2);
          if (process.processStartDate) {
            this.content.implStartDate = formatDate(
              new Date(process.processStartDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.statusID) {
            this.content.implStatusID = process.statusID;
          }
          if (process.processPausedDate) {
            this.content.implPauseDate = formatDate(
              new Date(process.processPausedDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.processCompletedDate) {
            this.content.implCompletedDate = formatDate(
              new Date(process.processCompletedDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.processTransactID) {
            this.content.processImplTransactID = process.processTransactID;
          }
        }
        if (process.processName === "Followup") {
          this.percentFollowUp = process.processPercentage.toFixed(2);
          if (process.processStartDate) {
            this.content.foloupStartDate = formatDate(
              new Date(process.processStartDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.statusID) {
            this.content.foloupStatusID = process.statusID;
          }
          if (process.processPausedDate) {
            this.content.foloupPausedDate = formatDate(
              new Date(process.processPausedDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.processCompletedDate) {
            this.content.foloupCompletedDate = formatDate(
              new Date(process.processCompletedDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.processTransactID) {
            this.content.processFoloupTransactID = process.processTransactID;
          }
        }
        if (process.processName === "Sustainability") {
          this.percentSustain = process.processPercentage.toFixed(2);
          if (process.processStartDate) {
            this.content.sustainStartDate = formatDate(
              new Date(process.processStartDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.statusID) {
            this.content.sustainStatusID = process.statusID;
          }
          if (process.processPausedDate) {
            this.content.sustainPausedDate = formatDate(
              new Date(process.processPausedDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.processCompletedDate) {
            this.content.sustainCompletedDate = formatDate(
              new Date(process.processCompletedDate),
              "MM/dd/yyyy",
              "en"
            );
          }
          if (process.processTransactID) {
            this.content.processSustainTransactID = process.processTransactID;
          }
        }
      }
    }

    this.checkStartButton();
  }

  // checkStartButton() {

  //   if (this.percentPrep === 0 || this.percentPrep === 0.00) {
  //     this.content.enablePrep = true;
  //     this.content.enableAssement = false;
  //     this.content.enableImpl = false;
  //     this.content.enableFoloup = false;
  //     this.content.enableSustain = false;
  //   }
  //   if (
  //     (this.percentPrep !== 0 && this.percentPrep < 100) ||
  //     (this.percentPrep !== 0 && this.percentAssesment < 100) ||
  //     (this.percentImplementation !== 0 && this.percentImplementation < 100) ||
  //     (this.percentFollowUp !== 0 && this.percentFollowUp < 100)
  //   ) {
  //     this.content.enablePrep = false;
  //     this.content.enableAssement = false;
  //     this.content.enableImpl = false;
  //     this.content.enableFoloup = false;
  //     this.content.enableSustain = false;
  //   }
  //   if (
  //     this.percentAssesment === 0 &&
  //     (this.percentPrep === 100 || this.onFloatingTaskPending(this.taskData1))
  //   ) {
  //     this.content.enablePrep = false;
  //     this.content.enableAssement = true;
  //     this.content.enableImpl = false;
  //     this.content.enableFoloup = false;
  //     this.content.enableSustain = false;
  //   }
  //   if (
  //     this.percentImplementation === 0 &&
  //     (this.percentAssesment === 100 ||
  //       this.onFloatingTaskPending(this.taskData2))
  //   ) {
  //     this.content.enablePrep = false;
  //     this.content.enableAssement = false;
  //     this.content.enableImpl = true;
  //     this.content.enableFoloup = false;
  //     this.content.enableSustain = false;
  //   }
  //   if (
  //     this.percentFollowUp === 0 &&
  //     (this.percentImplementation === 100 ||
  //       this.onFloatingTaskPending(this.taskData3))
  //   ) {
  //     this.content.enablePrep = false;
  //     this.content.enableAssement = false;
  //     this.content.enableImpl = false;
  //     this.content.enableFoloup = true;
  //     this.content.enableSustain = false;
  //   }
  //   if (
  //     this.percentSustain === 0 &&
  //     (this.percentFollowUp === 100 ||
  //       this.onFloatingTaskPending(this.taskData4))
  //   ) {
  //     this.content.enablePrep = false;
  //     this.content.enableAssement = false;
  //     this.content.enableImpl = false;
  //     this.content.enableFoloup = false;
  //     this.content.enableSustain = true;
  //   }
  // }
  checkStartButton() {
    let roundPrep = Math.round(this.percentPrep);

    let roundAsmnt = Math.round(this.percentAssesment);

    let roundImpl =  Math.round(this.percentImplementation) ;

    let roundFolouo =  Math.round(this.percentFollowUp);

    let roundSustain = this.percentSustain;
      if (roundPrep === 0 || roundPrep === 0.00) {
        this.content.enablePrep = true;
        this.content.enableAssement = false;
        this.content.enableImpl = false;
        this.content.enableFoloup = false;
        this.content.enableSustain = false;
      }
      if (
        (roundPrep !== 0 && roundPrep < 100) ||
        (roundPrep !== 0 && roundAsmnt < 100) ||
        (roundImpl !== 0 && roundImpl < 100) ||
        (roundFolouo !== 0 && roundFolouo < 100)
      ) {
        this.content.enablePrep = false;
        this.content.enableAssement = false;
        this.content.enableImpl = false;
        this.content.enableFoloup = false;
        this.content.enableSustain = false;
      }
      if (
        roundAsmnt === 0 &&
        (roundPrep === 100 || this.onFloatingTaskPending(this.taskData1))
      ) {
        this.content.enablePrep = false;
        this.content.enableAssement = true;
        this.content.enableImpl = false;
        this.content.enableFoloup = false;
        this.content.enableSustain = false;
      }
      if (
        roundImpl === 0 &&
        (roundAsmnt === 100 ||
          this.onFloatingTaskPending(this.taskData2))
      ) {
        this.content.enablePrep = false;
        this.content.enableAssement = false;
        this.content.enableImpl = true;
        this.content.enableFoloup = false;
        this.content.enableSustain = false;
      }
      if (
        roundFolouo === 0 &&
        (roundImpl === 100 ||
          this.onFloatingTaskPending(this.taskData3))
      ) {
        this.content.enablePrep = false;
        this.content.enableAssement = false;
        this.content.enableImpl = false;
        this.content.enableFoloup = true;
        this.content.enableSustain = false;
      }
      if (
        roundSustain === 0 &&
        (roundFolouo === 100 ||
          this.onFloatingTaskPending(this.taskData4))
      ) {
        this.content.enablePrep = false;
        this.content.enableAssement = false;
        this.content.enableImpl = false;
        this.content.enableFoloup = false;
        this.content.enableSustain = true;
      }
    }
  onFloatingTaskPending(taskContent: any): boolean {
    if (taskContent && taskContent.onlyFloatingTasksPending) {
      return taskContent.onlyFloatingTasksPending;
    } else {
      return false;
    }
  }

  onStartClick(processID: any) {
    this.processStart = true;
    const dialogRef = this.dialog.open(ConfirmProcesDialogComponent, {
      data: { processID },
    });
  }

  srcGoToDashboard() {
   const url = 'https://datastudio.google.com/embed/u/0/reporting/f8286bba-aaf9-4946-b42a-3a9e92f0412d/page/IkLbC?params=%7B%22df119%22:%22include%25EE%2580%25800%25EE%2580%2580IN%25EE%2580%2580' + localStorage.getItem('plantName') + '%22%/7D';
   return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  goToLink() {
    // window.open(urlDashboardlink, "_blank");
    this.urlDashboardlink =
      "https://qv.lhinside.com/QvAJAXZfc/opendoc.htm?document=userdocs%20itind%5Ccif%20dashboard.qvw&lang=en-US&host=QVS%40Cluster&Select=LB33," +
      this.getWeekYear() +
      "-" +
      (this.getWeek() - 1) +
      "&Select=LB40," +
      '"' +
      localStorage.getItem("code") +
      '"';
    window.open(this.urlDashboardlink, "_blank");
    // console.log(this.urlDashboardlink);
    // this.func2();
  }

  goToTaskPage(processID: any) {
    localStorage.setItem("processID", processID);
    const plantCode = localStorage.getItem("code");
    this.router.navigate(["/main/task"], { queryParams: { plantCode } });
  }
  pauseProcess(processTransactID: any, processID: number, pauseOrResume: any) {
    const data: any = {};

    data.processTransactID = processTransactID;
    data.processID = processID;
    data.process = pauseOrResume;

    const dialogRef = this.dialog.open(ConfirmProcesDialogComponent, {
      data: { data },
    });
  }

  resumeProcess(processTransactID: any) {
    this.userService.resumeProcess(processTransactID).subscribe(
      (data) => {},
      (error) => {}
    );
  }

  logout() {
    this.OAuth.signOut().then((data) => {
      // tslint:disable-next-line:no-debugger
      debugger;
    });
    this.router.navigateByUrl("/login");
  }

  // funcRefreshIframes(){
  // document.getElementById('TX268').contentWindow.location.reload();
  // document.getElementById('TX268').src = document.getElementById('TX268').id;
  // }

  FailLoadIframe() {
    //    alert("I am an alert box!");
    (document.getElementById("loadingMessage") as HTMLElement).style.display =
      "inline";
  }

  exportAsXLSX() {
    this.task2 = JSON.parse(JSON.stringify(this.ELEMENT_DATA)); // , this.ELEMENT_DATA.result || [];
    this.excelService.exportAsExcelFile(this.task2, "Tasks detail");
    // console.log("tarea: " + JSON.stringify(this.task2));
  }

  exportAsXLSX2() {
    this.task3 = JSON.parse(JSON.stringify(this.COMPETENCY_DATA));
    this.excelService.exportAsExcelFile(this.task3, "Competency detail");
    // console.log("tarea: " + JSON.stringify(this.task2));
  }

  getTotalNumberOfTask1(content: any) {
    this.totalPrecentPreparation =
      content.taskTrans[0].completedTask.length +
      content.taskTrans[0].toDo.length +
      content.taskTrans[0].dueSoon.length +
      content.taskTrans[0].overDue.length +
      content.taskTrans[0].notSorted.length;
    return this.totalPrecentPreparation;
  }
  getTotalNumberOfTask2(content: any) {
    this.totalPrecentAssessment =
      content.taskTrans[0].completedTask.length +
      content.taskTrans[0].toDo.length +
      content.taskTrans[0].dueSoon.length +
      content.taskTrans[0].overDue.length +
      content.taskTrans[0].notSorted.length;
    return this.totalPrecentAssessment;
  }
  getTotalNumberOfTask3(content: any) {
    this.totalPrecentImplementation =
      content.taskTrans[0].completedTask.length +
      content.taskTrans[0].toDo.length +
      content.taskTrans[0].dueSoon.length +
      content.taskTrans[0].overDue.length +
      content.taskTrans[0].notSorted.length;
    return this.totalPrecentImplementation;
  }
  getTotalNumberOfTask4(content: any) {
    this.totalPrecentFollowUp =
      content.taskTrans[0].completedTask.length +
      content.taskTrans[0].toDo.length +
      content.taskTrans[0].dueSoon.length +
      content.taskTrans[0].overDue.length +
      content.taskTrans[0].notSorted.length;
    return this.totalPrecentFollowUp;
  }
  getTotalNumberOfTask5(content: any) {
    this.totalPrecentSustainability =
      content.taskTrans[0].completedTask.length +
      content.taskTrans[0].toDo.length +
      content.taskTrans[0].dueSoon.length +
      content.taskTrans[0].overDue.length +
      content.taskTrans[0].notSorted.length;
    return this.totalPrecentSustainability;
  }

  getNumberOfTaskPerProcess1(content: any) {
    setTimeout(() => {
      this.doughnutChartDataPreparation = [
        this.doughnutChartDataPreparation[0],
        this.doughnutChartDataPreparation[1],
        this.doughnutChartDataPreparation[2],
        this.doughnutChartDataPreparation[3],
        this.doughnutChartDataPreparation[4],
      ];
      this.doughnutChartDataPreparation[0] =
        content.taskTrans[0].completedTask.length;
      this.doughnutChartDataPreparation[1] = content.taskTrans[0].toDo.length;
      this.doughnutChartDataPreparation[2] =
        content.taskTrans[0].dueSoon.length;
      this.doughnutChartDataPreparation[3] =
        content.taskTrans[0].overDue.length;
      this.doughnutChartDataPreparation[4] =
        content.taskTrans[0].notSorted.length;
      if (this.totalPrecentPreparation === 0) {
        this.doughnutChartDataPreparation = [0, 0, 0, 0, 100];
      }
      // console.log(this.doughnutChartDataPreparation);
      return this.doughnutChartDataPreparation;
    }, 1000);
  }

  getNumberOfTaskPerProcess2(content: any) {
    setTimeout(() => {
      this.doughnutChartDataAssessment = [
        this.doughnutChartDataAssessment[0],
        this.doughnutChartDataAssessment[1],
        this.doughnutChartDataAssessment[2],
        this.doughnutChartDataAssessment[3],
        this.doughnutChartDataAssessment[4],
      ];
      this.doughnutChartDataAssessment[0] =
        content.taskTrans[0].completedTask.length;
      this.doughnutChartDataAssessment[1] = content.taskTrans[0].toDo.length;
      this.doughnutChartDataAssessment[2] = content.taskTrans[0].dueSoon.length;
      this.doughnutChartDataAssessment[3] = content.taskTrans[0].overDue.length;
      this.doughnutChartDataAssessment[4] =
        content.taskTrans[0].notSorted.length;
      if (this.totalPrecentAssessment === 0) {
        this.doughnutChartDataAssessment = [0, 0, 0, 0, 100];
      }
      // console.log(this.doughnutChartDataAssessment);
      return this.doughnutChartDataAssessment;
    }, 1000);
  }

  getNumberOfTaskPerProcess3(content: any) {
    setTimeout(() => {
      this.doughnutChartDataImplementation = [
        this.doughnutChartDataImplementation[0],
        this.doughnutChartDataImplementation[1],
        this.doughnutChartDataImplementation[2],
        this.doughnutChartDataImplementation[3],
        this.doughnutChartDataImplementation[4],
      ];
      this.doughnutChartDataImplementation[0] =
        content.taskTrans[0].completedTask.length;
      this.doughnutChartDataImplementation[1] =
        content.taskTrans[0].toDo.length;
      this.doughnutChartDataImplementation[2] =
        content.taskTrans[0].dueSoon.length;
      this.doughnutChartDataImplementation[3] =
        content.taskTrans[0].overDue.length;
      this.doughnutChartDataImplementation[4] =
        content.taskTrans[0].notSorted.length;
      if (this.totalPrecentImplementation === 0) {
        this.doughnutChartDataImplementation = [0, 0, 0, 0, 100];
      }
      return this.doughnutChartDataImplementation;
    }, 1000);
  }

  getNumberOfTaskPerProcess4(content: any) {
    setTimeout(() => {
      this.doughnutChartDataFollowup = [
        this.doughnutChartDataFollowup[0],
        this.doughnutChartDataFollowup[1],
        this.doughnutChartDataFollowup[2],
        this.doughnutChartDataFollowup[3],
        this.doughnutChartDataFollowup[4],
      ];
      this.doughnutChartDataFollowup[0] =
        content.taskTrans[0].completedTask.length;
      this.doughnutChartDataFollowup[1] = content.taskTrans[0].toDo.length;
      this.doughnutChartDataFollowup[2] = content.taskTrans[0].dueSoon.length;
      this.doughnutChartDataFollowup[3] = content.taskTrans[0].overDue.length;
      this.doughnutChartDataFollowup[4] = content.taskTrans[0].notSorted.length;
      if (this.totalPrecentFollowUp === 0) {
        this.doughnutChartDataFollowup = [0, 0, 0, 0, 100];
      }
      return this.doughnutChartDataFollowup;
    }, 1000);
  }

  getNumberOfTaskPerProcess5(content: any) {
    setTimeout(() => {
      this.doughnutChartDataSustainability = [
        this.doughnutChartDataSustainability[0],
        this.doughnutChartDataSustainability[1],
        this.doughnutChartDataSustainability[2],
      ];
      this.doughnutChartDataSustainability[0] =
        content.taskTrans[0].completedTask.length;
      this.doughnutChartDataSustainability[1] =
        content.taskTrans[0].toDo.length;
      this.doughnutChartDataSustainability[2] =
        content.taskTrans[0].dueSoon.lenth;
      this.doughnutChartDataSustainability[3] =
        content.taskTrans[0].overDue.length;
      this.doughnutChartDataSustainability[4] =
        content.taskTrans[0].notSorted.length;
      if (this.totalPrecentSustainability === 0) {
        this.doughnutChartDataSustainability = [0, 0, 0, 0, 100];
      }
      return this.doughnutChartDataSustainability;
    }, 1000);
  }

  get completed3phases(): boolean {
    // let a = Number(JSON.parse(localStorage.getItem("nwaveSelected")));
    // let b = Number(JSON.parse(localStorage.getItem("waves")).length);
    // tslint:disable-next-line:max-line-length
    if (
      this.percentPrep === "100.00" &&
      this.percentAssesment === "100.00" &&
      this.percentImplementation === "100.00" &&
      this.check()
    ) {
      // console.log (a+" "+b);
      return true;
    } else {
      return false;
    }
  }

  closewave(plantID: number) {
    this.PlantIdservice.closeWave(plantID).subscribe((result) => {
      // console.log(result);
    });

    setTimeout(() => {
      this.PlantIdserv.getAllClosedWave().subscribe((data) => {
        this.nwaves = data;
        // this.nwaves=(Number(this.nwaves)+2);
        localStorage.setItem(
          "waves",
          JSON.stringify(this.rangewaves(Number(this.nwaves) + 1))
        );
        localStorage.setItem("Nowaves", data);
        this.nwaveSelected = this.waves.reduce((a, b) => Math.max(a, b));
        localStorage.setItem(
          "nwaveSelected",
          JSON.stringify(this.nwaveSelected)
        );
        this.nwavemax = this.waves.reduce((a, b) => Math.max(a, b));
        localStorage.setItem("nwavemax", JSON.stringify(this.nwavemax));
        // this.nwaveSelectedHistory=this.nwaveSelectedHistoryFunc(this.nwaves);
        // localStorage.setItem('nwaveSelectedHistory', JSON.stringify(this.nwaveSelectedHistory));
        this.NArrayWavesserv.changeNArrayWaves(this.waves);
        this.NWavesserv.changeWaveSelected(this.nwaveSelected);
        // localStorage.setItem('waves', JSON.stringify(this.waves));
        localStorage.setItem(
          "nwaveSelected",
          JSON.stringify(this.nwaveSelected)
        );
        localStorage.setItem("viewClosedWave", "1");
      });
      // console.log("Button close wave pushed for plantId:"+ JSON.stringify(plantID)+"/"+JSON.stringify(this.nwaves));
      // tslint:disable-next-line:no-unused-expression
      this.completed3phases;
      this.nwaveSelectChange(this.nwaveSelected);
    }, 300);
  }

  nwaveSelectedHistoryFunc(nwaves: number) {
    // let nwaves = localStorage.getItem('waves');
    if (localStorage.getItem("Nowaves") === "") {
      this.nwaves = this.nwaves + 1;
      localStorage.setItem("Nowaves", JSON.stringify(Number(this.nwaves)));
      // console.log("sutituido valor 0 de nwaves al venir vacio");
      return Number(this.nwaves);
    } else {
      return Number(nwaves);
    }
  }

  rangewaves(nwaves) {
    const range = Array.from({ length: nwaves }, (_, i) => i + 1);
    this.waves = range;
    // localStorage.setItem('waves', JSON.stringify(this.waves));
    // console.log("range: "+range);
    // console.log("Array: "+Array.from({length: nwaves}, (_, i) => i + 1));
    return range;
  }

  getTaskPanelCompetencyData() {
    this.COMPETENCY_DATA = [];
    if(localStorage.getItem('isCorporate')){
      this.isCorporate = true;
    }
    this.CompetencyService.getCompetency().subscribe((data) => {
      // console.log(JSON.stringify(data));
      // setTimeout(() => this.content.dataSource.paginator = this.paginator; //), 1000;
      for (data of data) {
        const courseName = data.courseName;
        const enrolled = data.enrolled;
        const inProgress = data.inProgress;
        const completed = data.completed;
        const impactVote = data.impactVote;
        const averageRating = data.averageRating;
        // tslint:disable-next-line:no-shadowed-variable
        const Course = {
          courseName,
          enrolled,
          inProgress: data.inProgress,
          completed,
          impactVote,
          averageRating,
        };
        this.COMPETENCY_DATA.push(Course);
      }
      this.content2.dataSource2 = new MatTableDataSource<TaskElements2>(
        this.COMPETENCY_DATA
      );
      this.content2.dataSource2.paginator2 = this.paginator2;
    });
  }

   onClick(event){
   // console.log(event);
   // this.getTaskPanelCompetencyData();
   // this.changeDetectorRefs.detectChanges();
      setTimeout(() => (
        this.getTaskPanelCompetencyData(),
        this.changeDetectorRefs.detectChanges()), 1000);
   }

  onClickStarRating(value, element) {
    element.impactVote = value.rating;
    this.CompetencyService.updateCompetency(element.courseName, value.rating ).subscribe(data => console.log(data));
    setTimeout(() => (
    this.getTaskPanelCompetencyData(),
    this.changeDetectorRefs.detectChanges()), 1000);
  }

  GoToWorkflowByWave(element) {
    const plantCode = localStorage.getItem('code');
    localStorage.setItem('nwaveSelected', JSON.stringify(Number(element.wave)));
    const ProccessID = this.getProccessID(element.category);
    localStorage.setItem('processID', JSON.stringify(Number(ProccessID)));
    this.nwaveSelected = Number(localStorage.getItem('nwaveSelected'));
    this.router.navigate(['/main/task'], { queryParams: { plantCode } });
  }

  getProccessID(element: any) {
    let ProccessID = 0;
    if (element === 'Preparation') {
      ProccessID = 1;
    }
    if (element === 'Assessment') {
      ProccessID = 2;
    }
    if (element === 'Implementation') {
      ProccessID = 3;
    }
    if (element === 'Followup') {
      ProccessID = 4;
    }
    if (element === 'Sustainability') {
      ProccessID = 5;
    }
    return ProccessID;
  }


fillDashboard()  {
  this.barChartLabels2 = ['bar 1', 'bar 2', 'bar 3', 'bar 4', 'bar 5', 'bar 6'];
  this.barChartData2 = [{ data: [85, 74, 78, 75, 77, 75, 0], label: 'Worksstreams', backgroundColor: [], borderColor: [], borderWidth: 3, fill: false }];

  for (let i = 0; i < this.barChartData2[0].data.length; i++ ) {
    const v = this.barChartData2[0].data[i];

    if ( v > this.goal ) {
     this.fillbackgroundColor[i] = this.green;
    }
    else if ( v < this.goal ) {
      this.fillbackgroundColor[i] = this.orange;
    }
    else {
      this.fillbackgroundColor[i] = this.red;
    }
  }

  this.barChartData2[0].backgroundColor = this.fillbackgroundColor;
  this.barChartColors2[0].backgroundColor = this.fillbackgroundColor;

  this.borderColor = this.fillbackgroundColor.map(color => this.adjustColor(color, -40));
  this.barChartData2[0].borderColor = this.borderColor;

  // console.log(JSON.stringify(this.fillbackgroundColor));
  // console.log(JSON.stringify(this.borderColor));
}

  adjustColor(color, amount) {
    // tslint:disable-next-line:no-shadowed-variable
    return '#' + color.replace(/^#/, '').replace(/../g, (color: string) => ('0' + Math.min(255, Math.max(0, parseInt(color, 16) + amount)).toString(16)).substr(-2));
}

// end
}
/*
ngAfterViewInit(): void {
  console.log("afterinit");
  setTimeout(() => {
    const src = this.iframe.nativeElement.getAttribute("src");
    this.iframe.nativeElement.focus();
    if (src.endsWith("413")) {
      console.log("iframe loading failed");
      console.log("iframe loading failed: "+src);
    } else {
      console.log("iframe load succesfuly");
      console.log("iframe load succesfuly: "+src);
    }
  }, 4000);

}
 */

/*
export class EventBindingsComponent {
  onClickResult: ClickEvent;
  onHoverRatingChangeResult: HoverRatingChangeEvent;
  onRatingChangeResult: RatingChangeEvent;

  onClick = ($event: ClickEvent) => {
    console.log('onClick $event: ', $event);
    this.onClickResult = $event;
  };

  onRatingChange = ($event: RatingChangeEvent) => {
    console.log('onRatingUpdated $event: ', $event);
    this.onRatingChangeResult = $event;
  };

  onHoverRatingChange = ($event: HoverRatingChangeEvent) => {
    console.log('onHoverRatingChange $event: ', $event);
    this.onHoverRatingChangeResult = $event;
  };
}
*/

//  console.log("button close wave pushed for plantId:"+ JSON.stringify(plantID));
// this.loading = false;
// });
//   console.log("button close wave pushed for plantId:"+ JSON.stringify(plantID));
// }

/*
    onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 500) ? 1 : 12;
  }

*/

/*
    uploadDone(){
      console.log("iframe done")
      this.status=true;
    }

    errorIframe(){
      console.log("iframe error")
      this.status= false;
    }
  */

/*
   onResize(event) {
   console.log(event.target.innerWidth);
   if (event.target.innerWidth > 600 && event.target.innerWidth < 800) {
     this.pageSize = 10;
   }
   else if (event.target.innerWidth > 800 && event.target.innerWidth < 1000) {
     this.pageSize = 15;
   } else {
     this.pageSize = 5;
   }
 }
 */
