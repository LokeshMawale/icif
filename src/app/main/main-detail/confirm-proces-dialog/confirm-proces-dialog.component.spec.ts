import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmProcesDialogComponent } from './confirm-proces-dialog.component';

describe('ConfirmProcesDialogComponent', () => {
  let component: ConfirmProcesDialogComponent;
  let fixture: ComponentFixture<ConfirmProcesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmProcesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmProcesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
