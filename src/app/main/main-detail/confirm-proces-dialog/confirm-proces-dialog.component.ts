import { Component, OnInit,Inject} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { formatDate } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TasksService } from 'src/app/services/tasks.service';
import { PlantIdservice } from '../../../services/PlantIdservice';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-confirm-proces-dialog',
  templateUrl: './confirm-proces-dialog.component.html',
  styleUrls: ['./confirm-proces-dialog.component.scss']
})
export class ConfirmProcesDialogComponent implements OnInit {

  public description : String ;

  public todayDate : String;

  loading : any;

  successfull : Boolean = false;

  today : any;

  startdate : any;

  pauseResumeProcess : Boolean = false;

  pauseResume : String ;

  processTransactID : any;

  newVersionMessage : Boolean = false;

  constructor(public dialogRef: MatDialogRef<ConfirmProcesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private dialog: MatDialog,
    private taskService: TasksService,
    private plantIdService : PlantIdservice,
    public userService: UserService,
    private router: Router) { 
      dialogRef.disableClose = true;
      this.loading = false;
      this.startdate= new Date();
      this.today = new Date();
    }

  ngOnInit(): void {

    if(this.data && this.data.data && this.data.data.process ){
      this.pauseResumeProcess = true;
      this.pauseResume = this.data.data.process;
      this.processTransactID = this.data.data.processTransactID;
    }

    if(this.data.processID == 1){
      this.description = " PREPARATION ";
    }
    if(this.data.processID == 2){
      this.description = " ASSESSMENT ";
    }
    if(this.data.processID == 3){
      this.description = " IMPLEMENTATION ";
    }
    if(this.data.processID == 4){
      this.description = " FOLLOW UP ";
    }
    if(this.data.processID == 5){
      this.description = " SUSTAINABILITY ";
    }


   this.todayDate=  formatDate(new Date(), 'MM/dd/yyyy','en');

   let plantName = localStorage.getItem('plantName');


   let plantDetails = JSON.parse(localStorage.getItem(plantName));
   if(plantDetails.newVersionAfterDate)
   var afterDate = new Date(plantDetails.newVersionAfterDate);
   //var afterDate = formatDate(new Date(plantDetails.newVersionAfterDate), 'MM/dd/yyyy', 'en');
    //console.log('afterdate '+afterDate);
   if(plantDetails.processPercentages){
     for(var processData of plantDetails.processPercentages){
       if(processData.processStartDate){
        var processDate = formatDate(new Date(processData.processStartDate), 'MM/dd/yyyy', 'en');
        var newProcessDate = new Date(processDate);
        if(newProcessDate < afterDate){
          this.newVersionMessage = true;
          break;
        }
      }
     }
   }

  }

  close(){
    this.dialogRef.close();
  }

  pauseOrResume(){
    this.loading= true;
    this.successfull = false;
    if(this.pauseResume =="Pause"){
      this.userService.pauseProcess(this.processTransactID).subscribe(data => {
        this.successfull = true;
        this.loading= false;
      },
      error => {
        this.loading= false;
      });
    }

    if(this.pauseResume =="Resume"){

      this.userService.resumeProcess(this.processTransactID).subscribe(data => {
        this.successfull = true;
        this.loading= false;
      },
      error => {
        this.loading= false;
      });

    }

  }

  finalClose(){
    this.dialogRef.close();
    localStorage.setItem("processID",this.data.processID);
    this.router.navigate(['/main/task']);
   }

   pauseResumeClose(){
    this.dialogRef.close();
    this.router.navigate(['/main/task-assignment']);
   }
  
   start(){

      if(this.startdate){
        this.loading = true;
        this.startdate = formatDate(this.startdate, 'yyyy-MM-dd','en');
          this.taskService.startProcess(this.data.processID ,this.startdate).subscribe(data => {
          console.log(data);
          if(this.data.processID == 3){
            this.data.processID = "3=S1";
          }
          this.loading = false;
          this.successfull = true;
        },
          error => {
            console.log(error);
          }
        );
    }
  }
}
