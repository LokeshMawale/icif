import { Component, OnInit, ViewChild, ElementRef, OnDestroy, Input, Output, EventEmitter, Directive, HostListener } from '@angular/core';
import { WorkflowService } from '../../services/workflow.service';
import { RoleService } from '../../services/role.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Workflow } from '../../model/Workflow';
import { DataSource } from '@angular/cdk/collections';
import { ToastrService } from 'ngx-toastr';
import { Processes, typetask } from './../../model/Workflow';
import { Role } from './../../model/Role';
import { AddWorkflowDialogComponent } from './dialogs/add/addWorkflow.dialog.component';
import { EditWorkflowDialogComponent } from './dialogs/edit/editWorkflow.dialog.component';
import { DeleteWorkflowDialogComponent } from './dialogs/delete/deleteWorkflow.dialog.component';
import { BehaviorSubject, fromEvent, merge, Observable, Subject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss']
})

export class WorkflowComponent implements OnInit, OnDestroy {

  //@ViewChild(MatSelect) matSelect: MatSelect;
  //@Output() selectionChange: EventEmitter<MatSelectChange> = new EventEmitter<MatSelectChange>();
  //@Output() valueChange: EventEmitter<any> = new EventEmitter<any>();

  pager: any = {};
  displayedColumns = ['taskId', 'Name', 'Type', 'Description', 'DurationInDays', 'SequenceNumber', 'Mandatory', /*'linksToDoc', 'linksToVideo', 'linksToFolder', 'linksToSite', 'subTasks', 'procId', 'roles', */'active', 'actions'];
  displayedInitialColumns = ['taskId', 'Name', 'Type', 'Description', 'DurationInDays', 'SequenceNumber', 'Mandatory', /*'linksToDoc', 'linksToVideo', 'linksToFolder', 'linksToSite', 'subTasks', 'procId', 'roles', */'active', 'actions'];
  displayedColumnForMobilesPhones: string[] = ['taskId', 'Name','DurationInDays', 'actions'];
  isColumnsMobile: boolean;
  innerWidth: any;
  exampleDatabase: WorkflowService | null;
  dataSource: ExampleDataSource | null;
  index: number;
  taskId: number;
  roles: Role[] = [];
  newid: number;
  //workflow: Workflow[]=[];
  processes: Processes[];
  public Selectprocesses = [
    { "id": 1, "name": "Preparation" },
    { "id": 2, "name": "Assessment" },
    { "id": 3, "name": "Implementation" },
    { "id": 4, "name": "Follow Up" },
    { "id": 5, "name": "Sustainability" }
  ]

  @Input() processid: number = this.Selectprocesses[0].id;
  @Output() selectionChange: EventEmitter<number> = new EventEmitter();
  processid2: number = 1;
  // selectionChange: EventEmitter<MatSelectChange>
  // @Output() selectionChange: EventEmitter<number> = new EventEmitter<number>();

  roleslist: any[] = ['loaded without backend', 'User', 'Admin', 'Cif-Leader', 'site_prod_shift'];
  destroy$: Subject<boolean> = new Subject<boolean>();
  private paramsubscriptions: Subscription[] = [];

  isLoading = false;
  dataSourcespin = null;
  version : any;

  constructor(public httpClient: HttpClient,
    public dialog: MatDialog,
    public WorkflowService: WorkflowService,
    private toastr: ToastrService,
    public roleService: RoleService) { }

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  ngOnInit() {
    this.loadData();
    this.processid = this.Selectprocesses[0].id;
    //this.WorkflowService.getAllWorkflow(this.processid);
    //console.log(this.processid);
    this.selectionChange.emit(this.processid);
    this.WorkflowService.currentProcessId.subscribe(processid => this.processid2 = processid);
    this.func();
    this.version = localStorage.getItem('PROCESSVERSION');

    /*this.WorkflowService.sendGetRequest(this.processid).pipe(takeUntil(this.destroy$)).subscribe((res: HttpResponse<Workflow[]>) => {
      console.log(res);
      this.isLoading = true;
      this.workflow = res.body;
      console.log(this.selectionChange);
    });
    */
    //this.paramsubscriptions.push(this.roleService.getAllRole().subscribe(Role => this.roles = Role));

    this.exampleDatabase.dataChange
      .subscribe(data => {
        this.isLoading = false;
        this.dataSourcespin = data;
      },
        error => this.isLoading = false
      );

    this.innerWidth = window.innerWidth;
    if(this.innerWidth < 850 && !this.isColumnsMobile) {
      //console.log(this.innerWidth);
      this.displayedColumns = this.displayedColumnForMobilesPhones;
      this.isColumnsMobile = true;
  
    } else if(this.innerWidth >= 850 && this.isColumnsMobile) {
  
      this.displayedColumns = this.displayedInitialColumns;
      this.isColumnsMobile = false;
    }

  }

  ngAfterViewInit(): void {
    this.innerWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.innerWidth = window.innerWidth;
    if(event.target.innerWidth < 850 && !this.isColumnsMobile) {
  
      this.displayedColumns = this.displayedColumnForMobilesPhones;
      this.isColumnsMobile = true;
  
    } else if(event.target.innerWidth >= 850 && this.isColumnsMobile) {
  
      this.displayedColumns = this.displayedInitialColumns;
      this.isColumnsMobile = false;
    }
  }

  func() {
    return this.selectionChange.subscribe((processid) => {
      this.processid = processid;
      // console.log('This is emitted value on workflowcomponent is: ' + processid);
    });
  }

  select(processid: number) {
    this.processid = processid;
    this.selectionChange.emit(this.processid);
    this.WorkflowService.changeProcessIdSelected(processid);
    // console.log(this.processid);
    this.loadData();
  }

  refresh() {
    this.loadData();
  }

  addNew(workflow?: Workflow) {
    const dialogRef = this.dialog.open(AddWorkflowDialogComponent, {
      data: { workflow: workflow }
    });
    //console.log(this.workflow);
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // After dialog is closed we're doing frontend updates
        // For add we're just pushing a new row inside userService
        this.exampleDatabase.dataChange.value.push(this.WorkflowService.getDialogData());
        this.refreshTable();
        this.loadData();
      }
    });
  }

  startEdit(i: number, taskId: number, name: string, type: any[], description: string, durationInDays: number, sequenceNumber: number,
    mandatory: boolean, docName: string[], linksToDoc: string[], videoName: string[], linksToVideo: string[], folderName: string[], linksToFolder: string[], siteName: string[], linksToSite: string[], sprint: string, subTasks: [], procId: number, roles: any[],
    active: boolean) {
    this.taskId = taskId;
    // index row is used just for debugging proposes and can be removed
    this.index = i;
    const dialogRef = this.dialog.open(EditWorkflowDialogComponent, {
      data: {
        taskId: taskId, name: name, type: type, description: description, durationInDays: durationInDays, sequenceNumber: sequenceNumber, mandatory: mandatory, docName: docName, docLink: linksToDoc, videoName: videoName, videoLink: linksToVideo,
        folderName: folderName, folderLink: linksToFolder, siteName: siteName, siteLink: linksToSite, sprint:sprint, subTasks: subTasks, procId: procId, roles: roles, active: active
      }
    });
    
    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        // When using an edit things are little different, firstly we find record inside userService by id
        const foundIndex = this.exampleDatabase.dataChange.value.findIndex(x => x.taskId === this.taskId);
        // Then you update that record using data from dialogData (values you enetered)
        this.exampleDatabase.dataChange.value[foundIndex] = this.WorkflowService.getDialogData();
        // And lastly refresh table
        this.refreshTable();
        this.loadData();
      }
    });
  }

  deleteItem(i: number, taskId: number, name: string, type: typetask[], description: string, durationInDays: number, sequenceNumber: number,
    mandatory: boolean, docName: string[], linksToDoc: string[], videoName: string[], linksToVideo: string[], folderName: string[], linksToFolder: string[], siteName: string[],
    linksToSite: string[], sprint:string, subTasks: [], procId: number, roles: [], active: boolean) {
    this.index = i;
    this.taskId = taskId;
    const dialogRef = this.dialog.open(DeleteWorkflowDialogComponent, {
      data: {
        taskId: taskId, name: name, type: type, description: description, durationInDays: durationInDays, sequenceNumber: sequenceNumber, mandatory: mandatory, docName: docName, docLink: linksToDoc, videoName: videoName, videoLink: linksToVideo, folderName: folderName, folderLink: linksToFolder,
        siteName: siteName, siteLink: linksToSite, sprint:sprint, subTasks: subTasks, procId: procId, roles: roles, active: active
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 1) {
        const foundIndex = this.exampleDatabase.dataChange.value.findIndex(x => x.taskId === this.taskId);
        // for delete we use splice in order to remove single object from DataService
        this.exampleDatabase.dataChange.value.splice(foundIndex, 1);
        this.refreshTable();
        this.loadData();
      }
    });
  }

  private refreshTable() {
    // Refreshing table using paginator
    // Thanks yeager-j for tips
    // https://github.com/marinantonio/angular-mat-table-crud/issues/12
    this.paginator._changePageSize(this.paginator.pageSize);

    // If you don't need a filter or a pagination this can be simplified, you just use code from else block
    // OLD METHOD:
    // if there's a paginator active we're using it for refresh
    if (this.dataSource._paginator.hasNextPage()) {
      this.dataSource._paginator.nextPage();
      this.dataSource._paginator.previousPage();
      // in case we're on last page this if will tick
    } else if (this.dataSource._paginator.hasPreviousPage()) {
      this.dataSource._paginator.previousPage();
      this.dataSource._paginator.nextPage();
      // in all other cases including active filter we do it like this
    } else {
      this.dataSource.filter = '';
      this.dataSource.filter = this.filter.nativeElement.value;
    }
  }

  public loadData() {
    this.exampleDatabase = new WorkflowService(this.httpClient, this.toastr);
    this.dataSource = new ExampleDataSource(this.exampleDatabase, this.paginator, this.sort, this.WorkflowService);
    fromEvent(this.filter.nativeElement, 'keyup')
      // .debounceTime(150)
      // .distinctUntilChanged()
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }
        this.dataSource.filter = this.filter.nativeElement.value;
      });

  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();

    this.paramsubscriptions.forEach(subscription => subscription.unsubscribe());
  }

  showSuccess() {
    this.toastr.success('Getting Data', 'Complete suscesful!');
  }

  showFailure() {
    this.toastr.success('Getting Data', 'Bad Request!');
  }
}


@Directive()
export class ExampleDataSource extends DataSource<Workflow> {
  _filterChange = new BehaviorSubject('');

  @Input() processid: number = 1;
  @Input() selectionChange: number = this.processid;
  processid2: number = 1;
  taskId: any;
  version :any;

  get filter(): string {
    return this._filterChange.value;
  }

  set filter(filter: string) {
    this._filterChange.next(filter);
  }

  filteredData: Workflow[] = [];
  renderedData: Workflow[] = [];

  constructor(public _exampleDatabase: WorkflowService,
    public _paginator: MatPaginator,
    public _sort: MatSort,
    public WorkflowService: WorkflowService) {
    super();
    
    // Reset to the first page when the Workflow changes the filter.
    this._filterChange.subscribe(() => this._paginator.pageIndex = 0);
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Workflow[]> {
    // Listen for any changes in the base data, sorting, filtering, or pagination
    const displayDataChanges = [
      this._exampleDatabase.dataChange,
      this._sort.sortChange,
      this._filterChange,
      this._paginator.page
    ];

    //****************
    //this.processid=this._WorkflowComponent.processid;
    //this._WorkflowComponent.selectionChange.subscribe((processid) => {
    //this.processid = processid});
    this.WorkflowService.currentProcessId.subscribe((processid2: number) => this.processid2 = processid2);
    this.processid = Number(this.processid2);
    if (!this.processid) {
      this._exampleDatabase.getAllWorkflow(this.selectionChange);
    }
    else {
      this._exampleDatabase.getAllWorkflow(this.processid);
    }

    //    this._exampleDatabase.getAllWorkflow(this.selectionChange);
    //console.log("on class ExampleDataSource " + this.selectionChange);
    //console.log("on class ExampleDataSource processid " + this.processid);
    //console.log("on class ExampleDataSource processid2 " + (JSON.stringify(this.processid2)));

    return merge(...displayDataChanges).pipe(map(() => {
      // Filter data
      this.filteredData = this._exampleDatabase.data.slice().filter((Workflow: Workflow) => {
        const searchStr = (Workflow.taskId + Workflow.name + Workflow.description + Workflow.durationInDays + Workflow.sequenceNumber + Workflow.mandatory + Workflow.docName + 
          Workflow.folderName + Workflow.videoName + Workflow.siteName + Workflow.type + Workflow.linksToDoc + Workflow.linksToFolder +  Workflow.linksToVideo + Workflow.linksToSite +
          Workflow.sprint + Workflow.subTasks + Workflow.roles + Workflow.procId + Workflow.active).toLowerCase();

        return searchStr.indexOf(this.filter.toLowerCase()) !== -1;
      });

      // Sort filtered data
      const sortedData = this.sortData(this.filteredData.slice());

      // Grab the page's slice of the filtered sorted data.
      const startIndex = this._paginator.pageIndex * this._paginator.pageSize;
      this.renderedData = sortedData.splice(startIndex, this._paginator.pageSize);
      return this.renderedData;
    }
    ));
  }

  disconnect() { }

  /** Returns a sorted copy of the database data. */
  sortData(data: Workflow[]): Workflow[] {
    if (!this._sort.active || this._sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      let propertyA: number | string | string[] | Date = '';
      let propertyB: number | string | string[] | Date = '';
      let propertyC: number | string;
      let propertyD: any[] | any[];

      switch (this._sort.active) {
        case 'taskId': [propertyA, propertyB] = [a.taskId, b.taskId]; break;
        case 'name': [propertyA, propertyB] = [a.name, b.name]; break;
        //case 'type': [propertyD, propertyD] = [a.type, b.type]; break;
        case 'description': [propertyA, propertyB] = [a.description, b.description]; break;
        case 'durationInDays': [propertyA, propertyC] = [a.durationInDays, b.durationInDays]; break;
        case 'sequenceNumber': [propertyA, propertyC] = [a.sequenceNumber, b.sequenceNumber]; break;
        case 'docName': [propertyA, propertyB] = [a.docName, b.docName]; break;
        case 'linksToDoc': [propertyA, propertyB] = [a.linksToDoc, b.linksToDoc]; break;
        case 'videoName': [propertyA, propertyB] = [a.videoName, b.videoName]; break;
        case 'linksToVideo': [propertyA, propertyB] = [a.linksToVideo, b.linksToVideo]; break;
        case 'folderName': [propertyA, propertyB] = [a.folderName, b.folderName]; break;
        case 'linksToFolder': [propertyA, propertyB] = [a.linksToFolder, b.linksToFolder]; break;
        case 'siteName': [propertyA, propertyB] = [a.siteName, b.siteName]; break;
        case 'linksToSite': [propertyA, propertyB] = [a.linksToSite, b.linksToSite]; break;
        case 'sprint': [propertyA, propertyB] = [a.sprint, b.sprint]; break;
        case 'subTasks': [propertyA, propertyB] = [a.subTasks, b.subTasks]; break;
        case 'procId': [propertyA, propertyC] = [a.procId, b.procId]; break;
      }

      const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
      const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
      const valueC = isNaN(+propertyC) ? propertyC : +propertyC;
      const valueD = isNaN(+propertyD) ? propertyD : +propertyD;

      return (valueA < valueB ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
      return (valueC < valueC ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
      return (valueD < valueD ? -1 : 1) * (this._sort.direction === 'asc' ? 1 : -1);
    });
  }
}