import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { WorkflowService } from '../../../../services/workflow.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Workflow, Processes } from './../../../../model/Workflow';
import { Role } from './../../../../model/Role';
import { typetask } from './../../../../model/typetask';
import { RoleService } from './../../../../services/role.service';
import { TypeTaskService } from './../../../../services/typetask.service';
import { ExampleDataSource } from '../../workflow.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-addWorkflow.dialog',
  templateUrl: './addWorkflow.dialog.html',
  styleUrls: ['./addWorkflow.dialog.scss'],
})

export class AddWorkflowDialogComponent implements OnInit, AfterViewInit, OnDestroy {
  name: String = "Name";
  workflow: Workflow = {} as Workflow;
  typetasks: typetask[] = [];
  type: any[];
  typeVal: any;
  emailVerified: boolean = true;
  provider: string = "google";
  plantId: number;
  roleslist: string[] = ['User', 'Admin', 'Cif-Leader', 'site_prod_shift', 'site_geo_quarry', 'site_maint_plan', 'site_prv_maint', 'site_plant_mgr', 'site_indust_dir', 'site_quality_mgr', 'site_maint_mgr', 'site_disp_mgr', 'site_auto_mgr', 'site_quarry_mgr', 'site_prod_ccr', 'site_prod_coach', 'site_plant_contr', 'site_maint_elec', 'site_prod_mgr', 'site_add_pos_1', 'site_rep_resp', 'site_maint_mech', 'site_proc_opt', 'site_hr_mgr', 'site_add_pos_2', 'site_env_mgr', 'site_pft_resp', 'site_add_pos_3', 'site_add_pos_5', 'site_add_pos_4'];
  AllSprint: string[] = ["S1", "S2", "S3", "S4", "S5", "S6"];
  ValueToString: string;
  ValueToStringToArray: any[] = [];
  roles: Role[] = [];
  processes: Processes[] = [];
  procId: any;
  selectedRoles: number[] = [];
  active: boolean = true;
  mandatory: boolean = true;
  public Selectprocesses = [
    { "id": 1, "name": "Preparation" },
    { "id": 2, "name": "Assessment" },
    { "id": 3, "name": "Implementation" },
    { "id": 4, "name": "Follow Up" },
    { "id": 5, "name": "Sustainability" }
  ]
  public Processes: [{ "id": number, "name": string }] = [{ "id": 1, "name": "Preparation" }];
  private paramsubscriptions: Subscription[] = [];

  ExampleDataSource: ExampleDataSource;
  addForm: FormGroup;
  DefaultFormValues: Object = {
    name: '',
    type: '',
    description: '',
    durationInDays: '',
    sequenceNumber: '',
    mandatory: 'false',
    docName: '',
    docLink: '',
    videoName: '',
    videoLink: '',
    folderName: '',
    folderLink: '',
    siteName: '',
    siteLink: '',
    sprint: '',
    subTasks: '',
    procId: '',
    roles: [],
    active: 1,
  };

  formControl = new FormControl('', [
    Validators.required,
    Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')
  ]);

  constructor(public dialogRef: MatDialogRef<AddWorkflowDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Workflow,
    public workflowService: WorkflowService,
    public TypeTaskService: TypeTaskService,
    public RoleService: RoleService) {
    this.name = this.data.name;
    this.addForm = new FormGroup({
      'name': new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      'type': new FormControl('', [
        Validators.required,
      ]),
      'description': new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(256),
      ]),
      'durationInDays': new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.pattern('[0-9._%+-]{1,3}$')
      ]),
      'sequenceNumber': new FormControl('', [
        Validators.required
      ]),
      'mandatory': new FormControl('true', []),
      'docName': new FormControl('', []),
      'docLink': new FormControl('', []),
      'videoName': new FormControl('', []),
      'videoLink': new FormControl('', []),
      'folderName': new FormControl('', []),
      'folderLink': new FormControl('', []),
      'siteName': new FormControl('', []),
      'siteLink': new FormControl('', []),
      'sprint': new FormControl('', []),
      //'subTasks': new FormControl({value: '', disabled: true},[]),
      'subTasks': new FormControl('', []),
      'procId': new FormControl('', []),
      'roles': new FormControl('5', []),
      'active': new FormControl('true', [])
    });
    this.addForm.setValue(this.DefaultFormValues);
    this.data.mandatory = this.mandatory;
    this.data.active = this.active;
    //this.addForm.controls["subTasks"].enable();
  }

  ngOnInit(): void {
    this.paramsubscriptions.push(this.TypeTaskService.getTypetasks().subscribe(typetask => this.typetasks = typetask.sort((a, b) => {
      return a.id == b.id ? 0 : a.id > b.id ? 1 : -1
    })));

    this.paramsubscriptions.push(this.RoleService.getAllRole().subscribe(role => this.roles = role.sort((a, b) => {
      return a.name == b.name ? 0 : a.name > b.name ? 1 : -1
    })));
  }

  ngAfterViewInit(): void {
    this.addForm.setValue(this.DefaultFormValues)
  }

  ngOnDestroy() {
    this.paramsubscriptions.forEach(subscription => subscription.unsubscribe());
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' : '';
  }

  submit() {
    // emppty stuff
  }

  reset() {
    this.addForm.reset(this.DefaultFormValues);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  convertstringArray(value) {
    this.ValueToString = value.toString().replace(/\s/g, "");
    this.ValueToStringToArray = this.ValueToString.split(",");
    return value = this.ValueToStringToArray;
  }

  public confirmAdd(): void {
    //this.data.docName = this.convertstringArray(this.data.docName);
    //this.data.videoName = this.convertstringArray(this.data.videoName);
    //this.data.folderName = this.convertstringArray(this.data.folderName);
    //this.data.siteName = this.convertstringArray(this.data.siteName);

    this.data.docLink = this.data.linksToDoc;
    this.data.videoLink = this.data.linksToVideo;
    this.data.folderLink = this.data.linksToFolder;
    this.data.siteLink = this.data.linksToSite;

    // this.data.linksToDoc = this.convertstringArray(this.data.linksToDoc);
    // this.data.linksToVideo = this.convertstringArray(this.data.linksToVideo);
    // this.data.linksToFolder = this.convertstringArray(this.data.linksToFolder);
    // this.data.linksToSite = this.convertstringArray(this.data.linksToSite);
    this.data.procId = this.procId;
    let taskName = 'Conditional Task';
    if(this.typeVal == 1){
      taskName= 'Task Master';
    }
    var type = {
      "id" : this.typeVal,
      "name" : taskName
    }
    this.data.type =type;
    this.workflowService.addWorkflow(this.data);
    console.log(this.data);
  }

  onChangeRoles($event) {
    console.log("cambiado valor en selector de roles");
    console.log($event.value + " Clicked!");
  }

  isImplementationProcess() {
    if (this.Selectprocesses[0].id = 3)
      (this.active = true)
    else
      (this.active = false)
  }
  
  isActivetask(value) {
    this.data.active = !value;
  }

  isActiveMandatory(value) {
    this.data.mandatory = !value;
  }
}
/*
  isActivetask($event) {
    $event.source.toggle();
    console.log("marked actived task");
    this.active = this.data[0].active != null && this.workflow[0].active;
    console.log("marked actived task: " + this.active);
  }

  isActiveMandatory($event) {
    $event.source.toggle(this.data.mandatory);
    console.log("marked actived task");
    this.data.mandatory = $event.checked;
    console.log("marked actived task: " + this.data.mandatory);
  }

}
*/