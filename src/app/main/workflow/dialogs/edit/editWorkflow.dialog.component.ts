import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { WorkflowService } from '../../../../services/workflow.service';
import { RoleService } from './../../../../services/role.service';
import { TypeTaskService } from './../../../../services/typetask.service';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { Workflow, Processes } from './../../../../model/Workflow';
import { typetask } from './../../../../model/typetask';
import { Role } from './../../../../model/Role';
import { SubTasks } from './../../../../model/subTask';

@Component({
  selector: 'app-editWorkflow.dialog',
  templateUrl: './editWorkflow.dialog.html',
  styleUrls: ['./editWorkflow.dialog.scss']
})

export class EditWorkflowDialogComponent implements OnInit, OnDestroy {

  roleslist: string[] = ['User', 'Admin', 'Cif-Leader', 'site_prod_shift', 'site_geo_quarry', 'site_maint_plan', 'site_prv_maint', 'site_plant_mgr', 'site_indust_dir', 'site_quality_mgr', 'site_maint_mgr', 'site_disp_mgr', 'site_auto_mgr', 'site_quarry_mgr', 'site_prod_ccr', 'site_prod_coach', 'site_plant_contr', 'site_maint_elec', 'site_prod_mgr', 'site_add_pos_1', 'site_rep_resp', 'site_maint_mech', 'site_proc_opt', 'site_hr_mgr', 'site_add_pos_2', 'site_env_mgr', 'site_pft_resp', 'site_add_pos_3', 'site_add_pos_5', 'site_add_pos_4'];
  public Selectprocesses = [
    { "id": 1, "name": "Preparation" },
    { "id": 2, "name": "Assessment" },
    { "id": 3, "name": "Implementation" },
    { "id": 4, "name": "Follow Up" },
    { "id": 5, "name": "Sustainability" }
  ]
  public Processes: [{ "id": number, "name": string }] = [{ "id": 1, "name": "Preparation" }];
  editForm: FormGroup;
  numbers: number[] = [];
  processes: number;
  imageUrl: string = "";
  createDate: Date;
  Workflow: Workflow;
  type: typetask;
  procId: number;
  typetasks: typetask[] = [];
  AllSprint: string[] = ["S1", "S2", "S3", "S4", "S5", "S6"];
  selectedSubTasks: SubTasks[] = [];
  roles: Role[] = [];
  selectedRoles: number[] = [];
  selectedTypeTask: number;
  ValueToString: string;
  ValueToStringToArray: any[] = [];
  mandatory: boolean;
  active: boolean;
  toggle: boolean;
  siteName: string;
  taskId: number;

  private paramsubscriptions: Subscription[] = [];

  constructor(public dialogRef: MatDialogRef<EditWorkflowDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Workflow,
    public workflowService: WorkflowService,
    public TypeTaskService: TypeTaskService,
    //public SubtaskService: SubtaskService,
    public RoleService: RoleService) {
    this.editForm = new FormGroup({
      'name': new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      'type': new FormControl('', [
        Validators.required
      ]),
      'description': new FormControl('', [
        Validators.required,
        Validators.minLength(6)
      ]),
      'durationInDays': new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.pattern('[0-9._%+-]{1,2}$')
      ]),
      'sequenceNumber': new FormControl('', [
        Validators.required,
        Validators.minLength(1)
      ]),
      //'subTasks': new FormControl({value: '', disabled: true}, []),
      'subTasks': new FormControl('', []),
      'docName': new FormControl('', []),
      'docLink': new FormControl('', []),
      'videoName': new FormControl('', []),
      'videoLink': new FormControl('', []),
      'folderName': new FormControl('', []),
      'folderLink': new FormControl('', []),
      'siteName': new FormControl('', []),
      'siteLink': new FormControl('', []),
      'sprint': new FormControl('', []),
      'procId': new FormControl('', []),
      'roles': new FormControl('', []),
      'mandatory': new FormControl('true', []),
      'active': new FormControl('true', [])
    });
  }

  ngOnInit(): void {
    this.paramsubscriptions.push(this.TypeTaskService.getTypetasks().subscribe(typetask => this.typetasks = typetask.sort((a, b) => {
      return a.id == b.id ? 0 : a.id > b.id ? 1 : -1
    })));

    //this.paramsubscriptions.push(this.SubtaskService.getsubTasks().subscribe(SubTasks => this.SubTasks = SubTasks.sort((a, b) => {
    //  return a.id == b.id ? 0 : a.id > b.id ? 1 : -1
    //})));

    this.paramsubscriptions.push(this.RoleService.getAllRole().subscribe(role => this.roles = role.sort((a, b) => {
      return a.name == b.name ? 0 : a.name > b.name ? 1 : -1
    })));

    this.Selectprocesses = this.Selectprocesses.filter(x => x.id === this.data.procId);
    //console.log(this.Selectprocesses);

    this.selectedTypeTask = this.data.type.id; //map(a =>a.id);
    //this.selectedSubTasks = this.data[0].subTasks.id; //map(a =>a.id);
    //this.selectedSubTasks = this.data.subTasks;
    this.selectedRoles = this.data.roles.map(a => a.id);
    this.processes = this.data.procId;
  }

  //formControl = new FormControl('', [Validators.required]);

  getErrorMessage() {
    return this.editForm.hasError('required') ? 'Required field' :
      this.editForm.hasError('minLength') ? 'Not a valid size of field' : '';

    //    return this.formControl.hasError('required') ? 'Required field' :
    //      this.formControl.hasError('minLength') ? 'Not a valid size of field' : '';
  }

  ngOnDestroy() {
    this.paramsubscriptions.forEach(subscription => subscription.unsubscribe());
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  convertstringArray(value) {
    if  (value != null){
    this.ValueToString = value.toString().replace(/\s/g, "");
    this.ValueToStringToArray = this.ValueToString.split(",");
    return value = this.ValueToStringToArray;
  }
  }

  stopEdit(): void {
   // this.data.docName = this.convertstringArray(this.data.docName);
    //this.data.videoName = this.convertstringArray(this.data.videoName);
    //this.data.folderName = this.convertstringArray(this.data.folderName);
    //this.data.siteName = this.convertstringArray(this.data.siteName);

    // this.data.docLink = this.data.linksToDoc;
    // this.data.videoLink = this.data.linksToVideo;
    // this.data.folderLink = this.data.linksToFolder;
    // this.data.siteLink = this.data.linksToSite;


    // this.data.linksToDoc = this.convertstringArray(this.data.linksToDoc);
    // this.data.linksToVideo = this.convertstringArray(this.data.linksToVideo);
    // this.data.linksToFolder = this.convertstringArray(this.data.linksToFolder);
    // this.data.linksToSite = this.convertstringArray(this.data.linksToSite);

   


    this.workflowService.updateWorkflow(this.data);
    console.log(this.data);
  }
  
  onChangeRoles($event) {
    console.log("cambiado valor en selector de roles");
    console.log($event.value + " Clicked!");
  }

  isActivetask(value) {
    this.data.active = !value;
  }

  isActiveMandatory(value) {
    this.data.mandatory = !value;
  }

  /*  isActivetask($event) {
      console.log("marked active task as boolean3: " + this.data.active);
      console.log("marked active task as number: " + Number(this.data.active));
      //$event.source.toggle(Number(this.data.active));
      this.data.active = $event.checked;
      this.active = !Number(this.data.active);
    }
  
    isActiveMandatory($event) {
      $event.source.toggle(this.data.mandatory);
      this.data.mandatory[0] = $event.checked;
      this.mandatory = !this.data.mandatory[0];
      console.log("marked mandatory task: " + this.data.mandatory[0]);
    }
  */
}

