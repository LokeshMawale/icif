import { WorkflowService } from './../../../../services/workflow.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-deleteWorkflow.dialog',
  templateUrl: './deleteWorkflow.dialog.html',
  styleUrls: ['./deleteWorkflow.dialog.scss']
})
export class DeleteWorkflowDialogComponent {

  constructor(public dialogRef: MatDialogRef<DeleteWorkflowDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public workflowService: WorkflowService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  confirmDelete(): void {
    this.workflowService.deleteWorkflow(this.data.taskId);
  }
}
