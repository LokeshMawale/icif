// users.js
var faker = require('faker')
function generateUsers () {
  var users = []
  for (var id = 0; id < 50; id++) {
    var Name = faker.name.lastName()
    var email = faker.internet.email()
    var imageURL = faker.image.avatar()
    var createddate = faker.date.past()
    var sapid = faker.name.firstName()
    var roles = faker.name.jobArea()
    users.push({
      "id": id,
      "Name": Name,
      "email": email,
      "imageURL ": imageURL,
      "createddate":createddate,
      "sapid":sapid,
      "roles": roles
    })
  }
  return { "users": users }
}
module.exports = generateUsers
