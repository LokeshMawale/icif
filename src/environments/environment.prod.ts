export const environment = {
  production: true,
  // apiUrl: 'http://localhost:8080',
  //apiUrl: 'https://emea-lh-cif.appspot.com',
  // piUrl : 'https://dev-dot-emea-lh-cif.appspot.com',
  apiUrl : 'https://cem-icif-prod.ew.r.appspot.com',
  // apiUrl: 'http://34.65.104.98:8080',
  users: 'src/assets/users.json',
  devTitle : 'Development Environment',
  testTitle : 'Test Environment',
  prodTitle : 'Production Environment',
  googleSheetsApiKey: 'AIzaSyAX8ICvC01oVoLOTP9mtcr4TnzP6_KKLy4',
  characters: {
    spreadsheetId: '1loan_qtaTiF3km3Gfz6JTGORPXa-CNMBgRacY1JIZo4',
    worksheetName: 'Characters',
  },
  whitelist: ['localhost:8080', 'emea-lh-cif.appspot.com', 'corp-search-doc-library.appspot.com', 'cem-icif-prod.ew.r.appspot.com','dev-dot-emea-lh-cif.appspot.com']
};

// https://docs.google.com/spreadsheets/d/1loan_qtaTiF3km3Gfz6JTGORPXa-CNMBgRacY1JIZo4/edit#gid=0
// https://docs.google.com/spreadsheets/d/e/2PACX-1vREqxjKIHxBBZ21hh5WBEpnsBSZy_3WtRO1BN4ak7xUjZUQYCIlPuJ0zq6VOye2lEkCznSLINg60382/pubhtml
