// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //apiUrl: 'https://emea-lh-cif.appspot.com',
  // apiUrl: 'http://localhost:8080',
  // apiUrl : 'https://dev-dot-emea-lh-cif.appspot.com',
  apiUrl : 'https://cem-icif-prod.ew.r.appspot.com',
  // apiUrl: 'http://34.65.104.98:8080',
  users: 'src/assets/users.json',
  devTitle : 'Development Environment',
  testTitle : 'Test Environment',
  prodTitle : 'Production Environment',
  googleSheetsApiKey: 'AIzaSyAX8ICvC01oVoLOTP9mtcr4TnzP6_KKLy4',
  characters: {
    spreadsheetId: '1loan_qtaTiF3km3Gfz6JTGORPXa-CNMBgRacY1JIZo4',
    worksheetName: 'Characters',
  },
  whitelist: ['localhost:8080', 'localhost:4200', 'emea-lh-cif.appspot.com', 'corp-search-doc-library.appspot.com', 'cem-icif-prod.ew.r.appspot.com', 'dev-dot-emea-lh-cif.appspot.com']
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
